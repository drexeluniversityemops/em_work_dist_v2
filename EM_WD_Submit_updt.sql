USE [EM_Image_Processing]
GO
/****** Object:  StoredProcedure [dbo].[EM_WD_Submit_updt]    Script Date: 10/11/2013 3:15:41 PM ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO



Create proc [dbo].[EM_WD_Submit_updt_new]

@Image_ID						INT,
@Image_Queues_ID				INT,
@Banner_ID						VARCHAR(8),
@Sequence_Number_CDL			VARCHAR(50),
@ADMR_Code						VARCHAR(4),
@ADMR_Comment					NVARCHAR(30),
@ADMR_Received_Date				DATETIME,
@ADMR_Mandatory					BIT,
@General_Comment_Type			VARCHAR(3),
@General_Comment_Text			NVARCHAR(4000),
@DOB							DATETIME,
@SSN							VARCHAR(9),
@Gender							VARCHAR(1),
@Source_Code					VARCHAR(7),
@Interest_Codes_CDL				VARCHAR(50),
@Attribute_Codes_CDL			VARCHAR(50),
@Contact_Code					VARCHAR(3),
@CEEB_Zip						VARCHAR(5),
@CEEB							VARCHAR(6),
@HS_GPA							DECIMAL(3,2),
@Grad_Date_High_School			DATETIME,
@RIC_High_School				NUMERIC(4,0),
@Class_Size_High_School			NUMERIC(4,0),
@EM_WD_Users_ID					INT,
@Image_FA_Comment				VARCHAR(250),
@Test_On_Transcript_YN			BIT

as

DECLARE @NullDate DATETIME
SET @NullDate = '1900-01-01 00:00:00'

IF @Grad_Date_High_School = @NullDate
BEGIN
	Set @Grad_Date_High_School = Null
END

IF @RIC_High_School = 0
BEGIN
	Set @RIC_High_School = NULL
END

IF @HS_GPA = 0
BEGIN
	Set @HS_GPA = NULL
END

IF @Class_Size_High_School = 0
BEGIN
	Set @Class_Size_High_School = NULL
END

UPDATE
		Image_Move_To_Nolij_Banner
SET
		Banner_ID = @Banner_ID,
		Sequence_Number_CDL = @Sequence_Number_CDL,
		ADMR_Code = @ADMR_Code,
		ADMR_Comment = @ADMR_Comment,
		ADMR_Received_Date = @ADMR_Received_Date,
		ADMR_Mandatory = @ADMR_Mandatory,
		General_Comment_Type = @General_Comment_Type,
		General_Comment_Text = @General_Comment_Text,
		DOB = @DOB,
		SSN = @SSN,
		Gender = @Gender,
		Source_Code = @Source_Code,
		Interest_Codes_CDL = @Interest_Codes_CDL,
		Attribute_Codes_CDL = @Attribute_Codes_CDL,
		Contact_Code = @Contact_Code,
		Zip_CEEB = @CEEB_Zip,
		CEEB = @CEEB,
		HS_GPA = @HS_GPA,
		Grad_Date_High_School = @Grad_Date_High_School,
		RIC_High_School = @RIC_High_School,
		Class_Size_High_School = @Class_Size_High_School,
		Image_Processed = 1,
		Image_Processed_Datetime = getdate(),
		Image_Processed_Users_ID = @EM_WD_Users_ID,
		Test_On_Transcript_YN = @Test_On_Transcript_YN
WHERE
		Image_ID = @Image_ID

UPDATE
		Image_Exceptions
SET
		Image_FA_Comment = @Image_FA_Comment,
		FA_Comment_Users_ID = @EM_WD_Users_ID,
		FA_Comment_Datetime = getdate()
WHERE
		Image_Queues_ID = @Image_Queues_ID

/* log the insert into the database */		
INSERT INTO
	Image_Status_Log
		(EM_WD_Users_ID,
		Image_Status_Datetime_Added,
		Image_Status_ID,
		Image_ID)
	VALUES
		(@EM_WD_Users_ID,
		getdate(),
		25,
		@Image_ID)
		
/* Remove the processed image from the queue if it started there*/

/* Temporary removal of the Delete due to indexing issue */
DELETE FROM
	Image_Queues
WHERE
	Image_Queues_ID = @Image_Queues_ID
/*	
UPDATE Image_Queues
	SET Image_Queue_Type_ID = 55
WHERE Image_Queues_ID = @Image_Queues_ID
*/