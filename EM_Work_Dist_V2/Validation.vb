﻿Imports System.Collections.Generic
Imports System.Windows.Forms
Imports System.Windows.Forms.Form
Imports System.Text.RegularExpressions

Namespace DataProcessing

    Public Class Validation

        Public Shared Function ValidateDOB() As Boolean
            Dim bStatus As Boolean = True
            If DP_WDP.dtDOB.Checked Then

                If DP_WDP.dtDOB.Text = "" Then
                    DP_WDP.ErrorProvider1.SetError(DP_WDP.dtDOB, "Please enter your Age")
                    bStatus = False
                Else
                    DP_WDP.ErrorProvider1.SetError(DP_WDP.dtDOB, "")
                    Dim Age_13 As DateTime = Date.Now
                    Age_13 = Age_13.AddYears(-13)
                    Dim dtDOB_Date As DateTime = DP_WDP.dtDOB.Text
                    'If the date of birth is listed as today then ignore the value
                    If dtDOB_Date <> DateTime.Today Then
                        If dtDOB_Date > Age_13 Then
                            DP_WDP.ErrorProvider1.SetError(DP_WDP.dtDOB, "Must be at least 13 years old")
                            bStatus = False
                        Else
                            DP_WDP.ErrorProvider1.SetError(DP_WDP.dtDOB, "")
                        End If
                    Else
                        DP_WDP.ErrorProvider1.SetError(DP_WDP.dtDOB, "")
                    End If
                End If
            End If
            Return bStatus
        End Function

        Public Shared Function ValidateDecimal2_1(ByVal Decimal2_1 As String, ByVal From_Field As Object) As Boolean
            Dim bStatus As Boolean = True
            If Trim(Decimal2_1) = "" Then
                DP_WDP.ErrorProvider1.SetError(From_Field, "")
            Else
                Dim oGPA As New Regex("^[1]?[0-9]?[0-9]\.[0-9][0-9]$")
                If oGPA.IsMatch(Trim(Decimal2_1)) Then
                    DP_WDP.ErrorProvider1.SetError(From_Field, "")
                Else
                    DP_WDP.ErrorProvider1.SetError(From_Field, "Please enter a GPA in the format X.XX")
                    bStatus = False
                End If
            End If

            Return bStatus
        End Function

        Public Shared Function ValidatePhone_SSN_3Digit(ByVal Phone_SSN_3_Digit As String, ByVal From_Field As Object) As Boolean
            Dim bStatus As Boolean = True
            If Trim(Phone_SSN_3_Digit) = "" Then
                DP_WDP.ErrorProvider1.SetError(From_Field, "")
            Else
                Dim o3Digits As New Regex("^[0-9][0-9][0-9]$")
                If o3Digits.IsMatch(Trim(Phone_SSN_3_Digit)) Then
                    DP_WDP.ErrorProvider1.SetError(From_Field, "")
                Else
                    DP_WDP.ErrorProvider1.SetError(From_Field, "Please enter a number in the format XXX")
                    bStatus = False
                End If
            End If
            Return bStatus
        End Function

        Public Shared Function ValidateGPA_Decimal2_1() As Boolean
            Dim bStatus As Boolean = ValidateDecimal2_1(DP_WDP.txtHS_GPA.Text, DP_WDP.txtHS_GPA)
            Return bStatus
        End Function

        Public Shared Function Validate_Numeric_4Digit(ByVal From_Value As String, ByVal From_Field As Object) As Boolean
            Dim bStatus As Boolean = True
            If Trim(From_Value) = "" Then
                DP_WDP.ErrorProvider1.SetError(From_Field, "")
            Else
                Dim o3Digits As New Regex("^[0-9][0-9]?[0-9]?[0-9]?$")
                If o3Digits.IsMatch(Trim(From_Value)) Then
                    DP_WDP.ErrorProvider1.SetError(From_Field, "")
                Else
                    DP_WDP.ErrorProvider1.SetError(From_Field, "Please enter a number in the format XXXX")
                    bStatus = False
                End If
            End If
            Return bStatus
        End Function

        Public Shared Function ValidateUnclaimeds() As Boolean
            Dim bStatus As Boolean = True
            If DP_WDP.txtUnclaimed_First_Name.Text = "" Then
                DP_WDP.ErrorProvider1.SetError(DP_WDP.txtUnclaimed_First_Name, "Enter a First Name")
                bStatus = False
            Else
                DP_WDP.ErrorProvider1.SetError(DP_WDP.txtUnclaimed_First_Name, "")
            End If
            If DP_WDP.txtUnclaimed_Last_Name.Text = "" Then
                DP_WDP.ErrorProvider1.SetError(DP_WDP.txtUnclaimed_Last_Name, "Enter a Last Name")
                bStatus = False
            Else
                DP_WDP.ErrorProvider1.SetError(DP_WDP.txtUnclaimed_Last_Name, "")
            End If

            'Clear the exception comment error message.  It will get added when the image is removed from unclaimed queue.
            DP_WDP.ErrorProvider1.SetError(DP_WDP.txtExceptionComment_Follow_up, "")

            Return bStatus
        End Function

        Public Shared Function ValidateHS_CollegeSearch() As Boolean
            Dim bStatus As Boolean = True

            'Check to make sure multiple textboxes do not contain data if they do send over an error.
            If DP_WDP.txtCEEBCode.Text <> "" And DP_WDP.txtZipCode.Text <> "" Then
                DP_WDP.ErrorProvider1.SetError(DP_WDP.lblSearch_Error, "Enter text in only one box")
                DP_WDP.ErrorProvider1.SetError(DP_WDP.txtCEEBCode, "")
                DP_WDP.ErrorProvider1.SetError(DP_WDP.txtZipCode, "")
                DP_WDP.ErrorProvider1.SetError(DP_WDP.txtCity, "")
                bStatus = False
                GoTo ExitFunction
            End If

            If DP_WDP.txtCEEBCode.Text <> "" And DP_WDP.txtCity.Text <> "" Then
                DP_WDP.ErrorProvider1.SetError(DP_WDP.lblSearch_Error, "Enter text in only one box")
                DP_WDP.ErrorProvider1.SetError(DP_WDP.txtCEEBCode, "")
                DP_WDP.ErrorProvider1.SetError(DP_WDP.txtZipCode, "")
                DP_WDP.ErrorProvider1.SetError(DP_WDP.txtCity, "")
                bStatus = False
                GoTo ExitFunction
            End If

            If DP_WDP.txtCity.Text <> "" And DP_WDP.txtZipCode.Text <> "" Then
                DP_WDP.ErrorProvider1.SetError(DP_WDP.lblSearch_Error, "Enter text in only one box")
                DP_WDP.ErrorProvider1.SetError(DP_WDP.txtCEEBCode, "")
                DP_WDP.ErrorProvider1.SetError(DP_WDP.txtZipCode, "")
                DP_WDP.ErrorProvider1.SetError(DP_WDP.txtCity, "")
                bStatus = False
                GoTo ExitFunction
            End If

            DP_WDP.ErrorProvider1.SetError(DP_WDP.lblSearch_Error, "")
            'Check to make sure each textbox has at least three characters
            If Len(DP_WDP.txtCEEBCode.Text) > 2 Then
                DP_WDP.ErrorProvider1.SetError(DP_WDP.txtCEEBCode, "")
                DP_WDP.ErrorProvider1.SetError(DP_WDP.txtZipCode, "")
                DP_WDP.ErrorProvider1.SetError(DP_WDP.txtCity, "")
                GoTo ExitFunction
            ElseIf Len(DP_WDP.txtZipCode.Text) + Len(DP_WDP.txtCity.Text) = 0 Then
                DP_WDP.ErrorProvider1.SetError(DP_WDP.txtCEEBCode, "Enter a CEEB Code")
                bStatus = False
            End If

            If bStatus = True Then
                If Len(DP_WDP.txtZipCode.Text) > 2 Then
                    DP_WDP.ErrorProvider1.SetError(DP_WDP.txtCEEBCode, "")
                    DP_WDP.ErrorProvider1.SetError(DP_WDP.txtZipCode, "")
                    DP_WDP.ErrorProvider1.SetError(DP_WDP.txtCity, "")
                    GoTo ExitFunction
                ElseIf Len(DP_WDP.txtCity.Text) = 0 Then

                    DP_WDP.ErrorProvider1.SetError(DP_WDP.txtZipCode, "Enter a Zip Code")
                    bStatus = False
                End If
            End If

            If bStatus = True Then
                If Len(DP_WDP.txtCity.Text) > 2 Then
                    DP_WDP.ErrorProvider1.SetError(DP_WDP.txtCEEBCode, "")
                    DP_WDP.ErrorProvider1.SetError(DP_WDP.txtZipCode, "")
                    DP_WDP.ErrorProvider1.SetError(DP_WDP.txtCity, "")
                    GoTo ExitFunction
                Else
                    DP_WDP.ErrorProvider1.SetError(DP_WDP.txtCity, "Enter a City")
                    bStatus = False
                End If
            End If

ExitFunction:
            Return bStatus
        End Function

        Public Shared Function ValidateProblems() As Boolean
            Dim bStatus As Boolean = True
            If DP_WDP.txtProblem_Comments.Text = "" Then
                DP_WDP.ErrorProvider1.SetError(DP_WDP.txtProblem_Comments, "Please enter a comment")
                bStatus = False
            Else
                DP_WDP.ErrorProvider1.SetError(DP_WDP.txtProblem_Comments, "")
            End If

            'Clear the exception comment error message.  It will get added when the image is removed from problem queue.
            DP_WDP.ErrorProvider1.SetError(DP_WDP.txtExceptionComment_Follow_up, "")

            Return bStatus
        End Function

        Public Shared Function ValidateExceptions() As Boolean
            Dim bStatus As Boolean = True
            If DP_WDP.txtExceptionComment.Text = "" Then
                DP_WDP.ErrorProvider1.SetError(DP_WDP.txtExceptionComment, "Enter a comment")
                bStatus = False
            Else
                DP_WDP.ErrorProvider1.SetError(DP_WDP.txtExceptionComment, "")
            End If
            Return bStatus
        End Function

        Public Shared Function ValidateSSN_3Digit() As Boolean
            Dim bStatus As Boolean = ValidatePhone_SSN_3Digit(DP_WDP.txtSSN1.Text, DP_WDP.txtSSN1)
            Return bStatus
        End Function

        Public Shared Function ValidateSSN_2Digit() As Boolean
            Dim bStatus As Boolean = True
            If Trim(DP_WDP.txtSSN2.Text) = "" Then
                DP_WDP.ErrorProvider1.SetError(DP_WDP.txtSSN2, "")
            Else
                Dim o2Digits As New Regex("^[0-9][0-9]$")
                If o2Digits.IsMatch(Trim(DP_WDP.txtSSN2.Text)) Then
                    DP_WDP.ErrorProvider1.SetError(DP_WDP.txtSSN2, "")
                Else
                    DP_WDP.ErrorProvider1.SetError(DP_WDP.txtSSN2, "Please enter a number in the format XX")
                    bStatus = False
                End If
            End If
            Return bStatus
        End Function

        Public Shared Function ValidateSSN_4Digit() As Boolean
            Dim bStatus As Boolean = ValidatePhone_SSN_4Digit(DP_WDP.txtSSN3.Text, DP_WDP.txtSSN3)
            Return bStatus
        End Function

        Public Shared Function ValidateRIC() As Boolean
            Dim bStatus As Boolean = Validate_Numeric_4Digit(DP_WDP.txtHS_RIC.Text, DP_WDP.txtHS_RIC)
            Return bStatus
        End Function

        Public Shared Function ValidateClass_Size() As Boolean
            Dim bStatus As Boolean = Validate_Numeric_4Digit(DP_WDP.txtHS_ClassSize.Text, DP_WDP.txtHS_ClassSize)
            Return bStatus
        End Function

        Public Shared Function ValidatePhone_SSN_4Digit(ByVal Phone_SSN_4_Digit As String, ByVal From_Field As Object) As Boolean
            Dim bStatus As Boolean = True
            If Trim(Phone_SSN_4_Digit) = "" Then
                DP_WDP.ErrorProvider1.SetError(From_Field, "")
            Else
                Dim o4Digits As New Regex("^[0-9][0-9][0-9][0-9]$")
                If o4Digits.IsMatch(Trim(Phone_SSN_4_Digit)) Then
                    DP_WDP.ErrorProvider1.SetError(From_Field, "")
                Else
                    DP_WDP.ErrorProvider1.SetError(From_Field, "Please enter a number in the format XXXX")
                    bStatus = False
                End If
            End If
            Return bStatus
        End Function

        Public Shared Function ValidateSequence_Number() As Boolean
            Dim bStatus As Boolean = True
            If DP_WDP.cbxSeq1.Text = "N/A" And DP_WDP.cbxPayment_Department.Text = "" Then
                DP_WDP.ErrorProvider1.SetError(DP_WDP.cbxSeq1, "Please select a department")
                bStatus = False
            Else
                DP_WDP.ErrorProvider1.SetError(DP_WDP.cbxSeq1, "")
            End If

            Return bStatus
        End Function

        Public Shared Function ValidatePayment_Type() As Boolean
            Dim bStatus As Boolean = True

            If DP_WDP.cbxPayment_Type.Text = "" Then
                DP_WDP.ErrorProvider1.SetError(DP_WDP.cbxPayment_Type, "Please select a type")
                bStatus = False
            Else
                DP_WDP.ErrorProvider1.SetError(DP_WDP.cbxPayment_Type, "")
            End If
            Return bStatus
        End Function

        Public Shared Function ValidatePayment_Method() As Boolean
            Dim bStatus As Boolean = True

            If DP_WDP.cbxPayment_Method.Text = "" Then
                DP_WDP.ErrorProvider1.SetError(DP_WDP.cbxPayment_Method, "Please select a method")
                bStatus = False
            Else
                DP_WDP.ErrorProvider1.SetError(DP_WDP.cbxPayment_Method, "")
            End If
            Return bStatus
        End Function

        Public Shared Function ValidateException_Text() As Boolean
            Dim bStatus As Boolean = True

            If DP_WDP.txtExceptionComment_Follow_up.Visible And DP_WDP.txtExceptionComment_Follow_up.Text = "" Then
                DP_WDP.ErrorProvider1.SetError(DP_WDP.txtExceptionComment_Follow_up, "Please enter a follow-up to the comment above.")
                bStatus = False
            Else
                DP_WDP.ErrorProvider1.SetError(DP_WDP.txtExceptionComment_Follow_up, "")
            End If
            Return bStatus
        End Function

        Public Shared Function ValidateEM_EMO_Comment() As Boolean
            Dim bStatus As Boolean = True

            If Trim(DP_WDP.txtGeneralComment.Text) <> "" And (DP_WDP.rdlCommentType_EMO.Checked = False And DP_WDP.rdlCommentType_EM.Checked = False) Then
                DP_WDP.ErrorProvider1.SetError(DP_WDP.txtGeneralComment, "")
                DP_WDP.ErrorProvider1.SetError(DP_WDP.rdlCommentType_EMO, "Please select a comment type.")
                bStatus = False
            ElseIf Trim(DP_WDP.txtGeneralComment.Text) = "" And (DP_WDP.rdlCommentType_EMO.Checked = True Or DP_WDP.rdlCommentType_EM.Checked = True) Then
                DP_WDP.ErrorProvider1.SetError(DP_WDP.txtGeneralComment, "Please enter a comment.")
                DP_WDP.ErrorProvider1.SetError(DP_WDP.rdlCommentType_EMO, "")
                bStatus = False
            End If
            Return bStatus
        End Function

        Public Shared Function ValidatecbxPayment_Type() As Boolean
            Dim bStatus As Boolean = True

            If IsDBNull(DP_WDP.cbxPayment_Type.SelectedItem) = True Then
                DP_WDP.btnSubmit.Enabled = True
            ElseIf DP_WDP.cbxPayment_Type.SelectedItem = "" Then
                DP_WDP.btnSubmit.Enabled = True
            Else
                DP_WDP.btnSubmit.Enabled = False
            End If
            Return bStatus
        End Function

        Public Shared Function ValidateADMRComment(ByVal OA_Banner_ADMR_Comment_Required As Boolean, ByVal OA_Banner_EMO_Comment_Required As Boolean) As Boolean
            Dim bStatus As Boolean = True
            If OA_Banner_EMO_Comment_Required = True And DP_WDP.cbxPayment_Department.Text = "" Then
                If Trim(DP_WDP.txtGeneralComment.Text) = "" Or DP_WDP.rdlCommentType_EMO.Checked = False Then
                    DP_WDP.ErrorProvider1.SetError(DP_WDP.txtGeneralComment, "Please enter an EMO Comment")
                    DP_WDP.ErrorProvider1.SetError(DP_WDP.rdlCommentType_EMO, "Please enter an EMO Comment")
                    bStatus = False
                Else
                    DP_WDP.ErrorProvider1.SetError(DP_WDP.txtGeneralComment, "")
                    DP_WDP.ErrorProvider1.SetError(DP_WDP.rdlCommentType_EMO, "")
                End If
            Else
                DP_WDP.ErrorProvider1.SetError(DP_WDP.txtGeneralComment, "")
                DP_WDP.ErrorProvider1.SetError(DP_WDP.rdlCommentType_EMO, "")
            End If

            If OA_Banner_ADMR_Comment_Required = True And DP_WDP.cbxPayment_Department.Text = "" Then
                If Trim(DP_WDP.txtAdmrComment.Text) = "" Then
                    DP_WDP.ErrorProvider1.SetError(DP_WDP.txtAdmrComment, "Please enter an ADMR Comment")
                    bStatus = False
                Else
                    DP_WDP.ErrorProvider1.SetError(DP_WDP.txtAdmrComment, "")
                End If
            Else
                DP_WDP.ErrorProvider1.SetError(DP_WDP.txtAdmrComment, "")
            End If

            If IsDBNull(DP_WDP.cbxAdmrCode.SelectedItem) = True Then
                DP_WDP.ErrorProvider1.SetError(DP_WDP.cbxAdmrCode, "Please select an ADMR Code")
                bStatus = False
            Else
                If DP_WDP.cbxAdmrCode.Text = "N/A" And DP_WDP.cbxPayment_Department.Text = "" Then
                    DP_WDP.ErrorProvider1.SetError(DP_WDP.cbxAdmrCode, "Please select an ADMR Code")
                    bStatus = False
                Else
                    DP_WDP.ErrorProvider1.SetError(DP_WDP.cbxAdmrCode, "")
                End If
            End If
            Return bStatus
        End Function

        Public Shared Function ValidateHTR() As Boolean
            Dim bStatus As Boolean = True
            If Microsoft.VisualBasic.Left(DP_WDP.cbxAdmrCode.Text, 3) = "HTR" Then
                'Must select item from school or can not find school list
                'If there are no items in the High School drop down check to see if item from no school selected
                If DP_WDP.cbxCeebName.Items.Count() = 0 Then
                    If DP_WDP.cbxNoSchool.Items.Count() = 0 Then
                        DP_WDP.ErrorProvider1.SetError(DP_WDP.cbxCeebName, "Please select a school")
                        DP_WDP.ErrorProvider1.SetError(DP_WDP.cbxNoSchool, "Please select a school")
                        bStatus = False
                    ElseIf DP_WDP.cbxNoSchool.SelectedValue.ToString() = "" Then
                        DP_WDP.ErrorProvider1.SetError(DP_WDP.cbxCeebName, "Please select a school")
                        DP_WDP.ErrorProvider1.SetError(DP_WDP.cbxNoSchool, "Please select a school")
                        bStatus = False
                    Else
                        DP_WDP.ErrorProvider1.SetError(DP_WDP.cbxCeebName, "")
                        DP_WDP.ErrorProvider1.SetError(DP_WDP.cbxNoSchool, "")
                    End If
                Else
                    If IsDBNull(DP_WDP.cbxCeebName.SelectedItem) And DP_WDP.cbxNoSchool.SelectedValue.ToString() = "" Then
                        DP_WDP.ErrorProvider1.SetError(DP_WDP.cbxCeebName, "Please select a school")
                        bStatus = False
                    Else
                        DP_WDP.ErrorProvider1.SetError(DP_WDP.cbxCeebName, "")
                        DP_WDP.ErrorProvider1.SetError(DP_WDP.cbxNoSchool, "")
                    End If
                End If
                'An ADMR comment is required
                If DP_WDP.txtAdmrComment.Text = "" Then
                    DP_WDP.ErrorProvider1.SetError(DP_WDP.txtAdmrComment, "Please enter an ADMR Comment")
                    bStatus = False
                Else
                    DP_WDP.ErrorProvider1.SetError(DP_WDP.txtAdmrComment, "")
                End If
                If DP_WDP.dtHSGradDate.Checked Then
                    DP_WDP.ErrorProvider1.SetError(DP_WDP.dtHSGradDate, "")
                Else
                    '//ds 9/8/14 - commented out. grad date no longer required, //ds 10/28 - put back in as requested
                    DP_WDP.ErrorProvider1.SetError(DP_WDP.dtHSGradDate, "Please enter a graduation date")
                    bStatus = False
                End If
            Else
                DP_WDP.ErrorProvider1.SetError(DP_WDP.dtHSGradDate, "")
                DP_WDP.ErrorProvider1.SetError(DP_WDP.txtAdmrComment, "")
                DP_WDP.ErrorProvider1.SetError(DP_WDP.cbxCeebName, "")
                DP_WDP.ErrorProvider1.SetError(DP_WDP.cbxNoSchool, "")
            End If
            Return bStatus
        End Function

        Public Shared Function ValidateTestScoreADMR() As Boolean
            Dim bStatus As Boolean = True
            '//ds 9/9/14 - added UT, MID and QERD to this condtion
            If Microsoft.VisualBasic.Left(DP_WDP.cbxAdmrCode.Text, 3) = "HTR" Or _
                Microsoft.VisualBasic.Left(DP_WDP.cbxAdmrCode.Text, 4) = "FTRN" Or _
                Microsoft.VisualBasic.Left(DP_WDP.cbxAdmrCode.Text, 3) = "DTR" Or _
                Microsoft.VisualBasic.Left(DP_WDP.cbxAdmrCode.Text, 4) = "TEST" Or _
                Microsoft.VisualBasic.Left(DP_WDP.cbxAdmrCode.Text, 2) = "UT" Or _
                Microsoft.VisualBasic.Left(DP_WDP.cbxAdmrCode.Text, 3) = "MID" Or _
                Microsoft.VisualBasic.Left(DP_WDP.cbxAdmrCode.Text, 4) = "QGRD" Or _
                Microsoft.VisualBasic.Left(DP_WDP.cbxAdmrCode.Text, 4) = "IELT" Then
                'Test received must be marked with a Yes or No
                If Not DP_WDP.rdlTest_Yes.Checked And Not DP_WDP.rdlTest_No.Checked Then
                    DP_WDP.ErrorProvider1.SetError(DP_WDP.pnlTestScores, "Is a test score listed on the image?")
                    bStatus = False
                Else
                    DP_WDP.ErrorProvider1.SetError(DP_WDP.pnlTestScores, "")
                End If
            Else
                DP_WDP.ErrorProvider1.SetError(DP_WDP.pnlTestScores, "")
            End If
            Return bStatus
        End Function

        Public Shared Function ValidateCTR() As Boolean
            Dim bStatus As Boolean = True
            If Microsoft.VisualBasic.Left(DP_WDP.cbxAdmrCode.Text, 3) = "CTR" Then
                'Must select item from school or can not find school list
                'If there are no items in the College drop down check to see if item from no school selected

                If DP_WDP.cbxCeebName.Items.Count() = 0 Then
                    If DP_WDP.cbxNoSchool.Items.Count() = 0 Then
                        DP_WDP.ErrorProvider1.SetError(DP_WDP.cbxCeebName, "Please select a college")
                        DP_WDP.ErrorProvider1.SetError(DP_WDP.cbxNoSchool, "Please select a college")
                        bStatus = False
                    ElseIf DP_WDP.cbxNoSchool.SelectedValue.ToString() = "" Then
                        DP_WDP.ErrorProvider1.SetError(DP_WDP.cbxCeebName, "Please select a college")
                        DP_WDP.ErrorProvider1.SetError(DP_WDP.cbxNoSchool, "Please select a college")
                        bStatus = False
                    Else
                        DP_WDP.ErrorProvider1.SetError(DP_WDP.cbxCeebName, "")
                        DP_WDP.ErrorProvider1.SetError(DP_WDP.cbxNoSchool, "")
                    End If
                Else
                    If IsDBNull(DP_WDP.cbxCeebName.SelectedItem) And DP_WDP.cbxNoSchool.SelectedValue.ToString() = "" Then
                        DP_WDP.ErrorProvider1.SetError(DP_WDP.cbxCeebName, "Please select a college")
                        bStatus = False
                    Else
                        DP_WDP.ErrorProvider1.SetError(DP_WDP.cbxCeebName, "")
                        DP_WDP.ErrorProvider1.SetError(DP_WDP.cbxNoSchool, "")
                    End If
                End If
                'An ADMR comment is required
                If DP_WDP.txtAdmrComment.Text = "" Then
                    DP_WDP.ErrorProvider1.SetError(DP_WDP.txtAdmrComment, "Please enter an ADMR Comment")
                    bStatus = False
                Else
                    DP_WDP.ErrorProvider1.SetError(DP_WDP.txtAdmrComment, "")
                End If
            Else
                DP_WDP.ErrorProvider1.SetError(DP_WDP.txtAdmrComment, "")
                DP_WDP.ErrorProvider1.SetError(DP_WDP.cbxCeebName, "")
                DP_WDP.ErrorProvider1.SetError(DP_WDP.cbxNoSchool, "")
            End If
            Return bStatus
        End Function

        Public Shared Function ValidateBannerID() As Boolean
            Dim bStatus As Boolean = True
            If IsNumeric(Trim(DP_WDP.txtBannerID.Text)) Then
                If Microsoft.VisualBasic.Left(Trim(DP_WDP.txtBannerID.Text), 1) = 0 Then
                    DP_WDP.ErrorProvider1.SetError(DP_WDP.txtBannerID, "Please enter a valid Banner ID")
                    bStatus = False
                Else
                    Dim BannerID As Integer = Trim(DP_WDP.txtBannerID.Text)
                    Select Case BannerID
                        Case Is < 10000000
                            DP_WDP.ErrorProvider1.SetError(DP_WDP.txtBannerID, "Please enter a valid Banner ID")
                            bStatus = False
                        Case Is > 69999999
                            DP_WDP.ErrorProvider1.SetError(DP_WDP.txtBannerID, "Please enter a valid Banner ID")
                            bStatus = False
                        Case Else
                            DP_WDP.ErrorProvider1.SetError(DP_WDP.txtBannerID, "")
                    End Select
                End If

            Else
                DP_WDP.ErrorProvider1.SetError(DP_WDP.txtBannerID, "Please enter a Banner ID")
                bStatus = False
            End If
            Return bStatus
        End Function
    End Class
End Namespace
