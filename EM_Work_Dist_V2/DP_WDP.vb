﻿Imports System.IO
Imports System.Drawing.Imaging
Imports System.Threading
Imports System.Threading.Thread
Imports System.Text.RegularExpressions
Imports System.Reflection
Imports System.Security.Principal
Imports System.Collections
Imports System.Data
Imports System.Data.SqlClient
Imports System.Data.SqlTypes
Imports System
Imports System.Collections.Generic
Imports System.ComponentModel
Imports System.Drawing
Imports System.Linq
Imports System.Text
Imports System.Windows.Forms
Imports System.Runtime.Remoting.Messaging


Namespace DataProcessing
    Public Class DP_WDP


        Dim File_Queue As DataSet
        Dim Current_File() As String = Nothing ' (0) = full file path, (1) = ImageID, (2) = Filename, (3) = Image_Queues_ID
        Dim EM_WD_Users_ID As Integer ' The ID of the User populated on form load
        Dim EM_WD_Team_ID As Integer ' The ID of the User's team populated on form load
        Dim EM_WD_Team_Leader As Boolean = False
        Dim EM_WD_Interface_ID As Integer = 2
        Dim dtImages As DataTable
        Dim Contact_Code As Object = Nothing
        Dim dtImage_Info As DataTable

        Dim dtCEEBCode As dsCEEB_Codes.EM_WD_GetSchoolsDataTable
        Dim dtADMRCode As dsADMR_Codes.OA_Banner_ADMR_selDataTable

        Public Test_Score_Form As Test_Scores

        Private Sub txtMoveNext_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtSSN2.TextChanged, txtSSN1.TextChanged
            Dim CurrentControl As Control = Me.ActiveControl
            If TypeOf CurrentControl Is TextBox Then
                'Move to the next control when the max length is reached
                Dim t As TextBox

                t = DirectCast(Me.ActiveControl, TextBox)

                If t.TextLength = t.MaxLength Then
                    Me.SelectNextControl(Me.ActiveControl, True, True, True, False)
                End If
            End If
        End Sub

        Private Sub Form1_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

            'TODO: This line of code loads data into the 'DsTest_Codes.OA_Banner_TESC_sel' table. You can move, or remove it, as needed.
            'Me.OA_Banner_TESC_selTableAdapter.Fill(Me.DsTest_Codes.OA_Banner_TESC_sel)

            'Load the selected item for App sequence number combo boxes
            cbxSeq1.SelectedIndex = 0
            cbxSeq2.SelectedIndex = 0
            cbxSeq3.SelectedIndex = 0
            cbxGender.SelectedIndex = 0
            rdlCommentType_EM.Checked = True
            ErrorProvider1.ContainerControl = Me

            '//ds 9/18/14 - TEMPORARILY TOOK THIS CODE OUT
            'Check to see if the form is active or the version is the same
            If Not Form_Active() Then
                MessageBox.Show("The current form is disabled or you are using an older version.")
                'Close the form
                Me.DialogResult = DialogResult.Cancel
                Exit Sub
            End If

            Setup_User_ID()

            'If no user is found close the form
            If EM_WD_Users_ID = 0 Then
                'Close the form
                Me.Close()
                Exit Sub
            End If

            Setup_Team_Leader()
            'Disable the submit and help buttons
            'Disable feedme, disable submit
            Enable_Disable_Buttons(True, False)

            Setup_Contacts()
            Setup_Attributes()
            Setup_Interests()
            Setup_Source()
            Setup_ADMR_DS()
            Setup_CEEB_DS()
            Setup_Previous_Docs()

            Setup_Problem_Types()

            cbxNoSchool.Items.Clear()

            'Output the version to a label on the form
            lblVersion.Text = Application.ProductVersion

            'Update the label with the count of the documents being processed
            lblImages_Processed.Text = GetDailyProcessCount()

            ResetAllControls(Me)

        End Sub

        Private Function Form_Active() As Boolean
            Dim bStatus As Boolean = False

            Try
                'Get the form version from the database
                Dim dtForm_Check As DataTable
                Dim TA_Form_Check As New dsCheck_Form_StatusTableAdapters.EM_WD_Check_Form_StatusTableAdapter
                dtForm_Check = TA_Form_Check.GetData_Form_Status(Application.ProductName, _
                                                                 Application.ProductVersion)
                If dtForm_Check.Rows.Count = 0 Then
                    'There are no forms with the current version.  
                    bStatus = False
                Else
                    bStatus = dtForm_Check.Rows(0).Item(0).ToString
                End If
            Catch ex As Exception
                MessageBox.Show(ex.ToString)
            End Try
            Return bStatus

        End Function

        Private Sub cbxSequence_Number_Validating(ByVal sender As Object, ByVal e As System.ComponentModel.CancelEventArgs) Handles cbxSeq1.Validating
            Validation.ValidateSequence_Number()
        End Sub

        Private Sub EM_EMO_Comment_Validating(ByVal sender As Object, ByVal e As System.ComponentModel.CancelEventArgs) Handles rdlCommentType_EM.Validating, rdlCommentType_EMO.Validating, txtGeneralComment.Validating
            Validation.ValidateEM_EMO_Comment()
        End Sub

        Private Sub txt3DigitNumber_SSN_Validating(ByVal sender As Object, ByVal e As System.ComponentModel.CancelEventArgs) Handles txtSSN1.Validating
            Validation.ValidateSSN_3Digit()
        End Sub

        Private Sub txt3DigitNumber_Validating(ByVal sender As Object, ByVal e As System.ComponentModel.CancelEventArgs) Handles txtSSN1.Validating
            Validation.ValidatePhone_SSN_3Digit(sender.Text, sender)
        End Sub

        Private Sub txt2DigitNumber_SSN_Validating(ByVal sender As Object, ByVal e As System.ComponentModel.CancelEventArgs) Handles txtSSN2.Validating
            Validation.ValidateSSN_2Digit()
        End Sub

        Private Sub txt4DigitNumber_SSN_Validating(ByVal sender As Object, ByVal e As System.ComponentModel.CancelEventArgs) Handles txtSSN3.Validating
            Validation.ValidateSSN_4Digit()
        End Sub

        Private Sub txtRIC_Validating(ByVal sender As Object, ByVal e As System.ComponentModel.CancelEventArgs) Handles txtHS_RIC.Validating
            Validation.ValidateRIC()
        End Sub

        Private Sub txtClassSize_Validating(ByVal sender As Object, ByVal e As System.ComponentModel.CancelEventArgs) Handles txtHS_ClassSize.Validating
            Validation.ValidateClass_Size()
        End Sub

        Private Sub txtGPA_Validating(ByVal sender As Object, ByVal e As System.ComponentModel.CancelEventArgs) Handles txtHS_GPA.Validating
            Validation.ValidateGPA_Decimal2_1()
        End Sub

        Private Sub txtBannerID_Validating(ByVal sender As Object, ByVal e As System.ComponentModel.CancelEventArgs) Handles txtBannerID.Validating
            Validation.ValidateBannerID()
        End Sub

        Private Sub dtDOB_Validating(ByVal sender As Object, ByVal e As System.ComponentModel.CancelEventArgs) Handles dtDOB.Validating
            Validation.ValidateDOB()
        End Sub

        Private Sub txtADMRComment_Validating(ByVal sender As Object, ByVal e As System.ComponentModel.CancelEventArgs) Handles txtAdmrComment.Validating, cbxAdmrCode.Validating
            Validation.ValidateADMRComment(getOA_Banner_ADMR_Comment_Required(), getOA_Banner_EMO_Comment_Required())
        End Sub

        Private Sub cbxPayment_Type_Validating(ByVal sender As Object, ByVal e As System.ComponentModel.CancelEventArgs) Handles cbxPayment_Type.Validating
            Validation.ValidatecbxPayment_Type()
        End Sub

        Public Function getOA_Banner_ADMR_Comment_Required() As Boolean
            Dim OA_Banner_ADMR_ID As Integer = cbxAdmrCode.SelectedValue.ToString()
            Dim foundrows() As Data.DataRow = dtADMRCode.Select("OA_Banner_ADMR_ID = " & OA_Banner_ADMR_ID)

            Return foundrows(0)("OA_Banner_ADMR_Comment_Required").ToString()
        End Function

        Public Function getOA_Banner_EMO_Comment_Required() As Boolean
            Dim OA_Banner_ADMR_ID As Integer = cbxAdmrCode.SelectedValue.ToString()
            Dim foundrows() As Data.DataRow = dtADMRCode.Select("OA_Banner_ADMR_ID = " & OA_Banner_ADMR_ID)

            Return foundrows(0)("OA_Banner_EMO_Comment_Required").ToString()
        End Function

        Private Function ValidateForm() As Boolean
            Dim bStatus As Boolean = True

            Dim bValidCTR As Boolean = Validation.ValidateCTR
            Dim bValidDOB As Boolean = Validation.ValidateDOB
            Dim bValidGPA As Boolean = Validation.ValidateGPA_Decimal2_1
            Dim bValidHTR As Boolean = Validation.ValidateHTR
            Dim bValidTestScoreADMR As Boolean = Validation.ValidateTestScoreADMR
            Dim bValidSSN_4Digit As Boolean = Validation.ValidateSSN_4Digit
            Dim bValidSSN_2Digit As Boolean = Validation.ValidateSSN_2Digit
            Dim bValidSSN_3Digit As Boolean = Validation.ValidateSSN_3Digit
            Dim bValidBannerID As Boolean = Validation.ValidateBannerID
            Dim bValidRIC As Boolean = Validation.ValidateRIC
            Dim bValidClassSize As Boolean = Validation.ValidateClass_Size
            Dim bValidExceptionText As Boolean = Validation.ValidateException_Text
            Dim bValidEM_EMO_Comment As Boolean = Validation.ValidateEM_EMO_Comment

            Dim File_Category As String = String.Empty
            If Not Current_File Is Nothing Then
                File_Category = Microsoft.VisualBasic.Mid(Current_File(2), 3, 2)
            End If
            'Check to make sure a file is selected to be processed
            If Current_File Is Nothing Then
                bStatus = False
                ErrorProvider1.SetError(dgImages, "Please select a row.")
            Else
                ErrorProvider1.SetError(dgImages, "")
            End If

            Dim bValidADMRComment As Boolean = Validation.ValidateADMRComment(getOA_Banner_ADMR_Comment_Required(), getOA_Banner_EMO_Comment_Required())
            Dim bValidSequence_Number As Boolean = Validation.ValidateSequence_Number()

            'Check to see if the file is an inquiry.  If it is then sequence number and admr are not required.
            If File_Category = "IN" Then
                bValidADMRComment = True
                ErrorProvider1.SetError(cbxSeq1, "")
                bValidSequence_Number = True
                ErrorProvider1.SetError(cbxAdmrCode, "")
            End If

            Dim bValidPayment_Type As Boolean = True
            Dim bValidPayment_Method As Boolean = True

            'If a payment department has been selected then we are processing a matric payment
            If cbxPayment_Department.Text <> "" Then
                bValidPayment_Type = Validation.ValidatePayment_Type
                bValidPayment_Method = Validation.ValidatePayment_Method
            End If

            'Check all of the fields to make sure they are valid
            If bValidCTR AndAlso bValidDOB AndAlso bValidGPA AndAlso bValidHTR AndAlso bValidTestScoreADMR AndAlso bValidSSN_4Digit _
                AndAlso bValidSSN_2Digit AndAlso bValidSSN_3Digit _
                AndAlso bValidBannerID AndAlso bValidADMRComment AndAlso bValidSequence_Number AndAlso bValidClassSize AndAlso bValidRIC _
                AndAlso bValidPayment_Type AndAlso bValidPayment_Method AndAlso bValidExceptionText AndAlso bValidEM_EMO_Comment Then
            Else
                bStatus = False
            End If

            Return bStatus
        End Function

        Private Function ValidateUnclaimed() As Boolean
            Dim bStatus As Boolean = True

            Dim bValidCTR As Boolean = Validation.ValidateCTR
            Dim bValidDOB As Boolean = Validation.ValidateDOB
            Dim bValidHTR As Boolean = Validation.ValidateHTR
            Dim bValidTestScoreADMR As Boolean = Validation.ValidateTestScoreADMR
            Dim bValidUnclaimed As Boolean = Validation.ValidateUnclaimeds
            Dim bValidEM_EMO_Comment As Boolean = Validation.ValidateEM_EMO_Comment
            Dim bValidADMRComment As Boolean = Validation.ValidateADMRComment(getOA_Banner_ADMR_Comment_Required(), getOA_Banner_EMO_Comment_Required())

            'Check to make sure a file is selected to be processed
            If Current_File Is Nothing Then
                ErrorProvider1.SetError(Image_Viewer, "File missing")
                bStatus = False
            Else
                ErrorProvider1.SetError(Image_Viewer, "")
            End If

            'Check all of the fields to make sure they are valid
            If bValidUnclaimed AndAlso bValidCTR AndAlso bValidDOB AndAlso bValidHTR AndAlso bValidTestScoreADMR AndAlso bValidADMRComment AndAlso bValidEM_EMO_Comment Then
            Else
                bStatus = False
            End If

            Return bStatus
        End Function

        Private Function ValidateHS_CollegeSearch() As Boolean
            Dim bStatus As Boolean = True

            Dim bValidHS_CollegeSearch As Boolean = Validation.ValidateHS_CollegeSearch

            'Check all of the fields to make sure they are valid
            If Not bValidHS_CollegeSearch Then
                bStatus = False
            End If

            Return bStatus
        End Function

        Private Function ValidateException() As Boolean
            Dim bStatus As Boolean = True

            Dim bValidException As Boolean = Validation.ValidateExceptions

            'Check to make sure a file is selected to be processed
            If Current_File Is Nothing Then
                ErrorProvider1.SetError(Image_Viewer, "File missing")
                bStatus = False
            Else
                ErrorProvider1.SetError(Image_Viewer, "")
            End If

            'Check all of the fields to make sure they are valid
            If Not bValidException Then
                bStatus = False
            End If

            Return bStatus
        End Function

        Private Function ValidateProblem() As Boolean
            Dim bStatus As Boolean = True

            Dim bValidProblem As Boolean = Validation.ValidateProblems

            'Check to make sure a file is selected to be processed
            If Current_File Is Nothing Then
                ErrorProvider1.SetError(Image_Viewer, "File missing")
                bStatus = False
            Else
                ErrorProvider1.SetError(Image_Viewer, "")
            End If

            'Check all of the fields to make sure they are valid
            If Not bValidProblem Then
                bStatus = False
            End If

            Return bStatus
        End Function

        Public Function Convert_To_CDL(ByVal Text1 As String, ByVal Text2 As String, ByVal Text3 As String) As Object
            'Convert up to four strings into on comma delimited string
            Dim NewText As Object = Nothing
            'Check to make sure defaults are not selected
            If Text1 <> "N/A" And Text1 <> "NA" Then
                NewText = Text1
            End If
            'Check to make sure defaults or the same value are not selected
            If Text2 <> "N/A" And Text2 <> "NA" And Text2 <> Text1 Then
                If NewText = Nothing Then
                    NewText = Text2
                Else
                    NewText = NewText.ToString & "," & Text2
                End If
            End If
            'Check to make sure defaults or the same value are not selected
            If Text3 <> "N/A" And Text3 <> "NA" And Text3 <> Text1 And Text3 <> Text2 Then
                If NewText = Nothing Then
                    NewText = Text3
                Else
                    NewText = NewText.ToString & "," & Text3
                End If
            End If
            Return NewText
        End Function

        Public Function Check_Text_NA(ByVal Text As Object, ByVal Form As Object) As Object
            'Convert up to four strings into on comma delimited string
            Dim Output As Object = Nothing

            If Trim(Text) = "N/A" Or Text = Nothing Or Trim(Text) = "" Or Trim(Text) = "NA" Then
                Output = Nothing
            Else
                Output = Trim(Text)
            End If

            Return Output
        End Function

        Public Function Get_CEEB() As Object
            'Determine which CEEB Code was selected for the High School
            Dim CEEB As Object = Nothing
            If cbxNoSchool.Items.Count > 0 Then
                If cbxNoSchool.SelectedValue.ToString() <> "" Then
                    CEEB = cbxNoSchool.SelectedValue.ToString()
                ElseIf Not cbxCeebName.SelectedItem Is Nothing Then
                    CEEB = cbxCeebName.SelectedValue.ToString()
                End If
            End If
            Return CEEB
        End Function

        Public Function ConvertGPA(ByVal Incoming_GPA As String) As Decimal
            'Determine which CEEB Code was selected for the High School
            Dim GPA As Decimal = 0
            'Check to make sure a school was selected to enter the GPA and the GPA field is not null
            'If Not cbxHSCeebName.SelectedItem Is Nothing And Not IsDBNull(Check_Text_NA(Incoming_GPA)) Then
            If Check_Text_NA(Incoming_GPA, "Incoming_GPA") <> Nothing Then
                GPA = Incoming_GPA
                'If Incoming_GPA > 20 Then
                '    GPA = 4 - (((((txtHS_Grade.Text - 60) * 0.300001) + 93) - Incoming_GPA) * 0.1)
                'ElseIf chbxHS_Zeke.Checked Then
                '    GPA = Incoming_GPA
                'Else
                '    GPA = (4 / txtHS_Scale.Text) * Incoming_GPA
                'End If
            End If
            'The highest GPA is a 4.30
            If GPA > 4.3 Then
                GPA = 4.3
            End If

            Return GPA
        End Function

        Sub Setup_Team_Leader()
            'If the user is a team leader, hide the priority radio button and show payment box
            If EM_WD_Team_Leader Then
                gbx_Payments.Visible = True
            Else
                gbx_Payments.Visible = False
            End If
        End Sub

        Sub Setup_Contacts()
            Try
                Dim TA_ContactCode As New dsContact_CodesTableAdapters.OA_Banner_Contact_selTableAdapter
                Dim dtContactCode As dsContact_Codes.OA_Banner_Contact_selDataTable

                dtContactCode = TA_ContactCode.GetData_Contact_Codes

                'Add N/A option
                dtContactCode.Rows.Add(New Object() {"0", "N/A", "N/A", False, False})

                'Sort the view so the N/A is sorted
                Dim dView As New DataView(dtContactCode)

                dView.Sort = "OA_Banner_Contact_Code ASC"

                cbxContactCode.DataSource = dView
                cbxContactCode.DisplayMember = "OA_Banner_Contact_Code"
                cbxContactCode.ValueMember = "OA_Banner_Contact_Code"

                'Have the N/A option selected as the default
                cbxContactCode.Text = "N/A"
            Catch ex As Exception
                MessageBox.Show(ex.ToString)
            End Try
        End Sub

        Sub Setup_Problem_Types()
            Try
                Dim TA_Problem_Types As New dsProblem_TypesTableAdapters.EM_WD_GetImage_Problem_TypesTableAdapter
                Dim dtProblem_Types As dsProblem_Types.EM_WD_GetImage_Problem_TypesDataTable
                dtProblem_Types = TA_Problem_Types.GetData_Problem_Types(EM_WD_Users_ID, EM_WD_Interface_ID)

                cbxProblem_Type.DataSource = dtProblem_Types
                cbxProblem_Type.DisplayMember = "Image_Problem_Type_Desc"
                cbxProblem_Type.ValueMember = "Image_Problem_Type_ID"

                cbxProblem_Type.SelectedValue = 0
            Catch ex As Exception
                MessageBox.Show(ex.ToString)
            End Try
        End Sub

        Sub Setup_Source()
            Try
                Dim TA_SourceCode As New dsSource_CodesTableAdapters.OA_Banner_Source_selTableAdapter
                Dim dtSourceCode As dsSource_Codes.OA_Banner_Source_selDataTable

                dtSourceCode = TA_SourceCode.GetData_Source_Codes

                'Add N/A option
                dtSourceCode.Rows.Add(New Object() {"N/A", "N/A"})

                'Sort the view so the N/A is sorted
                Dim dView As New DataView(dtSourceCode)

                dView.Sort = "Banner_Code ASC"

                cbxSourceCode.DataSource = dView
                cbxSourceCode.DisplayMember = "Banner_Code"
                cbxSourceCode.ValueMember = "Banner_Code"

                'Have the N/A option selected as the default
                cbxSourceCode.Text = "N/A"
            Catch ex As Exception
                MessageBox.Show(ex.ToString)
            End Try
        End Sub

        Sub Setup_CEEB_DS()
            Try
                Dim TA_CEEBCode As New dsCEEB_CodesTableAdapters.EM_WD_GetSchoolsTableAdapter

                dtCEEBCode = TA_CEEBCode.GetData_CEEB_Codes
            Catch ex As Exception
                MessageBox.Show(ex.ToString)
            End Try
        End Sub

        Sub Load_Image_Info(ByVal Image_Queue_Type_ID As Integer, ByVal Image_Family As String)

            Try
                Dim TA_Image_Details As New dsImage_DetailsTableAdapters.EM_WD_GetImages_Details_By_FamilyTableAdapter
                Dim dtImage_Details As dsImage_Details.EM_WD_GetImages_Details_By_FamilyDataTable
                dtImage_Details = TA_Image_Details.GetData_Images_Details_By_Family(EM_WD_Users_ID, Image_Family, EM_WD_Interface_ID, Image_Queue_Type_ID)
                dtImage_Info = dtImage_Details
            Catch ex As Exception
                MessageBox.Show(ex.ToString)
            End Try

            If dtImage_Info.Rows.Count = 0 Then
                'dgImages.Visible = False
                MessageBox.Show("There was a problem getting the image information, form will not be prepopulated.")
            End If
        End Sub

        Sub Load_File_Queue(ByVal Image_Queue_Type_ID As Integer)
            Try
                dgImages.Columns.Clear()

                Dim TA_General_Queue As New dsGeneral_ImagesTableAdapters.EM_WD_GetImagesTableAdapter
                Dim dtGeneral_Queue As dsGeneral_Images.EM_WD_GetImagesDataTable
                dtGeneral_Queue = TA_General_Queue.GetData_General_Images(EM_WD_Users_ID, EM_WD_Team_ID, EM_WD_Interface_ID, Image_Queue_Type_ID)

                dtImages = dtGeneral_Queue


                If dtImages.Rows.Count > 0 Then
                    'There are images in the queue to be processed for this user
                    dgImages.Visible = True
                    dgImages.DataSource = dtImages

                    dgImages.Columns(0).Visible = False ' Hide the Image_Queues_ID
                    dgImages.Columns(3).Visible = False ' Hide the full path
                    dgImages.Columns(4).Visible = False ' Hide the Family Count
                    dgImages.Columns(6).Visible = False ' Hide the Image_Size
                    dgImages.Columns(7).Visible = False ' Hide the Image Family

                    dgImages.Columns(1).Width = 60 ' Image_ID
                    dgImages.Columns(2).Width = 250 ' Filename
                    dgImages.Columns(5).Width = 125 ' Date

                    'Disable feedme, enable submit
                    Enable_Disable_Buttons(False, True)

                    'Load the image details into the datatable
                    'Queue Type = 1
                    Load_Image_Info(Image_Queue_Type_ID, dgImages.Rows(0).Cells("Image_Family").Value.ToString)
                Else
                    dgImages.Visible = False
                    MessageBox.Show("There are no files which need to be processed.")
                End If
            Catch ex As Exception
                MessageBox.Show(ex.ToString)
            End Try
        End Sub

        Sub Load_Exception_Queue()
            Try
                dgImages.Columns.Clear()

                Dim TA_Exception_Queue As New dsException_ImagesTableAdapters.EM_WD_GetImages_ExceptionsTableAdapter
                Dim dtException_Queue As dsException_Images.EM_WD_GetImages_ExceptionsDataTable
                dtException_Queue = TA_Exception_Queue.GetData_Exception_Images(EM_WD_Users_ID, EM_WD_Team_ID, EM_WD_Interface_ID)

                dtImages = dtException_Queue


                If dtImages.Rows.Count > 0 Then
                    'There are images in the queue to be processed for this user
                    dgImages.Visible = True
                    dgImages.DataSource = dtImages

                    dgImages.Columns(0).Visible = False ' Hide the Image_Queues_ID
                    dgImages.Columns(3).Visible = False ' Hide the full path
                    dgImages.Columns(4).Visible = False ' Hide the Image DP Comment
                    dgImages.Columns(5).Visible = False ' Hide the Comment User
                    dgImages.Columns(6).Visible = False ' Hide the Image Family

                    dgImages.Columns(1).Width = 60 ' Image_ID
                    dgImages.Columns(2).Width = 250 ' Filename

                    'Disable feedme, enable submit
                    Enable_Disable_Buttons(False, True)

                    'Load the image details into the datatable
                    'Queue Type = 3
                    Load_Image_Info(3, dgImages.Rows(0).Cells("Image_Family").Value.ToString)
                Else
                    dgImages.Visible = False
                    MessageBox.Show("There are no files which need to be processed.")
                End If
            Catch ex As Exception
                MessageBox.Show(ex.ToString)
            End Try
        End Sub

        Sub Load_Problem_Queue()
            Try
                dgImages.Columns.Clear()

                Dim TA_Problem_Queue As New dsProblem_ImagesTableAdapters.EM_WD_GetImages_ProblemsTableAdapter
                Dim dtProblem_Queue As dsProblem_Images.EM_WD_GetImages_ProblemsDataTable
                dtProblem_Queue = TA_Problem_Queue.GetData_Problem_Images(EM_WD_Users_ID, EM_WD_Team_ID, EM_WD_Interface_ID, False)

                dtImages = dtProblem_Queue


                If dtImages.Rows.Count > 0 Then
                    'There are images in the queue to be processed for this user
                    dgImages.Visible = True
                    dgImages.DataSource = dtImages

                    dgImages.Columns(0).Visible = False ' Hide the Image_Queues_ID
                    dgImages.Columns(3).Visible = False ' Hide the full path
                    dgImages.Columns(4).Visible = False ' Hide the Problems_Comment
                    dgImages.Columns(5).Visible = False ' Hide the Problem username
                    dgImages.Columns(6).Visible = False ' Hide the Image Family
                    dgImages.Columns(7).Visible = False ' Hide the Family Count
                    dgImages.Columns(8).Visible = False ' Hide the Problem Type Tab
                    dgImages.Columns(9).Visible = False ' Hide the Problem Type ID

                    dgImages.Columns(1).Width = 60 ' Image_ID
                    dgImages.Columns(2).Width = 250 ' Filename

                    'Disable feedme, enable submit
                    Enable_Disable_Buttons(False, True)

                    'Load the image details into the datatable
                    'Queue Type = 2
                    Load_Image_Info(2, dgImages.Rows(0).Cells("Image_Family").Value.ToString)
                Else
                    dgImages.Visible = False
                    MessageBox.Show("There are no files which need to be processed.")
                End If
            Catch ex As Exception
                MessageBox.Show(ex.ToString)
            End Try
        End Sub

        Sub Setup_User_ID()
            Try
                'Setup the User ID
                Dim dtUsers_ID As DataTable
                Dim TA_Users_ID As New dsUser_IDTableAdapters.EM_WD_GetUsers_by_UsernameTableAdapter
                dtUsers_ID = TA_Users_ID.GetData_User_ID(Environment.UserName)
                If dtUsers_ID.Rows.Count = 0 Then
                    'There are no users with that ID.  Disable Feed Me button
                    'Disable feedme, disable submit
                    Enable_Disable_Buttons(False, False)

                    MessageBox.Show("Please contact your supervisor to have your ID setup in the system.")
                Else
                    EM_WD_Users_ID = dtUsers_ID.Rows(0).Item(0).ToString
                    EM_WD_Team_ID = dtUsers_ID.Rows(0).Item(1).ToString

                    'EM_WD_Users_ID = 11
                    'EM_WD_Team_ID = 8

                    If Not IsDBNull(dtUsers_ID.Rows(0).Item(3).ToString) Then
                        EM_WD_Team_Leader = True
                    End If

                    'Enable feedme, disable submit
                    Enable_Disable_Buttons(True, False)
                End If
            Catch ex As Exception
                MessageBox.Show(ex.ToString)
            End Try
        End Sub

        Sub Setup_ADMR_DS()
            Try
                Dim TA_ADMRCode As New dsADMR_CodesTableAdapters.OA_Banner_ADMR_selTableAdapter
                'Dim dtADMRCode As dsADMR_Codes.OA_Banner_ADMR_selDataTable

                dtADMRCode = TA_ADMRCode.GetData_ADMR_Codes

                'Add N/A option
                dtADMRCode.Rows.Add(New Object() {"0", "N/A", "N/A", False, False, False})

                'Sort the view so the N/A is sorted
                Dim dView As New DataView(dtADMRCode)

                dView.Sort = "OA_Banner_ADMR_Code ASC"

                'ADMR
                cbxAdmrCode.DataSource = dView
                cbxAdmrCode.DisplayMember = "OA_Banner_ADMR_Code"
                cbxAdmrCode.ValueMember = "OA_Banner_ADMR_ID"

                'Have the N/A option selected as the default
                cbxAdmrCode.Text = "N/A"
            Catch ex As Exception
                MessageBox.Show(ex.ToString)
            End Try
        End Sub

        Sub Setup_Col_NoSchool()
            'Clear the combobox
            cbxNoSchool.DataSource = Nothing

            Dim CollegeName As New ArrayList()
            CollegeName.Add(New vpair("N/A", ""))
            CollegeName.Add(New vpair("Unknown College", "0000"))
            CollegeName.Add(New vpair("International College", "A001"))

            cbxNoSchool.Items.Clear()

            cbxNoSchool.DataSource = CollegeName
            cbxNoSchool.DisplayMember = "Text"
            cbxNoSchool.ValueMember = "Value"
        End Sub

        Sub Setup_HS_NoSchool()
            'Clear the combobox
            cbxNoSchool.DataSource = Nothing

            Dim SchoolName As New ArrayList()
            SchoolName.Add(New vpair("N/A", ""))
            SchoolName.Add(New vpair("Home Schooled", "A00001"))
            SchoolName.Add(New vpair("Unknown High School", "UNKNWN"))
            SchoolName.Add(New vpair("International High School", "AA001"))

            cbxNoSchool.Items.Clear()

            cbxNoSchool.DataSource = SchoolName
            cbxNoSchool.DisplayMember = "Text"
            cbxNoSchool.ValueMember = "Value"
        End Sub

        Sub Setup_Attributes()
            Try
                Dim TA_AttributeCode As New dsAttribute_CodesTableAdapters.OA_Banner_Attribute_sel_newTableAdapter
                Dim dtAttributeCode As dsAttribute_Codes.OA_Banner_Attribute_sel_newDataTable

                dtAttributeCode = TA_AttributeCode.GetData_Attribute_Codes

                'Add N/A option
                dtAttributeCode.Rows.Add(New Object() {"0", "N/A", "N/A", False})

                'Sort the view so the N/A is sorted
                Dim dView As New DataView(dtAttributeCode)

                dView.Sort = "OA_Banner_Attribute_Code ASC"

                'Attribute1
                cbxAttributeCode1.DataSource = dView
                cbxAttributeCode1.DisplayMember = "OA_Banner_Attribute_Code"
                cbxAttributeCode1.ValueMember = "OA_Banner_Attribute_Code"

                'Have the N/A option selected as the default
                cbxAttributeCode1.Text = "N/A"
            Catch ex As Exception
                MessageBox.Show(ex.ToString)
            End Try
        End Sub

        Sub Setup_Interests()
            Try
                Dim TA_InterestCode As New dsInterest_CodesTableAdapters.OA_Banner_Interest_selTableAdapter
                Dim dtInterestCode As dsInterest_Codes.OA_Banner_Interest_selDataTable

                dtInterestCode = TA_InterestCode.GetData_Interest_Codes

                'Add N/A option
                dtInterestCode.Rows.Add(New Object() {"0", "NA", "N/A", False})

                'Sort the view so the N/A is sorted
                Dim dView As New DataView(dtInterestCode)
                Dim dView2 As New DataView(dtInterestCode)

                'Each DDL needs its own view
                dView.Sort = "OA_Banner_Interest_Code ASC"
                dView2.Sort = "OA_Banner_Interest_Code ASC"

                'Interest1
                cbxInterestCode1.DataSource = dView
                cbxInterestCode1.DisplayMember = "OA_Banner_Interest_Code"
                cbxInterestCode1.ValueMember = "OA_Banner_Interest_Code"

                'Have the N/A option selected as the default
                cbxInterestCode1.Text = "NA"

                'Interest2
                cbxInterestCode2.DataSource = dView2
                cbxInterestCode2.DisplayMember = "OA_Banner_Interest_Code"
                cbxInterestCode2.ValueMember = "OA_Banner_Interest_Code"

                'Have the N/A option selected as the default
                cbxInterestCode2.Text = "NA"

            Catch ex As Exception
                MessageBox.Show(ex.ToString)
            End Try
        End Sub

        Private Sub Setup_Previous_Docs()
            Try
                'Get the image_ID of the files in the database still assigned to the user
                dgImages.Columns.Clear()

                Dim TA_Queue_Images As New dsQueue_ImagesTableAdapters.EM_WD_GetImages_From_QueueTableAdapter
                Dim dtQueue_Images As dsQueue_Images.EM_WD_GetImages_From_QueueDataTable
                'Load from general queue (ID = 1)
                dtQueue_Images = TA_Queue_Images.GetData_Queue_Images(EM_WD_Users_ID, EM_WD_Interface_ID, 1)

                dtImages = dtQueue_Images
                dgImages.DataSource = dtImages

                If dtImages.Rows.Count > 0 Then
                    'There are images in the queue to be processed for this user
                    dgImages.Visible = True
                    'Disable the feed me get problem buttons there are docs to process
                    'Disable feedme, enable submit
                    Enable_Disable_Buttons(False, True)

                    'Load the image details into the datatable
                    'Queue Type = 2
                    Load_Image_Info(dgImages.Rows(0).Cells("Image_Queue_Type_ID").Value.ToString, dgImages.Rows(0).Cells("Image_Family").Value.ToString)
                Else
                    dgImages.Visible = False

                    'Enable feedme, disable submit
                    Enable_Disable_Buttons(True, False)
                End If
            Catch ex As Exception
                MessageBox.Show(ex.ToString)
            End Try
        End Sub

        Private Sub cbx_GPA_SelectedIndexChanged(sender As System.Object, e As System.EventArgs) Handles cbx_GPA.SelectedIndexChanged
            '0 = Enter below
            '1 = ADM
            '2 = OPS

            If cbx_GPA.SelectedIndex = 0 Then
                'Enable GPA, Rank in Class, Class Size
                txtHS_GPA.Enabled = True
                lblGPA.Enabled = True
                txtHS_RIC.Enabled = True
                lblRIC.Enabled = True
                txtHS_ClassSize.Enabled = True
                lblClassSize.Enabled = True
            ElseIf cbx_GPA.SelectedIndex > 0 Then
                'Disable GPA, Rank in Class, Class Size
                txtHS_GPA.Enabled = False
                txtHS_GPA.Text = ""
                lblGPA.Enabled = False
                txtHS_RIC.Enabled = False
                txtHS_RIC.Text = ""
                lblRIC.Enabled = False
                txtHS_ClassSize.Enabled = False
                txtHS_ClassSize.Text = ""
                lblClassSize.Enabled = False
            End If
        End Sub

        Private Sub dgImages_List_SelectionChanged(ByVal sender As Object, ByVal e As EventArgs) Handles dgImages.SelectionChanged
            If dgImages.SelectedRows.Count > 0 Then
                'A row was selected so allow the submission of the update or submission
                'Image is available to be processed
                'Disable feedme, enable submit
                Enable_Disable_Buttons(False, True)

                'The image has changed, reset the appropriate fields on the form.
                cbxAdmrCode.Text = "N/A"
                txtAdmrComment.Text = ""
                chkbxMandatory.Checked = True
                rdlCommentType_EM.Checked = False
                rdlCommentType_EMO.Checked = False
                txtGeneralComment.Text = ""
                dtDOB.Value = System.DateTime.Now
                dtDOB.Checked = False
                txtSSN1.Text = ""
                txtSSN2.Text = ""
                txtSSN3.Text = ""
                cbxGender.Text = "N/A"
                cbxSourceCode.Text = "N/A"
                cbxInterestCode1.Text = "NA"
                cbxInterestCode2.Text = "NA"
                cbxAttributeCode1.Text = "N/A"
                cbxContactCode.Text = "N/A"
                txtCEEBCode.Text = ""
                txtZipCode.Text = ""
                txtCity.Text = ""
                cbxCeebName.DataSource = Nothing
                cbxCeebName.Items.Clear()
                cbxNoSchool.DataSource = Nothing
                cbxNoSchool.Items.Clear()

                'Disable to Grad date, RIC, Class Size, GPA
                lblGradDate.Enabled = False
                dtHSGradDate.Enabled = False
                lblGPA.Enabled = False
                txtHS_GPA.Enabled = False
                cbx_GPA.Enabled = False
                lblRIC.Enabled = False
                txtHS_RIC.Enabled = False
                lblClassSize.Enabled = False
                txtHS_ClassSize.Enabled = False

                'When setting the grad date add a year if we are after August.
                Dim Current_Year As Integer = DateTime.Now.Year
                Dim Current_Month As Integer = DateTime.Now.Month
                Dim Grad_Year As Integer

                If Current_Month < 9 Then
                    Grad_Year = Current_Year
                Else
                    Grad_Year = Current_Year + 1
                End If

                dtHSGradDate.Value = New Date(Grad_Year, 6, 1)
                dtHSGradDate.Checked = False
                txtHS_RIC.Text = ""
                txtHS_ClassSize.Text = ""
                txtHS_GPA.Text = ""
                cbx_GPA.Enabled = False
                btnException_Submit.Enabled = True
                txtExceptionComment.Enabled = True
                txtExceptionComment_Follow_up.Visible = False
                lblException_Follow_Up.Visible = False
                txtExceptionComment.Text = ""

                Dim rowIndex As Integer = dgImages.CurrentCell.RowIndex 'Get the index of the current row
                Dim Image_ID As Integer = dgImages.Rows(rowIndex).Cells("Image_ID").Value.ToString
                Dim Image_Full_Path As String = dgImages.Rows(rowIndex).Cells("Image_Full_Path").Value.ToString
                Dim Image_Queues_ID As Integer = dgImages.Rows(rowIndex).Cells("Image_Queues_ID").Value.ToString
                Dim Image_Filename As String = dgImages.Rows(rowIndex).Cells("Image_Filename").Value.ToString

                ReDim Current_File(3)

                Current_File(0) = Image_Full_Path
                Current_File(1) = Image_ID
                Current_File(2) = Image_Filename
                Current_File(3) = Image_Queues_ID

                If dtImages.TableName = "EM_WD_GetImages_Exception" Then
                    Dim Image_DP_Comment As String = dgImages.Rows(rowIndex).Cells("Image_DP_Comment").Value.ToString
                    txtExceptionComment_Follow_up.Text = Image_DP_Comment

                    'Disable the entry of exception comments
                    txtExceptionComment.Enabled = False

                    'Hide the exception button
                    btnException_Submit.Visible = False

                    'Show the follow-up label and textbox
                    lblException_Follow_Up.Visible = True
                    txtExceptionComment_Follow_up.Visible = True
                Else
                    'Enable the entry of exception comments
                    txtExceptionComment.Enabled = True

                    'Show the exception button
                    btnException_Submit.Visible = True

                    'Hide the follow-up label and textbox
                    lblException_Follow_Up.Visible = False
                    txtExceptionComment_Follow_up.Visible = False
                End If

                'Prepopulate the form with data from dtImage_Info for Image_ID
                Pre_Populate_Form(Image_ID)

                Image_Viewer.Visible = True
                Image_Viewer.Navigate(New Uri(Current_File(0)))

                'If there is an error for an unselected image, clear it
                ErrorProvider1.SetError(dgImages, "")

            End If
        End Sub

        Private Sub Pre_Populate_Form(Image_ID)
            'Prepopulate the form with data from dtImage_Info for Image_ID
            Dim Image_Info_Row As DataRow

            If dtImage_Info.Rows.Count > 0 Then

                Image_Info_Row = dtImage_Info.Select("Image_ID = " & Image_ID)(0)

                'Add if statement to make sure a row is found
                If Not IsDBNull(Image_Info_Row) Then
                    If Not IsDBNull(Image_Info_Row("Banner_ID")) Then
                        txtBannerID.Text = CStr(Image_Info_Row("Banner_ID"))
                    End If

                    If Not IsDBNull(Image_Info_Row("Sequence_Number_CDL")) Then
                        'This approach works, but I wanted to find the current comboboxes, then I do not need to create the second array
                        Dim seqArr() As String
                        Dim seqArr_2(2) As String
                        seqArr_2(0) = "N/A"
                        seqArr_2(1) = "N/A"
                        seqArr_2(2) = "N/A"
                        seqArr = CStr(Image_Info_Row("Sequence_Number_CDL")).Split(",")

                        Dim Count As Integer = 0
                        For Each seq In seqArr
                            seqArr_2(Count) = seq
                            Count = Count + 1
                            'The array can only be three
                            If Count > 2 Then
                                Exit For
                            End If
                        Next
                        cbxSeq1.Text = seqArr_2(0)
                        cbxSeq2.Text = seqArr_2(1)
                        cbxSeq3.Text = seqArr_2(2)
                    End If

                    If Not IsDBNull(Image_Info_Row("ADMR_Code")) Then
                        cbxAdmrCode.Text = CStr(Image_Info_Row("ADMR_Code"))
                    End If

                    If Not IsDBNull(Image_Info_Row("ADMR_Comment")) Then
                        txtAdmrComment.Text = CStr(Image_Info_Row("ADMR_Comment"))
                    End If

                    If Not IsDBNull(Image_Info_Row("ADMR_Mandatory")) Then
                        chkbxMandatory.Checked = Image_Info_Row("ADMR_Mandatory")
                    End If

                    If Not IsDBNull(Image_Info_Row("General_Comment_Type")) Then
                        If CStr(Image_Info_Row("General_Comment_Type")) = "EM" Then
                            rdlCommentType_EM.Checked = True
                            rdlCommentType_EMO.Checked = False
                        ElseIf CStr(Image_Info_Row("General_Comment_Type")) = "EMO" Then
                            rdlCommentType_EM.Checked = False
                            rdlCommentType_EMO.Checked = True
                        Else
                            rdlCommentType_EM.Checked = False
                            rdlCommentType_EMO.Checked = False
                        End If
                    End If

                    If Not IsDBNull(Image_Info_Row("General_Comment_Text")) Then
                        txtGeneralComment.Text = CStr(Image_Info_Row("General_Comment_Text"))
                    End If

                    If Not IsDBNull(Image_Info_Row("DOB")) Then
                        dtDOB.Text = CStr(Image_Info_Row("DOB"))
                    End If

                    If Not IsDBNull(Image_Info_Row("SSN")) Then
                        txtSSN1.Text = Microsoft.VisualBasic.Left(CStr(Image_Info_Row("SSN")), 3)
                        txtSSN2.Text = Microsoft.VisualBasic.Mid(CStr(Image_Info_Row("SSN")), 4, 2)
                        txtSSN3.Text = Microsoft.VisualBasic.Right(CStr(Image_Info_Row("SSN")), 4)
                    End If

                    If Not IsDBNull(Image_Info_Row("Gender")) Then
                        cbxGender.Text = CStr(Image_Info_Row("Gender"))
                    End If

                    If Not IsDBNull(Image_Info_Row("Source_Code")) Then
                        cbxSourceCode.Text = CStr(Image_Info_Row("Source_Code"))
                    End If

                    If Not IsDBNull(Image_Info_Row("Interest_Codes_CDL")) Then

                        'This approach works, but I wanted to find the current comboboxes, then I do not need to create the second array
                        Dim intsArr() As String
                        Dim intsArr_2(2) As String
                        intsArr_2(0) = "N/A"
                        intsArr_2(1) = "N/A"
                        intsArr = CStr(Image_Info_Row("Interest_Codes_CDL")).Split(",")

                        Dim Count As Integer = 0
                        For Each ints In intsArr
                            intsArr_2(Count) = ints
                            Count = Count + 1
                            'The array can only be three
                            If Count > 1 Then
                                Exit For
                            End If
                        Next
                        cbxInterestCode1.Text = intsArr_2(0)
                        cbxInterestCode2.Text = intsArr_2(1)

                    End If

                    If Not IsDBNull(Image_Info_Row("Attribute_Codes_CDL")) Then
                        cbxAttributeCode1.Text = CStr(Image_Info_Row("Attribute_Codes_CDL"))
                    End If

                    If Not IsDBNull(Image_Info_Row("Contact_Code")) Then
                        cbxContactCode.Text = CStr(Image_Info_Row("Contact_Code"))
                    End If

                    If Not IsDBNull(Image_Info_Row("CEEB")) Then
                        txtCEEBCode.Text = CStr(Image_Info_Row("CEEB"))

                        'Need to determine if College or High School
                        Dim CEEBType As String = "H"

                        'Limit the dt table with the CEEB from the database
                        dtCEEBCode.DefaultView.RowFilter = "OA_Banner_CEEB_Code = '" & CStr(Image_Info_Row("CEEB")) & "'"

                        'Need to populate dropdown list with CEEB
                        cbxCeebName.DataSource = dtCEEBCode.DefaultView
                        cbxCeebName.DisplayMember = "OA_Banner_CEEB_Desc"
                        cbxCeebName.ValueMember = "OA_Banner_CEEB_Code"

                        'If no rows are found, default to type = "H"
                        If dtCEEBCode.DefaultView.Count = 0 Then
                            CEEBType = "H"
                        Else
                            'Set the CEEB type
                            CEEBType = dtCEEBCode.DefaultView.Item(0).Item("OA_Banner_CEEB_Type")
                        End If

                        If CEEBType = "H" Then
                            'Clear and add the High School items to the no school list
                            Setup_HS_NoSchool()

                            'Enable to Grad date, RIC, Class Size, GPA
                            lblGradDate.Enabled = True
                            dtHSGradDate.Enabled = True
                            lblGPA.Enabled = True
                            txtHS_GPA.Enabled = True
                            cbx_GPA.Enabled = True
                            lblRIC.Enabled = True
                            txtHS_RIC.Enabled = True
                            lblClassSize.Enabled = True
                            txtHS_ClassSize.Enabled = True
                        ElseIf CEEBType = "C" Then
                            'Clear and add the college items to the no school list
                            Setup_Col_NoSchool()

                            'Disable to Grad date, RIC, Class Size, GPA
                            lblGradDate.Enabled = False
                            dtHSGradDate.Enabled = False
                            lblGPA.Enabled = False
                            txtHS_GPA.Enabled = False
                            cbx_GPA.Enabled = False
                            lblRIC.Enabled = False
                            txtHS_RIC.Enabled = False
                            lblClassSize.Enabled = False
                            txtHS_ClassSize.Enabled = False
                        End If
                    End If

                    If Not IsDBNull(Image_Info_Row("Grad_Date_High_School")) Then
                        dtHSGradDate.Text = CStr(Image_Info_Row("Grad_Date_High_School"))
                    End If

                    If Not IsDBNull(Image_Info_Row("RIC_High_School")) Then
                        txtHS_RIC.Text = CStr(Image_Info_Row("RIC_High_School"))
                        txtHS_RIC.Enabled = True
                        lblRIC.Enabled = True
                    End If

                    If Not IsDBNull(Image_Info_Row("Class_Size_High_School")) Then
                        txtHS_ClassSize.Text = CStr(Image_Info_Row("Class_Size_High_School"))
                        txtHS_ClassSize.Enabled = True
                        lblClassSize.Enabled = True
                    End If

                    If Not IsDBNull(Image_Info_Row("HS_GPA")) Then
                        txtHS_GPA.Text = CStr(Image_Info_Row("HS_GPA"))
                        txtHS_GPA.Enabled = True
                        lblGPA.Enabled = True
                    End If

                    If Not IsDBNull(Image_Info_Row("Image_DP_Comment")) Then
                        'If there is an exception comment, disable the submission, and enable the entry
                        btnException_Submit.Enabled = False
                        txtExceptionComment.Enabled = False
                        txtExceptionComment_Follow_up.Visible = True
                        lblException_Follow_Up.Visible = True
                        txtExceptionComment.Text = Image_Info_Row("Image_DP_Comment")
                    End If
                End If

            End If
        End Sub

        Private Sub cbxCEEB_Leave(ByVal sender As Object, ByVal e As System.EventArgs) Handles cbxCeebName.Leave
            If sender.items.count() > 0 Then
                If Not IsDBNull(sender.SelectedItem) Then
                    'Only populate the ADMR comment if it is empty
                    If txtAdmrComment.Text = "" Then
                        txtAdmrComment.Text = sender.Text
                    End If
                End If
            End If
        End Sub

        Function CopyOneFile(ByVal fName As String, ByVal DestinationPath As String) As Boolean
            ' Move one file from one folder to another folder.
            If (Not Directory.Exists(DestinationPath)) Then
                MessageBox.Show(String.Format("The Directory '{0}' does not exist.", DestinationPath))
                Return False
            Else
                Dim CopyFile_Count As Integer = 0
TryCopyAgain:

                If File.Exists(fName) Then
                    Dim dFile As String = String.Empty

                    dFile = Path.GetFileName(fName)

                    Dim dFilePath As String = String.Empty
                    dFilePath = Path.Combine(DestinationPath, dFile)

                    'Make sure destination ends with a "\" if not throw error
                    If Microsoft.VisualBasic.Right(DestinationPath, 1) <> "\" Then
                        MessageBox.Show("ERROR: The path to the destination is not valid.  It must end in a \. File has not been moved")
                        Return False
                    Else
                        Dim isProcessed As Boolean = False
                        Do While Not isProcessed
                            Try
                                File.Copy(fName, DestinationPath + dFile, True)
                            Catch ex As Exception
                                isProcessed = False
                            Finally
                                isProcessed = True
                            End Try
                        Loop

                        'Check to see if file is in the new location.  If not, copy it again.
                        If Not File.Exists(dFilePath) Then
                            'Loop through the filing for 5 seconds if unable to process provide message box to close file
                            Thread.Sleep(1000)
                            If CopyFile_Count > 4 Then
                                MessageBox.Show("You have the file "" & dFilePath & "" open.")
                            Else
                                CopyFile_Count = CopyFile_Count + 1
                            End If

                            GoTo TryCopyAgain
                        End If

                        Return True
                    End If
                Else
                    MessageBox.Show("There was a problem copying the file. ERROR:The file is not in the directory.")
                    Return False
                End If
            End If
        End Function

        Public Sub ResetAllControls(ByVal form As Control)
            If form.Controls.Count > 0 Then
                For Each ctrl As Control In form.Controls
                    'Do not reset the Banner ID field until feedme is pressed
                    If Not ctrl.Name = "txtBannerID" Then
                        ResetAllControls(ctrl)
                    End If
                Next
            End If

            If TypeOf form Is TextBox Then
                Dim textBox As TextBox = DirectCast(form, TextBox)
                textBox.Text = Nothing
            End If

            If TypeOf form Is ComboBox Then
                Dim comboBox As ComboBox = DirectCast(form, ComboBox)
                If comboBox.Items.Count > 0 Then
                    'Do not reset the sequence drop down on each submission
                    If InStr(comboBox.Name, "GPA") > 0 Then
                        comboBox.SelectedIndex = 0
                    ElseIf InStr(comboBox.Name, "Seq") = 0 Then
                        comboBox.SelectedIndex = 0
                        comboBox.Text = "NA"
                        comboBox.Text = "N/A"
                    End If

                End If
            End If

            If TypeOf form Is CheckBox Then
                'Make sure the mandatory checkbox is checked
                If form.Name = "chkbxMandatory" Then
                    Dim checkBox As CheckBox = DirectCast(form, CheckBox)
                    checkBox.Checked = True
                Else
                    Dim checkBox As CheckBox = DirectCast(form, CheckBox)
                    checkBox.Checked = False
                End If
            End If

            If TypeOf form Is ListBox Then
                Dim listBox As ListBox = DirectCast(form, ListBox)
                listBox.ClearSelected()
            End If

            If TypeOf form Is RadioButton Then
                Dim RadioButton As RadioButton = DirectCast(form, RadioButton)
                RadioButton.Checked = False
            End If

            If TypeOf form Is DateTimePicker Then
                Dim DateTimePicker As DateTimePicker = DirectCast(form, DateTimePicker)
                DateTimePicker.Value = System.DateTime.Now
                'Disable the date picker
                DateTimePicker.Checked = False
            End If

            'Manually clear the High School and College Combo Boxes
            cbxCeebName.DataSource = Nothing
            cbxCeebName.Items.Clear()

            'Reset team leader
            Setup_Team_Leader()

            'Disable to Grad date, RIC, Class Size, GPA
            lblGradDate.Enabled = False
            dtHSGradDate.Enabled = False
            lblGPA.Enabled = False
            txtHS_GPA.Enabled = False
            cbx_GPA.Enabled = False
            lblRIC.Enabled = False
            txtHS_RIC.Enabled = False
            lblClassSize.Enabled = False
            txtHS_ClassSize.Enabled = False

            'Hide image on PDFViewer
            Image_Viewer.Visible = False
            'Set the viewer to a blank image
            Image_Viewer.Navigate("about:blank")

        End Sub

        Private Sub btnFeedMe_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnGet_DP.Click
            'Disable button to prevent double clicks
            btnGet_DP.Enabled = False
            'Check to make sure a User_ID has been assigned for the username
            If EM_WD_Users_ID > 0 Then

                'Get a list of files and move them to the users folder to process
                'Clear the clipboard so correct Banner ID is entered for next family
                Clipboard.Clear()

                Contact_Code = Nothing

                'Clear out the Banner ID field
                txtBannerID.Text = ""

                'Reset the sequence numbers to default (N/A)
                cbxSeq1.SelectedIndex = 0
                cbxSeq2.SelectedIndex = 0
                cbxSeq3.SelectedIndex = 0

                'Load the file queue (queue type = 1, general
                Load_File_Queue(1)
            Else
                MessageBox.Show("There is something wrong with your login.  Please restart the form. ERROR: No EM_WD_User_ID.")
            End If
            'Renable button to prevent double clicks
            btnGet_DP.Enabled = True
        End Sub

        Public Function LoadMatricData() As Boolean
            Dim bStatus As Boolean = False

            Try
                Dim TA_Submit_Matric As New Submit_MatricTableAdapters.QueriesTableAdapter
                TA_Submit_Matric.EM_WD_Submit_Matric_ins(Current_File(1), _
                                                         Current_File(3), _
                                                         Check_Text_NA(txtBannerID.Text, "txtBannerID"), _
                                                         EM_WD_Users_ID, _
                                                         cbxPayment_Type.Text, _
                                                         cbxPayment_Method.Text, _
                                                         cbxPayment_Department.Text)

                'Remove the selected row from the datagrid
                Dim theRowToDelete As DataRow = dtImages.Rows(dgImages.CurrentCell.RowIndex)
                dtImages.Rows.Remove(theRowToDelete)

                'Check to see if there are any remaining rows.  If not clear the datagrid.
                If dtImages.Rows.Count = 0 Then
                    dgImages.Columns.Clear()
                Else
                    'Update the datagrid
                    dgImages.DataSource = dtImages
                End If

                bStatus = True
            Catch ex As Exception
                MessageBox.Show(ex.ToString)
                bStatus = False
            End Try
            Return bStatus
        End Function

        Public Function LoadData() As Boolean
            Dim bStatus As Boolean = False

            Dim Mandatory As Boolean = chkbxMandatory.Checked

            Dim Image_ID As Integer = Current_File(1)
            Dim Image_Queues_ID As Integer = Current_File(3)

            'Check the radio buttons to determine if a comment type was selected
            'If the comment is blank then do not load any comments
            Dim General_Comment_Type As Object = Nothing

            If IsDBNull(Contact_Code) Then
                Contact_Code = Check_Text_NA(cbxContactCode.Text, "cbxContactCode")
            End If

            If IsDBNull(Check_Text_NA(txtGeneralComment.Text, "txtGeneralComment")) Then
                General_Comment_Type = Nothing
            Else
                If rdlCommentType_EM.Checked Then
                    General_Comment_Type = "EM"
                ElseIf rdlCommentType_EMO.Checked Then
                    General_Comment_Type = "EMO"
                End If
            End If

            'If the grad date is enabled then it should have a value
            Dim GradDate As Object = Nothing
            If dtHSGradDate.Checked Then
                Dim dtGradDate As DateTime
                dtGradDate = dtHSGradDate.Text
                GradDate = dtGradDate
            End If

            Dim DOBDate As Object = Nothing
            'If the date of birth is checked then it should have a value
            If dtDOB.Checked Then
                'Create a date time variable for loading the MM/yyyy value into
                Dim dtDOBDate As DateTime
                dtDOBDate = dtDOB.Text
                DOBDate = dtDOBDate
            End If

            Dim Test_On_Transcript_YN As Object = Nothing
            'Track the Y/N question about test scores on transcripts
            If rdlTest_No.Checked Then
                Test_On_Transcript_YN = False
            End If

            If rdlTest_Yes.Checked Then
                Test_On_Transcript_YN = True
            End If

            Try
                Dim TA_Submit_ERP As New Submit_Banner_NolijTableAdapters.QueriesTableAdapter
                TA_Submit_ERP.EM_WD_Submit_updt(Image_ID, _
                                                Image_Queues_ID, _
                                                Check_Text_NA(txtBannerID.Text, "txtBannerID"), _
                                                Convert_To_CDL(cbxSeq1.Text, cbxSeq2.Text, cbxSeq3.Text), _
                                                Check_Text_NA(cbxAdmrCode.Text, "cbxAdmrCode"), _
                                                Check_Text_NA(txtAdmrComment.Text, "txtAdmrComment"), _
                                                DateTime.Now, _
                                                Mandatory, _
                                                General_Comment_Type, _
                                                Check_Text_NA(txtGeneralComment.Text, "txtGeneralComment"), _
                                                DOBDate, _
                                                Check_Text_NA(txtSSN1.Text & txtSSN2.Text & txtSSN3.Text, "txtSSN1"), _
                                                Check_Text_NA(cbxGender.Text, "cbxGender"), _
                                                Check_Text_NA(cbxSourceCode.Text, "cbxSourceCode"), _
                                                Convert_To_CDL(cbxInterestCode1.Text, cbxInterestCode2.Text, "N/A"), _
                                                Check_Text_NA(cbxAttributeCode1.Text, "Attribute"), _
                                                Check_Text_NA(cbxContactCode.Text, "cbxContactCode"), _
                                                Check_Text_NA(txtZipCode.Text, "txtZipCode"), _
                                                Get_CEEB(), _
                                                ConvertGPA(txtHS_GPA.Text), _
                                                GradDate, _
                                                System.Convert.ToInt32(Check_Text_NA(txtHS_RIC.Text, "txtHS_RIC")), _
                                                System.Convert.ToInt32(Check_Text_NA(txtHS_ClassSize.Text, "txtHS_ClassSize")), _
                                                EM_WD_Users_ID, _
                                                Check_Text_NA(txtExceptionComment_Follow_up.Text, "txtExceptionComment_Follow_up"), _
                                                Test_On_Transcript_YN)

                'Remove the selected row from the datagrid
                Dim theRowToDelete As DataRow = dtImages.Rows(dgImages.CurrentCell.RowIndex)
                dtImages.Rows.Remove(theRowToDelete)

                'Check to see if there are any remaining rows.  If not clear the datagrid.
                If dtImages.Rows.Count = 0 Then
                    dgImages.Columns.Clear()

                    'Enable feedme, disable submit
                    Enable_Disable_Buttons(True, False)
                Else
                    'Update the datagrid
                    dgImages.DataSource = dtImages
                End If

                bStatus = True
            Catch ex As Exception
                MessageBox.Show(ex.ToString)
                bStatus = False
            End Try
            Return bStatus
        End Function

        Public Function LoadUnclaimed() As Boolean
            Dim bStatus As Boolean = False

            Dim Mandatory As Boolean = chkbxMandatory.Checked

            Dim Image_ID As Integer = Current_File(1)
            Dim Image_Queues_ID As Integer = Current_File(3)

            'Check the radio buttons to determine if a comment type was selected
            'If the comment is blank then do not load any comments
            Dim General_Comment_Type As Object = Nothing

            If IsDBNull(Contact_Code) Then
                Contact_Code = Check_Text_NA(cbxContactCode.Text, "cbxContactCode")
            End If

            If IsDBNull(Check_Text_NA(txtGeneralComment.Text, "txtGeneralComment")) Then
                General_Comment_Type = Nothing
            Else
                If rdlCommentType_EM.Checked Then
                    General_Comment_Type = "EM"
                ElseIf rdlCommentType_EMO.Checked Then
                    General_Comment_Type = "EMO"
                End If
            End If

            'If the grad date is enabled then it should have a value
            Dim GradDate As Object = Nothing
            If dtHSGradDate.Checked Then
                Dim dtGradDate As DateTime
                dtGradDate = dtHSGradDate.Text
                GradDate = dtGradDate
            End If

            Dim DOBDate As Object = Nothing
            'If the date of birth is checked then it should have a value
            If dtDOB.Checked Then
                'Create a date time variable for loading the MM/yyyy value into
                Dim dtDOBDate As DateTime
                dtDOBDate = dtDOB.Text
                DOBDate = dtDOBDate
            End If

            Try
                Dim TA_Submit_Unclaimed As New Submit_UnclaimedTableAdapters.QueriesTableAdapter
                TA_Submit_Unclaimed.EM_WD_Submit_Unclaimed_ins(Image_ID, _
                                                               Image_Queues_ID, _
                                                               EM_WD_Users_ID, _
                                                               txtUnclaimed_First_Name.Text, _
                                                               txtUnclaimed_Last_Name.Text, _
                                                               Nothing, _
                                                               Nothing, _
                                                               Nothing, _
                                                               Check_Text_NA(cbxAdmrCode.Text, "cbxAdmrCode"), _
                                                               Check_Text_NA(txtAdmrComment.Text, "txtAdmrComment"), _
                                                               DateTime.Now, _
                                                               Mandatory, _
                                                               General_Comment_Type, _
                                                               Check_Text_NA(txtGeneralComment.Text, "txtGeneralComment"), _
                                                               DOBDate, _
                                                               Check_Text_NA(txtSSN1.Text & txtSSN2.Text & txtSSN3.Text, "txtSSN1"), _
                                                               Check_Text_NA(cbxGender.Text, "cbxGender"), _
                                                               Check_Text_NA(cbxSourceCode.Text, "cbxSourceCode"), _
                                                               Convert_To_CDL(cbxInterestCode1.Text, cbxInterestCode2.Text, "N/A"), _
                                                               Check_Text_NA(cbxAttributeCode1.Text, "Attribute"), _
                                                               Check_Text_NA(cbxContactCode.Text, "cbxContactCode"), _
                                                               Check_Text_NA(txtZipCode.Text, "txtZipCode"), _
                                                               Get_CEEB(), _
                                                               ConvertGPA(txtHS_GPA.Text), _
                                                               GradDate, _
                                                               System.Convert.ToInt32(Check_Text_NA(txtHS_RIC.Text, "txtHS_RIC")), _
                                                               System.Convert.ToInt32(Check_Text_NA(txtHS_ClassSize.Text, "txtHS_ClassSize")))

                'Remove the selected row from the datagrid
                Dim theRowToDelete As DataRow = dtImages.Rows(dgImages.CurrentCell.RowIndex)
                dtImages.Rows.Remove(theRowToDelete)

                'Check to see if there are any remaining rows.  If not clear the datagrid.
                If dtImages.Rows.Count = 0 Then
                    dgImages.Columns.Clear()

                    'Enable feedme, disable submit
                    Enable_Disable_Buttons(True, False)
                Else
                    'Update the datagrid
                    dgImages.DataSource = dtImages
                End If

                bStatus = True

            Catch ex As Exception
                MessageBox.Show(ex.ToString)
                bStatus = False
            End Try
            Return bStatus
        End Function

        Public Function LoadException() As Boolean
            Dim bStatus As Boolean = False

            Dim Image_ID As Integer = Current_File(1)
            Dim Image_Queues_ID As Integer = Current_File(3)
            Dim Banner_ID As String = Check_Text_NA(txtBannerID.Text, "txtBannerID")

            Try


                Dim TA_Submit_Exception As New Submit_ExceptionTableAdapters.QueriesTableAdapter
                TA_Submit_Exception.EM_WD_Submit_Exception_ins(Image_ID, _
                                                               Image_Queues_ID, _
                                                               EM_WD_Users_ID, _
                                                               Banner_ID, _
                                                               txtExceptionComment.Text,
                                                               EM_WD_Team_ID)

                'Remove the selected row from the datagrid
                Dim theRowToDelete As DataRow = dtImages.Rows(dgImages.CurrentCell.RowIndex)
                dtImages.Rows.Remove(theRowToDelete)

                'Check to see if there are any remaining rows.  If not clear the datagrid.
                If dtImages.Rows.Count = 0 Then
                    dgImages.Columns.Clear()

                    'Enable feedme, disable submit
                    Enable_Disable_Buttons(True, False)
                Else
                    'Update the datagrid
                    dgImages.DataSource = dtImages
                End If

                bStatus = True
            Catch ex As Exception
                MessageBox.Show(ex.ToString)
                bStatus = False
            End Try
            Return bStatus
        End Function

        Public Function LoadProblem() As Boolean
            Dim bStatus As Boolean = False

            Dim Image_ID As Integer = Current_File(1)
            Dim Image_Queues_ID As Integer = Current_File(3)
            Dim Banner_ID As String = Check_Text_NA(txtBannerID.Text, "txtBannerID")
            Dim Image_Problem_Type_ID As Integer = DirectCast(cbxProblem_Type.SelectedItem, DataRowView).Row("Image_Problem_Type_ID")

            Try

                Dim TA_Submit_Problem As New Submit_ProblemTableAdapters.QueriesTableAdapter
                TA_Submit_Problem.EM_WD_Submit_Problem_ins(Image_ID, _
                                                           EM_WD_Users_ID, _
                                                           Banner_ID, _
                                                           Image_Problem_Type_ID, _
                                                           txtProblem_Comments.Text, _
                                                           Image_Queues_ID, _
                                                           EM_WD_Interface_ID)

                'Remove the selected row from the datagrid
                Dim theRowToDelete As DataRow = dtImages.Rows(dgImages.CurrentCell.RowIndex)
                dtImages.Rows.Remove(theRowToDelete)

                'Check to see if there are any remaining rows.  If not clear the datagrid.
                If dtImages.Rows.Count = 0 Then
                    dgImages.Columns.Clear()

                    'Enable feedme, disable submit
                    Enable_Disable_Buttons(True, False)
                Else
                    'Update the datagrid
                    dgImages.DataSource = dtImages
                End If

                bStatus = True
            Catch ex As Exception
                MessageBox.Show(ex.ToString)
                bStatus = False
            End Try
            Return bStatus
        End Function

        Public Sub Enable_Disable_Buttons(ByVal Get_Buttons As Boolean, ByVal Submit_Buttons As Boolean)

            If EM_WD_Team_Leader Then
                btnGet_Exception.Enabled = Get_Buttons
                btnGet_Problem.Enabled = Get_Buttons
            Else
                btnGet_Exception.Enabled = False
                btnGet_Problem.Enabled = False
            End If
            btnGet_DP.Enabled = Get_Buttons
            btnGet_Databank.Enabled = Get_Buttons
            btnGet_Exception.Enabled = Get_Buttons
            btnGet_Problem.Enabled = Get_Buttons

            btnSubmit.Enabled = Submit_Buttons
            btnException_Submit.Enabled = Submit_Buttons
            btnProblem_Submit.Enabled = Submit_Buttons
            btnUnclaimed_Submit.Enabled = Submit_Buttons
        End Sub

        Public Function GetDailyProcessCount() As Integer
            Dim ImageCount As Integer = 0

            Try
                Dim dtUsers_ID As DataTable
                Dim TA_WorkTotal_PerUser As New dsWorkTotal_PerUserTableAdapters.EM_WD_GetWorkTotals_perUserTableAdapter
                dtUsers_ID = TA_WorkTotal_PerUser.GetData_WorkTotal_PerUser(EM_WD_Users_ID, DateTime.Now)
                If dtUsers_ID.Rows.Count > 0 Then
                    ImageCount = dtUsers_ID.Rows(0).Item(0).ToString
                End If
            Catch ex As Exception
                MessageBox.Show(ex.ToString)
            End Try

            Return ImageCount
        End Function

        Private Sub btnGet_Problem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnGet_Problem.Click

            'Disable button to prevent double clicks
            btnGet_Problem.Enabled = False

            'Check to make sure a User_ID has been assigned for the username
            If EM_WD_Users_ID > 0 Then

                'Get a list of files and move them to the users folder to process
                'Clear the clipboard so correct Banner ID is entered for next family
                Clipboard.Clear()

                'Clear out the Banner ID field
                txtBannerID.Text = ""

                'Reset the sequence numbers to default (N/A)
                cbxSeq1.SelectedIndex = 0
                cbxSeq2.SelectedIndex = 0
                cbxSeq3.SelectedIndex = 0

                'Load the file queue
                Load_Problem_Queue()
            Else
                MessageBox.Show("There is something wrong with your login.  Please restart the form. ERROR: No EM_WD_User_ID.")
            End If

            'Enable button to prevent double clicks
            btnGet_Problem.Enabled = True
        End Sub

        Private Sub btnSubmit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSubmit.Click
            'Disable submit button to prevent double clicks
            btnSubmit.Enabled = False

            Dim EnableButtons As Boolean = True

            If ValidateForm() = True Then
                'Load the data into the database and move the file
                If LoadData() = True Then

                    'Reset all of the controls to initial state
                    ResetAllControls(Me)

                    'Reset the name of the file being processed
                    Current_File = Nothing

                    'Load successful disable buttons
                    EnableButtons = False

                    If Not IsNothing(Test_Score_Form) Then
                        'Close the test entry form if it is open
                        Test_Score_Form.Close()
                    End If


                Else
                    'There was a problem with the fields.  Show message.
                    MessageBox.Show("Please check all fields to make sure they are filled in correctly. ERROR: Database problem")
                End If

                'Update the label with the count of the documents being processed
                lblImages_Processed.Text = GetDailyProcessCount()
            Else
                'There was a problem with the fields.  Show message.
                MessageBox.Show("Please check all fields to make sure they are filled in correctly.")
            End If

            'Reenable the submit button to prevent double clicks
            btnSubmit.Enabled = EnableButtons

        End Sub

        Private Sub btnUnclaimed_Submit_Click(sender As System.Object, e As System.EventArgs) Handles btnUnclaimed_Submit.Click
            'Disable Unclaimed submit to prevent double clicking
            btnUnclaimed_Submit.Enabled = False

            Dim EnableButtons As Boolean = True

            'Update database and move file to a problem state or to queue for staff member
            If ValidateUnclaimed() = True Then
                If LoadUnclaimed() = True Then
                    'Reset all of the controls to initial state
                    ResetAllControls(Me)

                    'Reset the name of the file being processed
                    Current_File = Nothing

                    'Load successful disable buttons
                    EnableButtons = False

                    If Not IsNothing(Test_Score_Form) Then
                        'Close the test entry form if it is open
                        Test_Score_Form.Close()
                    End If
                Else
                    'There was a problem with the fields.  Show message.
                    MessageBox.Show("Please check all fields to make sure they are filled in correctly. ERROR: Database problem")
                End If

            Else
                MessageBox.Show("Please check all fields to make sure they are filled in correctly.")
            End If

            'Enable Unclaimed submit to prevent double clicking
            btnUnclaimed_Submit.Enabled = EnableButtons
        End Sub

        Private Sub btnException_Submit_Click(sender As System.Object, e As System.EventArgs) Handles btnException_Submit.Click
            'Disable Unclaimed submit to prevent double clicking
            btnException_Submit.Enabled = False

            Dim EnableButtons As Boolean = True

            'Update database and move file to a problem state or to queue for staff member
            If ValidateException() = True Then
                If LoadException() = True Then
                    'Reset all of the controls to initial state
                    ResetAllControls(Me)

                    'Reset the name of the file being processed
                    Current_File = Nothing

                    'Load successful disable buttons
                    EnableButtons = False

                    If Not IsNothing(Test_Score_Form) Then
                        'Close the test entry form if it is open
                        Test_Score_Form.Close()
                    End If
                Else
                    'There was a problem with the fields.  Show message.
                    MessageBox.Show("Please check all fields to make sure they are filled in correctly. ERROR: Database problem")
                End If

            Else
                MessageBox.Show("Please check all fields to make sure they are filled in correctly.")
            End If

            'Enable Unclaimed submit to prevent double clicking
            btnException_Submit.Enabled = EnableButtons
        End Sub

        Private Sub btnProblem_Submit_Click(sender As System.Object, e As System.EventArgs) Handles btnProblem_Submit.Click
            'Disable Unclaimed submit to prevent double clicking
            btnProblem_Submit.Enabled = False

            Dim EnableButtons As Boolean = True

            'Update database and move file to a problem state or to queue for staff member
            If ValidateProblem() = True Then
                If LoadProblem() = True Then
                    'Reset all of the controls to initial state
                    ResetAllControls(Me)

                    'Reset the name of the file being processed
                    Current_File = Nothing

                    'Load successful disable buttons
                    EnableButtons = False

                    If Not IsNothing(Test_Score_Form) Then
                        'Close the test entry form if it is open
                        Test_Score_Form.Close()
                    End If
                Else
                    'There was a problem with the fields.  Show message.
                    MessageBox.Show("Please check all fields to make sure they are filled in correctly. ERROR: Database problem")
                End If

            Else
                MessageBox.Show("Please check all fields to make sure they are filled in correctly.")
            End If

            'Enable Unclaimed submit to prevent double clicking
            btnProblem_Submit.Enabled = EnableButtons
        End Sub

        Private Sub btnGet_Exception_Click(sender As System.Object, e As System.EventArgs) Handles btnGet_Exception.Click
            'Disable button to prevent double clicks
            btnGet_Exception.Enabled = False
            'Check to make sure a User_ID has been assigned for the username
            If EM_WD_Users_ID > 0 Then

                'Get a list of files and move them to the users folder to process
                'Clear the clipboard so correct Banner ID is entered for next family
                Clipboard.Clear()

                Contact_Code = Nothing

                'Clear out the Banner ID field
                txtBannerID.Text = ""

                'Reset the sequence numbers to default (N/A)
                cbxSeq1.SelectedIndex = 0
                cbxSeq2.SelectedIndex = 0
                cbxSeq3.SelectedIndex = 0

                'Load the file queue
                Load_Exception_Queue()
            Else
                MessageBox.Show("There is something wrong with your login.  Please restart the form. ERROR: No EM_WD_User_ID.")
            End If
            'Renable button to prevent double clicks
            btnGet_Exception.Enabled = True
        End Sub

        Private Sub btnGet_Databank_Click(sender As System.Object, e As System.EventArgs) Handles btnGet_Databank.Click
            'Disable button to prevent double clicks
            btnGet_Databank.Enabled = False
            'Check to make sure a User_ID has been assigned for the username
            If EM_WD_Users_ID > 0 Then

                'Get a list of files and move them to the users folder to process
                'Clear the clipboard so correct Banner ID is entered for next family
                Clipboard.Clear()

                Contact_Code = Nothing

                'Clear out the Banner ID field
                txtBannerID.Text = ""

                'Reset the sequence numbers to default (N/A)
                cbxSeq1.SelectedIndex = 0
                cbxSeq2.SelectedIndex = 0
                cbxSeq3.SelectedIndex = 0

                'Load the file queue type = 7, from databank
                Load_File_Queue(7)
            Else
                MessageBox.Show("There is something wrong with your login.  Please restart the form. ERROR: No EM_WD_User_ID.")
            End If
            'Renable button to prevent double clicks
            btnGet_Databank.Enabled = True
        End Sub

        Private Sub btnCollege_Search_Click(sender As System.Object, e As System.EventArgs) Handles btnCollege_Search.Click
            'Disable button to prevent double clicks
            btnCollege_Search.Enabled = False

            If ValidateHS_CollegeSearch() Then
                If Len(Trim(txtZipCode.Text)) > 2 Then
                    dtCEEBCode.DefaultView.RowFilter = "OA_BANNER_CEEB_Zip Like '" & txtZipCode.Text & "%' AND OA_Banner_CEEB_Type='C'"
                    cbxCeebName.DataSource = dtCEEBCode.DefaultView
                    cbxCeebName.DisplayMember = "OA_Banner_CEEB_Desc"
                    cbxCeebName.ValueMember = "OA_Banner_CEEB_Code"
                End If
                If Len(Trim(txtCEEBCode.Text)) > 2 Then
                    dtCEEBCode.DefaultView.RowFilter = "OA_Banner_CEEB_Code Like '" & txtCEEBCode.Text & "%' AND OA_Banner_CEEB_Type='C'"
                    cbxCeebName.DataSource = dtCEEBCode.DefaultView
                    cbxCeebName.DisplayMember = "OA_Banner_CEEB_Desc"
                    cbxCeebName.ValueMember = "OA_Banner_CEEB_Code"
                End If
                If Len(Trim(txtCity.Text)) > 2 Then
                    dtCEEBCode.DefaultView.RowFilter = "OA_Banner_CEEB_City Like '" & txtCity.Text & "%' AND OA_Banner_CEEB_Type='C'"
                    cbxCeebName.DataSource = dtCEEBCode.DefaultView
                    cbxCeebName.DisplayMember = "OA_Banner_CEEB_Desc"
                    cbxCeebName.ValueMember = "OA_Banner_CEEB_Code"
                End If
            Else
                'Clear the data that is listed in the combobox
                cbxCeebName.DataSource = Nothing
            End If

            'Clear and add the college items to the no school list
            Setup_Col_NoSchool()

            'Disable to Grad date, RIC, Class Size, GPA
            lblGradDate.Enabled = False
            dtHSGradDate.Enabled = False
            lblGPA.Enabled = False
            txtHS_GPA.Enabled = False
            cbx_GPA.Enabled = False
            lblRIC.Enabled = False
            txtHS_RIC.Enabled = False
            lblClassSize.Enabled = False
            txtHS_ClassSize.Enabled = False

            'Enable button to prevent double clicks
            btnCollege_Search.Enabled = True
        End Sub

        Private Sub btnHS_Search_Click(sender As System.Object, e As System.EventArgs) Handles btnHS_Search.Click
            'Disable button to prevent double clicks
            btnHS_Search.Enabled = False

            If ValidateHS_CollegeSearch() Then
                If Len(Trim(txtZipCode.Text)) > 2 Then
                    dtCEEBCode.DefaultView.RowFilter = "OA_BANNER_CEEB_Zip Like '" & txtZipCode.Text & "%' AND OA_Banner_CEEB_Type='H'"
                    cbxCeebName.DataSource = dtCEEBCode.DefaultView
                    cbxCeebName.DisplayMember = "OA_Banner_CEEB_Desc"
                    cbxCeebName.ValueMember = "OA_Banner_CEEB_Code"
                End If
                If Len(Trim(txtCEEBCode.Text)) > 2 Then
                    dtCEEBCode.DefaultView.RowFilter = "OA_Banner_CEEB_Code Like '" & txtCEEBCode.Text & "%' AND OA_Banner_CEEB_Type='H'"
                    cbxCeebName.DataSource = dtCEEBCode.DefaultView
                    cbxCeebName.DisplayMember = "OA_Banner_CEEB_Desc"
                    cbxCeebName.ValueMember = "OA_Banner_CEEB_Code"
                End If
                If Len(Trim(txtCity.Text)) > 2 Then
                    dtCEEBCode.DefaultView.RowFilter = "OA_Banner_CEEB_City Like '" & txtCity.Text & "%' AND OA_Banner_CEEB_Type='H'"
                    cbxCeebName.DataSource = dtCEEBCode.DefaultView
                    cbxCeebName.DisplayMember = "OA_Banner_CEEB_Desc"
                    cbxCeebName.ValueMember = "OA_Banner_CEEB_Code"
                End If
            Else
                'Clear the data that is listed in the combobox
                cbxCeebName.DataSource = Nothing
            End If
            'Clear and add the High School items to the no school list
            Setup_HS_NoSchool()

            'Enable to Grad date, RIC, Class Size, GPA
            lblGradDate.Enabled = True
            dtHSGradDate.Enabled = True
            lblGPA.Enabled = True
            txtHS_GPA.Enabled = True
            cbx_GPA.Enabled = True
            lblRIC.Enabled = True
            txtHS_RIC.Enabled = True
            lblClassSize.Enabled = True
            txtHS_ClassSize.Enabled = True

            'Enable button to prevent double clicks
            btnHS_Search.Enabled = True
        End Sub

        Private Sub btnSubmit_Matric_Click(sender As System.Object, e As System.EventArgs) Handles btnSubmit_Matric.Click
            'Disable submit button to prevent double clicks
            btnSubmit_Matric.Enabled = False

            Dim EnableButtons As Boolean = True

            If ValidateForm() = True Then
                If LoadMatricData() = True Then
                    'Copy the current file to the appropriate admission folder
                    If cbxPayment_Department.Text = "Part-time/Transfer" Then
                        If CopyOneFile(Current_File(0), System.Configuration.ConfigurationManager.AppSettings("PT_Transfer_Matric")) = False Then
                            Exit Sub
                        End If
                    ElseIf cbxPayment_Department.Text = "Graduate" Then
                        If CopyOneFile(Current_File(0), System.Configuration.ConfigurationManager.AppSettings("Grad_Matric")) = False Then
                            Exit Sub
                        End If
                    Else
                        If CopyOneFile(Current_File(0), System.Configuration.ConfigurationManager.AppSettings("Freshmen_Matric")) = False Then
                            Exit Sub
                        End If
                    End If

                    'Reset all of the controls to initial state
                    ResetAllControls(Me)

                    'Reset the name of the file being processed
                    Current_File = Nothing

                    'Load successful disable buttons
                    EnableButtons = False

                    If Not IsNothing(Test_Score_Form) Then
                        'Close the test entry form if it is open
                        Test_Score_Form.Close()
                    End If

                Else
                    'There was a problem with the fields.  Show message.
                    MessageBox.Show("Please check all fields to make sure they are filled in correctly. ERROR: Database problem")
                End If

                'Update the label with the count of the documents being processed
                lblImages_Processed.Text = GetDailyProcessCount()
            Else
                'There was a problem with the fields.  Show message.
                MessageBox.Show("Please check all fields to make sure they are filled in correctly.")
            End If

            'Reenable the submit button to prevent double clicks
            btnSubmit_Matric.Enabled = EnableButtons

        End Sub

        Private Sub rdlTestReceived_CheckChanged(sender As Object, e As EventArgs) Handles rdlTest_Yes.CheckedChanged, rdlTest_No.CheckedChanged
            Dim rb As RadioButton = TryCast(sender, RadioButton)

            If rb.Checked Then
                ErrorProvider1.SetError(pnlTestScores, "")
            End If

            'If rb.Name = "rdlTest_Yes" And rb.Checked Then  '//ds 9/5/14
            If rb.Name = "rdlTest_Yes" And rb.Checked And (cbxAdmrCode.Text Like "HTR*" Or cbxAdmrCode.Text Like "DTR*" Or cbxAdmrCode.Text Like "FTRN" Or cbxAdmrCode.Text Like "UT*" Or cbxAdmrCode.Text Like "MID" Or cbxAdmrCode.Text Like "QGRD" Or cbxAdmrCode.Text Like "IELT" Or cbxAdmrCode.Text Like "TEST") Then

                btnEnter_Scores.Enabled = True
            Else
                btnEnter_Scores.Enabled = False
            End If
        End Sub

        Sub btnEnter_Scores_Click(sender As System.Object, e As System.EventArgs) Handles btnEnter_Scores.Click
            If Test_Score_Form Is Nothing Then
                'Check to make sure a file is selected to be processed
                If Current_File Is Nothing Then
                    ErrorProvider1.SetError(dgImages, "Please select a row.")
                Else
                    ErrorProvider1.SetError(dgImages, "")

                    Dim Image_ID As Integer = Current_File(1)

                    'Pass the image ID to the new form and open it
                    Test_Score_Form = New Test_Scores(Image_ID, txtBannerID.Text, EM_WD_Users_ID)
                    Test_Score_Form.Show()
                End If
            End If
        End Sub

        Private Sub cbxAdmrCode_SelectedIndexChanged(sender As System.Object, e As System.EventArgs) Handles cbxAdmrCode.SelectedIndexChanged
            btnEnter_Scores.Enabled = False   '//ds 9/5/14
            rdlTest_Yes.Checked = False
            rdlTest_No.Checked = False
        End Sub
    End Class

    Public Class CalendarColumn
        Inherits DataGridViewColumn

        Public Sub New()
            MyBase.New(New CalendarCell())
        End Sub

        Public Overrides Property CellTemplate() As DataGridViewCell
            Get
                Return MyBase.CellTemplate
            End Get
            Set(ByVal value As DataGridViewCell)

                ' Ensure that the cell used for the template is a CalendarCell.
                If (value IsNot Nothing) AndAlso _
                    Not value.GetType().IsAssignableFrom(GetType(CalendarCell)) _
                    Then
                    Throw New InvalidCastException("Must be a CalendarCell")
                End If
                MyBase.CellTemplate = value

            End Set
        End Property

    End Class

    Public Class CalendarCell
        Inherits DataGridViewTextBoxCell

        Public Sub New()
            ' Use the short date format.
            Me.Style.Format = "MM/yyyy"
        End Sub

        Public Overrides Sub InitializeEditingControl(ByVal rowIndex As Integer, _
            ByVal initialFormattedValue As Object, _
            ByVal dataGridViewCellStyle As DataGridViewCellStyle)

            ' Set the value of the editing control to the current cell value.
            MyBase.InitializeEditingControl(rowIndex, initialFormattedValue, _
                dataGridViewCellStyle)

            Dim ctl As CalendarEditingControl = _
                CType(DataGridView.EditingControl, CalendarEditingControl)

            ' Use the default row value when Value property is null.
            If (Me.Value Is Nothing) Then
                ctl.Value = CType(Me.DefaultNewRowValue, DateTime)
            Else
                ctl.Value = CType(Me.Value, DateTime)
            End If
        End Sub

        Public Overrides ReadOnly Property EditType() As Type
            Get
                ' Return the type of the editing control that CalendarCell uses.
                Return GetType(CalendarEditingControl)
            End Get
        End Property

        Public Overrides ReadOnly Property ValueType() As Type
            Get
                ' Return the type of the value that CalendarCell contains.
                Return GetType(DateTime)
            End Get
        End Property

        Public Overrides ReadOnly Property DefaultNewRowValue() As Object
            Get
                ' Use the current date and time as the default value.
                'Return DateTime.Now
                Return ""
            End Get
        End Property

    End Class

    Class CalendarEditingControl
        Inherits DateTimePicker
        Implements IDataGridViewEditingControl

        Private dataGridViewControl As DataGridView
        Private valueIsChanged As Boolean = False
        Private rowIndexNum As Integer

        Public Sub New()
            Me.Format = DateTimePickerFormat.Custom
            Me.CustomFormat = "MM/yyyy"
            Me.ShowUpDown = True
        End Sub

        Public Property EditingControlFormattedValue() As Object _
            Implements IDataGridViewEditingControl.EditingControlFormattedValue

            Get
                Return Me.Value.ToShortDateString()
            End Get

            Set(ByVal value As Object)
                Try
                    ' This will throw an exception of the string is 
                    ' null, empty, or not in the format of a date.
                    Me.Value = DateTime.Parse(CStr(value))
                Catch
                    ' In the case of an exception, just use the default
                    ' value so we're not left with a null value.
                    Me.Value = DateTime.Now
                End Try
            End Set

        End Property

        Public Function GetEditingControlFormattedValue(ByVal context _
            As DataGridViewDataErrorContexts) As Object _
            Implements IDataGridViewEditingControl.GetEditingControlFormattedValue

            Return Me.Value.ToShortDateString()

        End Function

        Public Sub ApplyCellStyleToEditingControl(ByVal dataGridViewCellStyle As  _
            DataGridViewCellStyle) _
            Implements IDataGridViewEditingControl.ApplyCellStyleToEditingControl

            Me.Font = dataGridViewCellStyle.Font
            Me.CalendarForeColor = dataGridViewCellStyle.ForeColor
            Me.CalendarMonthBackground = dataGridViewCellStyle.BackColor

        End Sub

        Public Property EditingControlRowIndex() As Integer _
            Implements IDataGridViewEditingControl.EditingControlRowIndex

            Get
                Return rowIndexNum
            End Get
            Set(ByVal value As Integer)
                rowIndexNum = value
            End Set

        End Property

        Public Function EditingControlWantsInputKey(ByVal key As Keys, _
            ByVal dataGridViewWantsInputKey As Boolean) As Boolean _
            Implements IDataGridViewEditingControl.EditingControlWantsInputKey

            ' Let the DateTimePicker handle the keys listed.
            Select Case key And Keys.KeyCode
                Case Keys.Left, Keys.Up, Keys.Down, Keys.Right, _
                    Keys.Home, Keys.End, Keys.PageDown, Keys.PageUp

                    Return True

                Case Else
                    Return Not dataGridViewWantsInputKey
            End Select

        End Function

        Public Sub PrepareEditingControlForEdit(ByVal selectAll As Boolean) _
            Implements IDataGridViewEditingControl.PrepareEditingControlForEdit

            ' No preparation needs to be done.

        End Sub

        Public ReadOnly Property RepositionEditingControlOnValueChange() _
            As Boolean Implements _
            IDataGridViewEditingControl.RepositionEditingControlOnValueChange

            Get
                Return False
            End Get

        End Property

        Public Property EditingControlDataGridView() As DataGridView _
            Implements IDataGridViewEditingControl.EditingControlDataGridView

            Get
                Return dataGridViewControl
            End Get
            Set(ByVal value As DataGridView)
                dataGridViewControl = value
            End Set

        End Property

        Public Property EditingControlValueChanged() As Boolean _
            Implements IDataGridViewEditingControl.EditingControlValueChanged

            Get
                Return valueIsChanged
            End Get
            Set(ByVal value As Boolean)
                valueIsChanged = value
            End Set

        End Property

        Public ReadOnly Property EditingControlCursor() As Cursor _
            Implements IDataGridViewEditingControl.EditingPanelCursor

            Get
                Return MyBase.Cursor
            End Get

        End Property

        Protected Overrides Sub OnValueChanged(ByVal eventargs As EventArgs)

            ' Notify the DataGridView that the contents of the cell have changed.
            valueIsChanged = True
            Me.EditingControlDataGridView.NotifyCurrentCellDirty(True)
            MyBase.OnValueChanged(eventargs)

        End Sub

    End Class
End Namespace



