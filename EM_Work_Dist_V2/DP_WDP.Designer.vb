﻿
Namespace DataProcessing
    <Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
    Partial Class DP_WDP
        Inherits System.Windows.Forms.Form

        'Form overrides dispose to clean up the component list.
        <System.Diagnostics.DebuggerNonUserCode()> _
        Protected Overrides Sub Dispose(ByVal disposing As Boolean)
            Try
                If disposing AndAlso components IsNot Nothing Then
                    components.Dispose()
                End If
            Finally
                MyBase.Dispose(disposing)
            End Try
        End Sub

        'Required by the Windows Form Designer
        Private components As System.ComponentModel.IContainer

        'NOTE: The following procedure is required by the Windows Form Designer
        'It can be modified using the Windows Form Designer.  
        'Do not modify it using the code editor.
        <System.Diagnostics.DebuggerStepThrough()> _
        Private Sub InitializeComponent()
            Me.components = New System.ComponentModel.Container()
            Me.Label1 = New System.Windows.Forms.Label()
            Me.cbxCeebName = New System.Windows.Forms.ComboBox()
            Me.dtHSGradDate = New System.Windows.Forms.DateTimePicker()
            Me.btnGet_DP = New System.Windows.Forms.Button()
            Me.txtBannerID = New System.Windows.Forms.TextBox()
            Me.txtAdmrComment = New System.Windows.Forms.TextBox()
            Me.txtHS_RIC = New System.Windows.Forms.TextBox()
            Me.rdlCommentType_EM = New System.Windows.Forms.RadioButton()
            Me.rdlCommentType_EMO = New System.Windows.Forms.RadioButton()
            Me.txtGeneralComment = New System.Windows.Forms.TextBox()
            Me.btnSubmit = New System.Windows.Forms.Button()
            Me.Label2 = New System.Windows.Forms.Label()
            Me.Label3 = New System.Windows.Forms.Label()
            Me.Label4 = New System.Windows.Forms.Label()
            Me.lblName = New System.Windows.Forms.Label()
            Me.lblRIC = New System.Windows.Forms.Label()
            Me.lblGradDate = New System.Windows.Forms.Label()
            Me.Label14 = New System.Windows.Forms.Label()
            Me.Label15 = New System.Windows.Forms.Label()
            Me.lblSearch_Error = New System.Windows.Forms.Label()
            Me.lblGPA = New System.Windows.Forms.Label()
            Me.txtHS_GPA = New System.Windows.Forms.TextBox()
            Me.btnHS_Search = New System.Windows.Forms.Button()
            Me.txtCEEBCode = New System.Windows.Forms.TextBox()
            Me.txtCity = New System.Windows.Forms.TextBox()
            Me.Label32 = New System.Windows.Forms.Label()
            Me.Label31 = New System.Windows.Forms.Label()
            Me.Label30 = New System.Windows.Forms.Label()
            Me.txtHS_ClassSize = New System.Windows.Forms.TextBox()
            Me.lblClassSize = New System.Windows.Forms.Label()
            Me.cbxNoSchool = New System.Windows.Forms.ComboBox()
            Me.lblNo_School = New System.Windows.Forms.Label()
            Me.txtZipCode = New System.Windows.Forms.TextBox()
            Me.btnCollege_Search = New System.Windows.Forms.Button()
            Me.TabControl2 = New System.Windows.Forms.TabControl()
            Me.TabPage3 = New System.Windows.Forms.TabPage()
            Me.TabControl4 = New System.Windows.Forms.TabControl()
            Me.TabPage4 = New System.Windows.Forms.TabPage()
            Me.cbxSeq1 = New System.Windows.Forms.ComboBox()
            Me.cbxSeq2 = New System.Windows.Forms.ComboBox()
            Me.cbxSeq3 = New System.Windows.Forms.ComboBox()
            Me.TabPage5 = New System.Windows.Forms.TabPage()
            Me.btnUnclaimed_Submit = New System.Windows.Forms.Button()
            Me.lblUnclaimed_First_Name = New System.Windows.Forms.Label()
            Me.txtUnclaimed_Last_Name = New System.Windows.Forms.TextBox()
            Me.lblUnclaimed_Last_Name = New System.Windows.Forms.Label()
            Me.txtUnclaimed_First_Name = New System.Windows.Forms.TextBox()
            Me.GroupBox1 = New System.Windows.Forms.GroupBox()
            Me.txtSSN3 = New System.Windows.Forms.TextBox()
            Me.cbxAttributeCode1 = New System.Windows.Forms.ComboBox()
            Me.txtSSN2 = New System.Windows.Forms.TextBox()
            Me.dtDOB = New System.Windows.Forms.DateTimePicker()
            Me.cbxGender = New System.Windows.Forms.ComboBox()
            Me.txtSSN1 = New System.Windows.Forms.TextBox()
            Me.cbxSourceCode = New System.Windows.Forms.ComboBox()
            Me.cbxContactCode = New System.Windows.Forms.ComboBox()
            Me.Label19 = New System.Windows.Forms.Label()
            Me.Label11 = New System.Windows.Forms.Label()
            Me.cbxInterestCode1 = New System.Windows.Forms.ComboBox()
            Me.Label20 = New System.Windows.Forms.Label()
            Me.Label16 = New System.Windows.Forms.Label()
            Me.Label22 = New System.Windows.Forms.Label()
            Me.Label23 = New System.Windows.Forms.Label()
            Me.chkbxMandatory = New System.Windows.Forms.CheckBox()
            Me.cbxInterestCode2 = New System.Windows.Forms.ComboBox()
            Me.cbxAdmrCode = New System.Windows.Forms.ComboBox()
            Me.TabPage6 = New System.Windows.Forms.TabPage()
            Me.btnSubmit_Matric = New System.Windows.Forms.Button()
            Me.gbx_Payments = New System.Windows.Forms.GroupBox()
            Me.cbxPayment_Department = New System.Windows.Forms.ComboBox()
            Me.cbxPayment_Method = New System.Windows.Forms.ComboBox()
            Me.cbxPayment_Type = New System.Windows.Forms.ComboBox()
            Me.Label8 = New System.Windows.Forms.Label()
            Me.Label6 = New System.Windows.Forms.Label()
            Me.Label7 = New System.Windows.Forms.Label()
            Me.TabControl3 = New System.Windows.Forms.TabControl()
            Me.TabPage1 = New System.Windows.Forms.TabPage()
            Me.cbxProblem_Type = New System.Windows.Forms.ComboBox()
            Me.btnProblem_Submit = New System.Windows.Forms.Button()
            Me.Label18 = New System.Windows.Forms.Label()
            Me.txtProblem_Comments = New System.Windows.Forms.TextBox()
            Me.Label10 = New System.Windows.Forms.Label()
            Me.TabPage2 = New System.Windows.Forms.TabPage()
            Me.lblException_Follow_Up = New System.Windows.Forms.Label()
            Me.txtExceptionComment_Follow_up = New System.Windows.Forms.TextBox()
            Me.Label24 = New System.Windows.Forms.Label()
            Me.btnException_Submit = New System.Windows.Forms.Button()
            Me.txtExceptionComment = New System.Windows.Forms.TextBox()
            Me.btnEnter_Scores = New System.Windows.Forms.Button()
            Me.EM_WD_GetImage_Test_ScoresBindingSource = New System.Windows.Forms.BindingSource(Me.components)
            Me.DsTest_Scores = New EM_Work_Dist_V2.dsTest_Scores()
            Me.OABannerTESCselBindingSource = New System.Windows.Forms.BindingSource(Me.components)
            Me.DsTestCodesBindingSource = New System.Windows.Forms.BindingSource(Me.components)
            Me.ErrorProvider1 = New System.Windows.Forms.ErrorProvider(Me.components)
            Me.btnGet_Problem = New System.Windows.Forms.Button()
            Me.Label5 = New System.Windows.Forms.Label()
            Me.lblImages_Processed = New System.Windows.Forms.Label()
            Me.lblVersion = New System.Windows.Forms.Label()
            Me.dgImages = New System.Windows.Forms.DataGridView()
            Me.btnGet_Exception = New System.Windows.Forms.Button()
            Me.txtHS_Grade = New System.Windows.Forms.TextBox()
            Me.btnGet_Databank = New System.Windows.Forms.Button()
            Me.TableAdapterManager = New EM_Work_Dist_V2.dsTest_ScoresTableAdapters.TableAdapterManager()
            Me.EM_WD_GetImage_Test_ScoresTableAdapter = New EM_Work_Dist_V2.dsTest_ScoresTableAdapters.EM_WD_GetImage_Test_ScoresTableAdapter()
            Me.Image_Viewer = New System.Windows.Forms.WebBrowser()
            Me.cbx_GPA = New System.Windows.Forms.ComboBox()
            Me.lblTestReceived = New System.Windows.Forms.Label()
            Me.rdlTest_Yes = New System.Windows.Forms.RadioButton()
            Me.rdlTest_No = New System.Windows.Forms.RadioButton()
            Me.pnlTestScores = New System.Windows.Forms.Panel()
            Me.TabControl2.SuspendLayout()
            Me.TabPage3.SuspendLayout()
            Me.TabControl4.SuspendLayout()
            Me.TabPage4.SuspendLayout()
            Me.TabPage5.SuspendLayout()
            Me.GroupBox1.SuspendLayout()
            Me.TabPage6.SuspendLayout()
            Me.gbx_Payments.SuspendLayout()
            Me.TabControl3.SuspendLayout()
            Me.TabPage1.SuspendLayout()
            Me.TabPage2.SuspendLayout()
            CType(Me.EM_WD_GetImage_Test_ScoresBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.DsTest_Scores, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.OABannerTESCselBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.DsTestCodesBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.ErrorProvider1, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.dgImages, System.ComponentModel.ISupportInitialize).BeginInit()
            Me.pnlTestScores.SuspendLayout()
            Me.SuspendLayout()
            '
            'Label1
            '
            Me.Label1.AutoSize = True
            Me.Label1.Location = New System.Drawing.Point(24, 17)
            Me.Label1.Name = "Label1"
            Me.Label1.Size = New System.Drawing.Size(18, 13)
            Me.Label1.TabIndex = 0
            Me.Label1.Text = "ID"
            '
            'cbxCeebName
            '
            Me.cbxCeebName.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
            Me.cbxCeebName.FormattingEnabled = True
            Me.cbxCeebName.Location = New System.Drawing.Point(753, 120)
            Me.cbxCeebName.Name = "cbxCeebName"
            Me.cbxCeebName.Size = New System.Drawing.Size(226, 21)
            Me.cbxCeebName.TabIndex = 47
            Me.cbxCeebName.TabStop = False
            '
            'dtHSGradDate
            '
            Me.dtHSGradDate.CustomFormat = "MM/yyyy"
            Me.dtHSGradDate.Enabled = False
            Me.dtHSGradDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom
            Me.dtHSGradDate.Location = New System.Drawing.Point(805, 187)
            Me.dtHSGradDate.Name = "dtHSGradDate"
            Me.dtHSGradDate.ShowCheckBox = True
            Me.dtHSGradDate.ShowUpDown = True
            Me.dtHSGradDate.Size = New System.Drawing.Size(104, 20)
            Me.dtHSGradDate.TabIndex = 51
            Me.dtHSGradDate.TabStop = False
            '
            'btnGet_DP
            '
            Me.btnGet_DP.Location = New System.Drawing.Point(754, 540)
            Me.btnGet_DP.Name = "btnGet_DP"
            Me.btnGet_DP.Size = New System.Drawing.Size(42, 54)
            Me.btnGet_DP.TabIndex = 5
            Me.btnGet_DP.TabStop = False
            Me.btnGet_DP.Text = "Get DP"
            Me.btnGet_DP.UseVisualStyleBackColor = True
            '
            'txtBannerID
            '
            Me.txtBannerID.Location = New System.Drawing.Point(48, 14)
            Me.txtBannerID.MaxLength = 10
            Me.txtBannerID.Name = "txtBannerID"
            Me.txtBannerID.Size = New System.Drawing.Size(100, 20)
            Me.txtBannerID.TabIndex = 7
            '
            'txtAdmrComment
            '
            Me.txtAdmrComment.Location = New System.Drawing.Point(95, 32)
            Me.txtAdmrComment.MaxLength = 30
            Me.txtAdmrComment.Name = "txtAdmrComment"
            Me.txtAdmrComment.Size = New System.Drawing.Size(147, 20)
            Me.txtAdmrComment.TabIndex = 5
            '
            'txtHS_RIC
            '
            Me.txtHS_RIC.Enabled = False
            Me.txtHS_RIC.Location = New System.Drawing.Point(891, 280)
            Me.txtHS_RIC.MaxLength = 4
            Me.txtHS_RIC.Name = "txtHS_RIC"
            Me.txtHS_RIC.Size = New System.Drawing.Size(34, 20)
            Me.txtHS_RIC.TabIndex = 52
            Me.txtHS_RIC.TabStop = False
            '
            'rdlCommentType_EM
            '
            Me.rdlCommentType_EM.AutoSize = True
            Me.rdlCommentType_EM.Location = New System.Drawing.Point(73, 9)
            Me.rdlCommentType_EM.Name = "rdlCommentType_EM"
            Me.rdlCommentType_EM.Size = New System.Drawing.Size(41, 17)
            Me.rdlCommentType_EM.TabIndex = 17
            Me.rdlCommentType_EM.Text = "EM"
            Me.rdlCommentType_EM.UseVisualStyleBackColor = True
            '
            'rdlCommentType_EMO
            '
            Me.rdlCommentType_EMO.AutoSize = True
            Me.rdlCommentType_EMO.Location = New System.Drawing.Point(120, 9)
            Me.rdlCommentType_EMO.Name = "rdlCommentType_EMO"
            Me.rdlCommentType_EMO.Size = New System.Drawing.Size(49, 17)
            Me.rdlCommentType_EMO.TabIndex = 18
            Me.rdlCommentType_EMO.Text = "EMO"
            Me.rdlCommentType_EMO.UseVisualStyleBackColor = True
            '
            'txtGeneralComment
            '
            Me.txtGeneralComment.Location = New System.Drawing.Point(4, 32)
            Me.txtGeneralComment.MaxLength = 4000
            Me.txtGeneralComment.Multiline = True
            Me.txtGeneralComment.Name = "txtGeneralComment"
            Me.txtGeneralComment.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
            Me.txtGeneralComment.Size = New System.Drawing.Size(235, 65)
            Me.txtGeneralComment.TabIndex = 19
            Me.txtGeneralComment.TabStop = False
            '
            'btnSubmit
            '
            Me.btnSubmit.Location = New System.Drawing.Point(147, 85)
            Me.btnSubmit.Name = "btnSubmit"
            Me.btnSubmit.Size = New System.Drawing.Size(88, 39)
            Me.btnSubmit.TabIndex = 23
            Me.btnSubmit.Tag = "9"
            Me.btnSubmit.Text = "Submit"
            Me.btnSubmit.UseVisualStyleBackColor = True
            '
            'Label2
            '
            Me.Label2.AutoSize = True
            Me.Label2.Location = New System.Drawing.Point(6, 56)
            Me.Label2.Name = "Label2"
            Me.Label2.Size = New System.Drawing.Size(36, 13)
            Me.Label2.TabIndex = 24
            Me.Label2.Text = "Seq #"
            '
            'Label3
            '
            Me.Label3.AutoSize = True
            Me.Label3.Location = New System.Drawing.Point(7, 7)
            Me.Label3.Name = "Label3"
            Me.Label3.Size = New System.Drawing.Size(39, 13)
            Me.Label3.TabIndex = 25
            Me.Label3.Text = "ADMR"
            '
            'Label4
            '
            Me.Label4.AutoSize = True
            Me.Label4.Location = New System.Drawing.Point(6, 35)
            Me.Label4.Name = "Label4"
            Me.Label4.Size = New System.Drawing.Size(86, 13)
            Me.Label4.TabIndex = 26
            Me.Label4.Text = "ADMR Comment"
            '
            'lblName
            '
            Me.lblName.AutoSize = True
            Me.lblName.Location = New System.Drawing.Point(751, 104)
            Me.lblName.Name = "lblName"
            Me.lblName.Size = New System.Drawing.Size(83, 13)
            Me.lblName.TabIndex = 27
            Me.lblName.Text = "Name of School"
            '
            'lblRIC
            '
            Me.lblRIC.AutoSize = True
            Me.lblRIC.Enabled = False
            Me.lblRIC.Location = New System.Drawing.Point(815, 283)
            Me.lblRIC.Name = "lblRIC"
            Me.lblRIC.Size = New System.Drawing.Size(72, 13)
            Me.lblRIC.TabIndex = 28
            Me.lblRIC.Text = "Rank in Class"
            '
            'lblGradDate
            '
            Me.lblGradDate.AutoSize = True
            Me.lblGradDate.Enabled = False
            Me.lblGradDate.Location = New System.Drawing.Point(743, 193)
            Me.lblGradDate.Name = "lblGradDate"
            Me.lblGradDate.Size = New System.Drawing.Size(56, 13)
            Me.lblGradDate.TabIndex = 29
            Me.lblGradDate.Text = "Grad Date"
            '
            'Label14
            '
            Me.Label14.AutoSize = True
            Me.Label14.Location = New System.Drawing.Point(7, 64)
            Me.Label14.Name = "Label14"
            Me.Label14.Size = New System.Drawing.Size(75, 13)
            Me.Label14.TabIndex = 35
            Me.Label14.Text = "Interest Codes"
            '
            'Label15
            '
            Me.Label15.AutoSize = True
            Me.Label15.Location = New System.Drawing.Point(8, 101)
            Me.Label15.Name = "Label15"
            Me.Label15.Size = New System.Drawing.Size(74, 13)
            Me.Label15.TabIndex = 36
            Me.Label15.Text = "Attribute Code"
            '
            'lblSearch_Error
            '
            Me.lblSearch_Error.AutoSize = True
            Me.lblSearch_Error.Location = New System.Drawing.Point(905, 41)
            Me.lblSearch_Error.Name = "lblSearch_Error"
            Me.lblSearch_Error.Size = New System.Drawing.Size(0, 13)
            Me.lblSearch_Error.TabIndex = 68
            '
            'lblGPA
            '
            Me.lblGPA.AutoSize = True
            Me.lblGPA.Enabled = False
            Me.lblGPA.Location = New System.Drawing.Point(741, 283)
            Me.lblGPA.Name = "lblGPA"
            Me.lblGPA.Size = New System.Drawing.Size(29, 13)
            Me.lblGPA.TabIndex = 67
            Me.lblGPA.Text = "GPA"
            '
            'txtHS_GPA
            '
            Me.txtHS_GPA.Enabled = False
            Me.txtHS_GPA.Location = New System.Drawing.Point(779, 280)
            Me.txtHS_GPA.MaxLength = 4
            Me.txtHS_GPA.Name = "txtHS_GPA"
            Me.txtHS_GPA.Size = New System.Drawing.Size(34, 20)
            Me.txtHS_GPA.TabIndex = 66
            Me.txtHS_GPA.TabStop = False
            '
            'btnHS_Search
            '
            Me.btnHS_Search.Location = New System.Drawing.Point(919, 12)
            Me.btnHS_Search.Name = "btnHS_Search"
            Me.btnHS_Search.Size = New System.Drawing.Size(86, 37)
            Me.btnHS_Search.TabIndex = 65
            Me.btnHS_Search.TabStop = False
            Me.btnHS_Search.Text = "Search High School"
            Me.btnHS_Search.UseVisualStyleBackColor = True
            '
            'txtCEEBCode
            '
            Me.txtCEEBCode.Location = New System.Drawing.Point(811, 12)
            Me.txtCEEBCode.MaxLength = 6
            Me.txtCEEBCode.Name = "txtCEEBCode"
            Me.txtCEEBCode.Size = New System.Drawing.Size(88, 20)
            Me.txtCEEBCode.TabIndex = 64
            Me.txtCEEBCode.TabStop = False
            '
            'txtCity
            '
            Me.txtCity.Location = New System.Drawing.Point(811, 64)
            Me.txtCity.MaxLength = 20
            Me.txtCity.Name = "txtCity"
            Me.txtCity.Size = New System.Drawing.Size(88, 20)
            Me.txtCity.TabIndex = 63
            Me.txtCity.TabStop = False
            '
            'Label32
            '
            Me.Label32.AutoSize = True
            Me.Label32.Location = New System.Drawing.Point(766, 67)
            Me.Label32.Name = "Label32"
            Me.Label32.Size = New System.Drawing.Size(24, 13)
            Me.Label32.TabIndex = 62
            Me.Label32.Text = "City"
            '
            'Label31
            '
            Me.Label31.AutoSize = True
            Me.Label31.Location = New System.Drawing.Point(755, 16)
            Me.Label31.Name = "Label31"
            Me.Label31.Size = New System.Drawing.Size(35, 13)
            Me.Label31.TabIndex = 61
            Me.Label31.Text = "CEEB"
            '
            'Label30
            '
            Me.Label30.AutoSize = True
            Me.Label30.Location = New System.Drawing.Point(768, 41)
            Me.Label30.Name = "Label30"
            Me.Label30.Size = New System.Drawing.Size(22, 13)
            Me.Label30.TabIndex = 60
            Me.Label30.Text = "Zip"
            '
            'txtHS_ClassSize
            '
            Me.txtHS_ClassSize.Enabled = False
            Me.txtHS_ClassSize.Location = New System.Drawing.Point(990, 280)
            Me.txtHS_ClassSize.MaxLength = 4
            Me.txtHS_ClassSize.Name = "txtHS_ClassSize"
            Me.txtHS_ClassSize.Size = New System.Drawing.Size(34, 20)
            Me.txtHS_ClassSize.TabIndex = 53
            Me.txtHS_ClassSize.TabStop = False
            '
            'lblClassSize
            '
            Me.lblClassSize.AutoSize = True
            Me.lblClassSize.Enabled = False
            Me.lblClassSize.Location = New System.Drawing.Point(931, 283)
            Me.lblClassSize.Name = "lblClassSize"
            Me.lblClassSize.Size = New System.Drawing.Size(55, 13)
            Me.lblClassSize.TabIndex = 34
            Me.lblClassSize.Text = "Class Size"
            '
            'cbxNoSchool
            '
            Me.cbxNoSchool.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
            Me.cbxNoSchool.FormattingEnabled = True
            Me.cbxNoSchool.Items.AddRange(New Object() {"5,3"})
            Me.cbxNoSchool.Location = New System.Drawing.Point(754, 160)
            Me.cbxNoSchool.Name = "cbxNoSchool"
            Me.cbxNoSchool.Size = New System.Drawing.Size(202, 21)
            Me.cbxNoSchool.TabIndex = 33
            Me.cbxNoSchool.TabStop = False
            '
            'lblNo_School
            '
            Me.lblNo_School.AutoSize = True
            Me.lblNo_School.Location = New System.Drawing.Point(754, 144)
            Me.lblNo_School.Name = "lblNo_School"
            Me.lblNo_School.Size = New System.Drawing.Size(93, 13)
            Me.lblNo_School.TabIndex = 32
            Me.lblNo_School.Text = "School Not Found"
            '
            'txtZipCode
            '
            Me.txtZipCode.Location = New System.Drawing.Point(811, 38)
            Me.txtZipCode.MaxLength = 5
            Me.txtZipCode.Name = "txtZipCode"
            Me.txtZipCode.Size = New System.Drawing.Size(88, 20)
            Me.txtZipCode.TabIndex = 46
            Me.txtZipCode.TabStop = False
            '
            'btnCollege_Search
            '
            Me.btnCollege_Search.Location = New System.Drawing.Point(919, 54)
            Me.btnCollege_Search.Name = "btnCollege_Search"
            Me.btnCollege_Search.Size = New System.Drawing.Size(86, 39)
            Me.btnCollege_Search.TabIndex = 61
            Me.btnCollege_Search.TabStop = False
            Me.btnCollege_Search.Text = "Search Colleges"
            Me.btnCollege_Search.UseVisualStyleBackColor = True
            '
            'TabControl2
            '
            Me.TabControl2.Controls.Add(Me.TabPage3)
            Me.TabControl2.Controls.Add(Me.TabPage6)
            Me.TabControl2.Location = New System.Drawing.Point(470, 13)
            Me.TabControl2.Name = "TabControl2"
            Me.TabControl2.SelectedIndex = 0
            Me.TabControl2.Size = New System.Drawing.Size(267, 635)
            Me.TabControl2.TabIndex = 49
            Me.TabControl2.TabStop = False
            '
            'TabPage3
            '
            Me.TabPage3.Controls.Add(Me.TabControl4)
            Me.TabPage3.Controls.Add(Me.GroupBox1)
            Me.TabPage3.Controls.Add(Me.txtSSN3)
            Me.TabPage3.Controls.Add(Me.cbxAttributeCode1)
            Me.TabPage3.Controls.Add(Me.txtSSN2)
            Me.TabPage3.Controls.Add(Me.dtDOB)
            Me.TabPage3.Controls.Add(Me.cbxGender)
            Me.TabPage3.Controls.Add(Me.txtSSN1)
            Me.TabPage3.Controls.Add(Me.cbxSourceCode)
            Me.TabPage3.Controls.Add(Me.cbxContactCode)
            Me.TabPage3.Controls.Add(Me.Label19)
            Me.TabPage3.Controls.Add(Me.Label11)
            Me.TabPage3.Controls.Add(Me.cbxInterestCode1)
            Me.TabPage3.Controls.Add(Me.Label20)
            Me.TabPage3.Controls.Add(Me.Label16)
            Me.TabPage3.Controls.Add(Me.Label22)
            Me.TabPage3.Controls.Add(Me.Label23)
            Me.TabPage3.Controls.Add(Me.chkbxMandatory)
            Me.TabPage3.Controls.Add(Me.cbxInterestCode2)
            Me.TabPage3.Controls.Add(Me.cbxAdmrCode)
            Me.TabPage3.Controls.Add(Me.Label14)
            Me.TabPage3.Controls.Add(Me.Label15)
            Me.TabPage3.Controls.Add(Me.txtAdmrComment)
            Me.TabPage3.Controls.Add(Me.Label3)
            Me.TabPage3.Controls.Add(Me.Label4)
            Me.TabPage3.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
            Me.TabPage3.Location = New System.Drawing.Point(4, 22)
            Me.TabPage3.Name = "TabPage3"
            Me.TabPage3.Padding = New System.Windows.Forms.Padding(3)
            Me.TabPage3.Size = New System.Drawing.Size(259, 609)
            Me.TabPage3.TabIndex = 0
            Me.TabPage3.Text = "General"
            Me.TabPage3.UseVisualStyleBackColor = True
            '
            'TabControl4
            '
            Me.TabControl4.Controls.Add(Me.TabPage4)
            Me.TabControl4.Controls.Add(Me.TabPage5)
            Me.TabControl4.Location = New System.Drawing.Point(0, 444)
            Me.TabControl4.Name = "TabControl4"
            Me.TabControl4.SelectedIndex = 0
            Me.TabControl4.Size = New System.Drawing.Size(256, 156)
            Me.TabControl4.TabIndex = 351
            Me.TabControl4.TabStop = False
            '
            'TabPage4
            '
            Me.TabPage4.Controls.Add(Me.txtBannerID)
            Me.TabPage4.Controls.Add(Me.Label2)
            Me.TabPage4.Controls.Add(Me.cbxSeq1)
            Me.TabPage4.Controls.Add(Me.cbxSeq2)
            Me.TabPage4.Controls.Add(Me.cbxSeq3)
            Me.TabPage4.Controls.Add(Me.Label1)
            Me.TabPage4.Controls.Add(Me.btnSubmit)
            Me.TabPage4.Location = New System.Drawing.Point(4, 22)
            Me.TabPage4.Name = "TabPage4"
            Me.TabPage4.Padding = New System.Windows.Forms.Padding(3)
            Me.TabPage4.Size = New System.Drawing.Size(248, 130)
            Me.TabPage4.TabIndex = 0
            Me.TabPage4.Text = "Entry"
            Me.TabPage4.UseVisualStyleBackColor = True
            '
            'cbxSeq1
            '
            Me.cbxSeq1.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
            Me.cbxSeq1.FormattingEnabled = True
            Me.cbxSeq1.Items.AddRange(New Object() {"N/A", "1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12", "13", "14", "15"})
            Me.cbxSeq1.Location = New System.Drawing.Point(48, 53)
            Me.cbxSeq1.Name = "cbxSeq1"
            Me.cbxSeq1.Size = New System.Drawing.Size(56, 21)
            Me.cbxSeq1.TabIndex = 8
            '
            'cbxSeq2
            '
            Me.cbxSeq2.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
            Me.cbxSeq2.FormattingEnabled = True
            Me.cbxSeq2.Items.AddRange(New Object() {"N/A", "1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12", "13", "14", "15"})
            Me.cbxSeq2.Location = New System.Drawing.Point(112, 53)
            Me.cbxSeq2.Name = "cbxSeq2"
            Me.cbxSeq2.Size = New System.Drawing.Size(56, 21)
            Me.cbxSeq2.TabIndex = 28
            Me.cbxSeq2.TabStop = False
            '
            'cbxSeq3
            '
            Me.cbxSeq3.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
            Me.cbxSeq3.FormattingEnabled = True
            Me.cbxSeq3.Items.AddRange(New Object() {"N/A", "1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12", "13", "14", "15"})
            Me.cbxSeq3.Location = New System.Drawing.Point(179, 53)
            Me.cbxSeq3.Name = "cbxSeq3"
            Me.cbxSeq3.Size = New System.Drawing.Size(56, 21)
            Me.cbxSeq3.TabIndex = 29
            Me.cbxSeq3.TabStop = False
            '
            'TabPage5
            '
            Me.TabPage5.Controls.Add(Me.btnUnclaimed_Submit)
            Me.TabPage5.Controls.Add(Me.lblUnclaimed_First_Name)
            Me.TabPage5.Controls.Add(Me.txtUnclaimed_Last_Name)
            Me.TabPage5.Controls.Add(Me.lblUnclaimed_Last_Name)
            Me.TabPage5.Controls.Add(Me.txtUnclaimed_First_Name)
            Me.TabPage5.Location = New System.Drawing.Point(4, 22)
            Me.TabPage5.Name = "TabPage5"
            Me.TabPage5.Padding = New System.Windows.Forms.Padding(3)
            Me.TabPage5.Size = New System.Drawing.Size(248, 130)
            Me.TabPage5.TabIndex = 1
            Me.TabPage5.Text = "Unclaimed"
            Me.TabPage5.UseVisualStyleBackColor = True
            '
            'btnUnclaimed_Submit
            '
            Me.btnUnclaimed_Submit.Location = New System.Drawing.Point(68, 69)
            Me.btnUnclaimed_Submit.Name = "btnUnclaimed_Submit"
            Me.btnUnclaimed_Submit.Size = New System.Drawing.Size(108, 23)
            Me.btnUnclaimed_Submit.TabIndex = 54
            Me.btnUnclaimed_Submit.Text = "Submit Unclaimed"
            Me.btnUnclaimed_Submit.UseVisualStyleBackColor = True
            '
            'lblUnclaimed_First_Name
            '
            Me.lblUnclaimed_First_Name.AutoSize = True
            Me.lblUnclaimed_First_Name.Location = New System.Drawing.Point(9, 34)
            Me.lblUnclaimed_First_Name.Name = "lblUnclaimed_First_Name"
            Me.lblUnclaimed_First_Name.Size = New System.Drawing.Size(57, 13)
            Me.lblUnclaimed_First_Name.TabIndex = 50
            Me.lblUnclaimed_First_Name.Text = "First Name"
            '
            'txtUnclaimed_Last_Name
            '
            Me.txtUnclaimed_Last_Name.Location = New System.Drawing.Point(77, 4)
            Me.txtUnclaimed_Last_Name.Name = "txtUnclaimed_Last_Name"
            Me.txtUnclaimed_Last_Name.Size = New System.Drawing.Size(149, 20)
            Me.txtUnclaimed_Last_Name.TabIndex = 49
            '
            'lblUnclaimed_Last_Name
            '
            Me.lblUnclaimed_Last_Name.AutoSize = True
            Me.lblUnclaimed_Last_Name.Location = New System.Drawing.Point(8, 7)
            Me.lblUnclaimed_Last_Name.Name = "lblUnclaimed_Last_Name"
            Me.lblUnclaimed_Last_Name.Size = New System.Drawing.Size(58, 13)
            Me.lblUnclaimed_Last_Name.TabIndex = 51
            Me.lblUnclaimed_Last_Name.Text = "Last Name"
            '
            'txtUnclaimed_First_Name
            '
            Me.txtUnclaimed_First_Name.Location = New System.Drawing.Point(77, 30)
            Me.txtUnclaimed_First_Name.Name = "txtUnclaimed_First_Name"
            Me.txtUnclaimed_First_Name.Size = New System.Drawing.Size(149, 20)
            Me.txtUnclaimed_First_Name.TabIndex = 48
            '
            'GroupBox1
            '
            Me.GroupBox1.Controls.Add(Me.rdlCommentType_EM)
            Me.GroupBox1.Controls.Add(Me.txtGeneralComment)
            Me.GroupBox1.Controls.Add(Me.rdlCommentType_EMO)
            Me.GroupBox1.Location = New System.Drawing.Point(3, 216)
            Me.GroupBox1.Name = "GroupBox1"
            Me.GroupBox1.Size = New System.Drawing.Size(250, 120)
            Me.GroupBox1.TabIndex = 350
            Me.GroupBox1.TabStop = False
            Me.GroupBox1.Text = "Comment"
            '
            'txtSSN3
            '
            Me.txtSSN3.Location = New System.Drawing.Point(201, 180)
            Me.txtSSN3.MaxLength = 4
            Me.txtSSN3.Name = "txtSSN3"
            Me.txtSSN3.Size = New System.Drawing.Size(34, 20)
            Me.txtSSN3.TabIndex = 34
            Me.txtSSN3.TabStop = False
            '
            'cbxAttributeCode1
            '
            Me.cbxAttributeCode1.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
            Me.cbxAttributeCode1.FormattingEnabled = True
            Me.cbxAttributeCode1.Location = New System.Drawing.Point(83, 98)
            Me.cbxAttributeCode1.Name = "cbxAttributeCode1"
            Me.cbxAttributeCode1.Size = New System.Drawing.Size(55, 21)
            Me.cbxAttributeCode1.TabIndex = 45
            Me.cbxAttributeCode1.TabStop = False
            '
            'txtSSN2
            '
            Me.txtSSN2.Location = New System.Drawing.Point(170, 180)
            Me.txtSSN2.MaxLength = 2
            Me.txtSSN2.Name = "txtSSN2"
            Me.txtSSN2.Size = New System.Drawing.Size(23, 20)
            Me.txtSSN2.TabIndex = 33
            Me.txtSSN2.TabStop = False
            '
            'dtDOB
            '
            Me.dtDOB.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
            Me.dtDOB.Location = New System.Drawing.Point(153, 151)
            Me.dtDOB.Name = "dtDOB"
            Me.dtDOB.ShowCheckBox = True
            Me.dtDOB.Size = New System.Drawing.Size(100, 20)
            Me.dtDOB.TabIndex = 31
            Me.dtDOB.TabStop = False
            '
            'cbxGender
            '
            Me.cbxGender.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
            Me.cbxGender.FormattingEnabled = True
            Me.cbxGender.Items.AddRange(New Object() {"N/A", "M", "F"})
            Me.cbxGender.Location = New System.Drawing.Point(55, 151)
            Me.cbxGender.Name = "cbxGender"
            Me.cbxGender.Size = New System.Drawing.Size(55, 21)
            Me.cbxGender.TabIndex = 35
            Me.cbxGender.TabStop = False
            '
            'txtSSN1
            '
            Me.txtSSN1.Location = New System.Drawing.Point(135, 180)
            Me.txtSSN1.MaxLength = 3
            Me.txtSSN1.Name = "txtSSN1"
            Me.txtSSN1.Size = New System.Drawing.Size(26, 20)
            Me.txtSSN1.TabIndex = 32
            Me.txtSSN1.TabStop = False
            '
            'cbxSourceCode
            '
            Me.cbxSourceCode.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
            Me.cbxSourceCode.FormattingEnabled = True
            Me.cbxSourceCode.Location = New System.Drawing.Point(83, 124)
            Me.cbxSourceCode.Name = "cbxSourceCode"
            Me.cbxSourceCode.Size = New System.Drawing.Size(69, 21)
            Me.cbxSourceCode.TabIndex = 37
            Me.cbxSourceCode.TabStop = False
            '
            'cbxContactCode
            '
            Me.cbxContactCode.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
            Me.cbxContactCode.FormattingEnabled = True
            Me.cbxContactCode.Location = New System.Drawing.Point(211, 98)
            Me.cbxContactCode.Name = "cbxContactCode"
            Me.cbxContactCode.Size = New System.Drawing.Size(45, 21)
            Me.cbxContactCode.TabIndex = 6
            '
            'Label19
            '
            Me.Label19.AutoSize = True
            Me.Label19.Location = New System.Drawing.Point(12, 183)
            Me.Label19.Name = "Label19"
            Me.Label19.Size = New System.Drawing.Size(117, 13)
            Me.Label19.TabIndex = 33
            Me.Label19.Text = "Social Security Number"
            '
            'Label11
            '
            Me.Label11.AutoSize = True
            Me.Label11.Location = New System.Drawing.Point(117, 152)
            Me.Label11.Name = "Label11"
            Me.Label11.Size = New System.Drawing.Size(30, 13)
            Me.Label11.TabIndex = 31
            Me.Label11.Text = "DOB"
            '
            'cbxInterestCode1
            '
            Me.cbxInterestCode1.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
            Me.cbxInterestCode1.FormattingEnabled = True
            Me.cbxInterestCode1.Location = New System.Drawing.Point(88, 60)
            Me.cbxInterestCode1.Name = "cbxInterestCode1"
            Me.cbxInterestCode1.Size = New System.Drawing.Size(45, 21)
            Me.cbxInterestCode1.TabIndex = 44
            Me.cbxInterestCode1.TabStop = False
            '
            'Label20
            '
            Me.Label20.AutoSize = True
            Me.Label20.Location = New System.Drawing.Point(7, 151)
            Me.Label20.Name = "Label20"
            Me.Label20.Size = New System.Drawing.Size(42, 13)
            Me.Label20.TabIndex = 35
            Me.Label20.Text = "Gender"
            '
            'Label16
            '
            Me.Label16.AutoSize = True
            Me.Label16.Location = New System.Drawing.Point(139, 101)
            Me.Label16.Name = "Label16"
            Me.Label16.Size = New System.Drawing.Size(72, 13)
            Me.Label16.TabIndex = 101
            Me.Label16.Text = "Contact Code"
            '
            'Label22
            '
            Me.Label22.AutoSize = True
            Me.Label22.Location = New System.Drawing.Point(176, 7)
            Me.Label22.Name = "Label22"
            Me.Label22.Size = New System.Drawing.Size(63, 13)
            Me.Label22.TabIndex = 32
            Me.Label22.Text = "Mandatory?"
            '
            'Label23
            '
            Me.Label23.AutoSize = True
            Me.Label23.Location = New System.Drawing.Point(8, 127)
            Me.Label23.Name = "Label23"
            Me.Label23.Size = New System.Drawing.Size(69, 13)
            Me.Label23.TabIndex = 41
            Me.Label23.Text = "Source Code"
            '
            'chkbxMandatory
            '
            Me.chkbxMandatory.AutoSize = True
            Me.chkbxMandatory.Checked = True
            Me.chkbxMandatory.CheckState = System.Windows.Forms.CheckState.Checked
            Me.chkbxMandatory.Location = New System.Drawing.Point(239, 7)
            Me.chkbxMandatory.Name = "chkbxMandatory"
            Me.chkbxMandatory.Size = New System.Drawing.Size(15, 14)
            Me.chkbxMandatory.TabIndex = 4
            Me.chkbxMandatory.UseVisualStyleBackColor = True
            '
            'cbxInterestCode2
            '
            Me.cbxInterestCode2.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
            Me.cbxInterestCode2.FormattingEnabled = True
            Me.cbxInterestCode2.Location = New System.Drawing.Point(142, 60)
            Me.cbxInterestCode2.Name = "cbxInterestCode2"
            Me.cbxInterestCode2.Size = New System.Drawing.Size(45, 21)
            Me.cbxInterestCode2.TabIndex = 39
            Me.cbxInterestCode2.TabStop = False
            '
            'cbxAdmrCode
            '
            Me.cbxAdmrCode.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
            Me.cbxAdmrCode.FormattingEnabled = True
            Me.cbxAdmrCode.Location = New System.Drawing.Point(52, 3)
            Me.cbxAdmrCode.Name = "cbxAdmrCode"
            Me.cbxAdmrCode.Size = New System.Drawing.Size(118, 21)
            Me.cbxAdmrCode.TabIndex = 3
            '
            'TabPage6
            '
            Me.TabPage6.Controls.Add(Me.btnSubmit_Matric)
            Me.TabPage6.Controls.Add(Me.gbx_Payments)
            Me.TabPage6.Controls.Add(Me.TabControl3)
            Me.TabPage6.Location = New System.Drawing.Point(4, 22)
            Me.TabPage6.Name = "TabPage6"
            Me.TabPage6.Padding = New System.Windows.Forms.Padding(3)
            Me.TabPage6.Size = New System.Drawing.Size(259, 609)
            Me.TabPage6.TabIndex = 3
            Me.TabPage6.Text = "Matrics/Problems "
            Me.TabPage6.UseVisualStyleBackColor = True
            '
            'btnSubmit_Matric
            '
            Me.btnSubmit_Matric.Location = New System.Drawing.Point(129, 127)
            Me.btnSubmit_Matric.Name = "btnSubmit_Matric"
            Me.btnSubmit_Matric.Size = New System.Drawing.Size(115, 23)
            Me.btnSubmit_Matric.TabIndex = 105
            Me.btnSubmit_Matric.TabStop = False
            Me.btnSubmit_Matric.Text = "Submit Matric"
            Me.btnSubmit_Matric.UseVisualStyleBackColor = True
            '
            'gbx_Payments
            '
            Me.gbx_Payments.Controls.Add(Me.cbxPayment_Department)
            Me.gbx_Payments.Controls.Add(Me.cbxPayment_Method)
            Me.gbx_Payments.Controls.Add(Me.cbxPayment_Type)
            Me.gbx_Payments.Controls.Add(Me.Label8)
            Me.gbx_Payments.Controls.Add(Me.Label6)
            Me.gbx_Payments.Controls.Add(Me.Label7)
            Me.gbx_Payments.Location = New System.Drawing.Point(17, 21)
            Me.gbx_Payments.Name = "gbx_Payments"
            Me.gbx_Payments.Size = New System.Drawing.Size(227, 97)
            Me.gbx_Payments.TabIndex = 104
            Me.gbx_Payments.TabStop = False
            Me.gbx_Payments.Text = "Matric Payments"
            Me.gbx_Payments.Visible = False
            '
            'cbxPayment_Department
            '
            Me.cbxPayment_Department.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
            Me.cbxPayment_Department.FormattingEnabled = True
            Me.cbxPayment_Department.Items.AddRange(New Object() {"", "Freshman", "Graduate", "Part-time/Transfer"})
            Me.cbxPayment_Department.Location = New System.Drawing.Point(91, 71)
            Me.cbxPayment_Department.Name = "cbxPayment_Department"
            Me.cbxPayment_Department.Size = New System.Drawing.Size(121, 21)
            Me.cbxPayment_Department.TabIndex = 107
            Me.cbxPayment_Department.TabStop = False
            '
            'cbxPayment_Method
            '
            Me.cbxPayment_Method.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
            Me.cbxPayment_Method.FormattingEnabled = True
            Me.cbxPayment_Method.Items.AddRange(New Object() {"", "Check", "Credit Card", "Cash", "Waived"})
            Me.cbxPayment_Method.Location = New System.Drawing.Point(91, 43)
            Me.cbxPayment_Method.Name = "cbxPayment_Method"
            Me.cbxPayment_Method.Size = New System.Drawing.Size(121, 21)
            Me.cbxPayment_Method.TabIndex = 106
            Me.cbxPayment_Method.TabStop = False
            '
            'cbxPayment_Type
            '
            Me.cbxPayment_Type.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
            Me.cbxPayment_Type.FormattingEnabled = True
            Me.cbxPayment_Type.Items.AddRange(New Object() {"", "Application Fee", "Housing", "Tuition", "Unknown"})
            Me.cbxPayment_Type.Location = New System.Drawing.Point(91, 16)
            Me.cbxPayment_Type.Name = "cbxPayment_Type"
            Me.cbxPayment_Type.Size = New System.Drawing.Size(121, 21)
            Me.cbxPayment_Type.TabIndex = 105
            Me.cbxPayment_Type.TabStop = False
            '
            'Label8
            '
            Me.Label8.AutoSize = True
            Me.Label8.Location = New System.Drawing.Point(17, 74)
            Me.Label8.Name = "Label8"
            Me.Label8.Size = New System.Drawing.Size(62, 13)
            Me.Label8.TabIndex = 104
            Me.Label8.Text = "Department"
            '
            'Label6
            '
            Me.Label6.AutoSize = True
            Me.Label6.Location = New System.Drawing.Point(17, 19)
            Me.Label6.Name = "Label6"
            Me.Label6.Size = New System.Drawing.Size(31, 13)
            Me.Label6.TabIndex = 102
            Me.Label6.Text = "Type"
            '
            'Label7
            '
            Me.Label7.AutoSize = True
            Me.Label7.Location = New System.Drawing.Point(17, 46)
            Me.Label7.Name = "Label7"
            Me.Label7.Size = New System.Drawing.Size(43, 13)
            Me.Label7.TabIndex = 103
            Me.Label7.Text = "Method"
            '
            'TabControl3
            '
            Me.TabControl3.Controls.Add(Me.TabPage1)
            Me.TabControl3.Controls.Add(Me.TabPage2)
            Me.TabControl3.Location = New System.Drawing.Point(1, 171)
            Me.TabControl3.Name = "TabControl3"
            Me.TabControl3.SelectedIndex = 0
            Me.TabControl3.Size = New System.Drawing.Size(260, 208)
            Me.TabControl3.TabIndex = 62
            '
            'TabPage1
            '
            Me.TabPage1.Controls.Add(Me.cbxProblem_Type)
            Me.TabPage1.Controls.Add(Me.btnProblem_Submit)
            Me.TabPage1.Controls.Add(Me.Label18)
            Me.TabPage1.Controls.Add(Me.txtProblem_Comments)
            Me.TabPage1.Controls.Add(Me.Label10)
            Me.TabPage1.Location = New System.Drawing.Point(4, 22)
            Me.TabPage1.Name = "TabPage1"
            Me.TabPage1.Padding = New System.Windows.Forms.Padding(3)
            Me.TabPage1.Size = New System.Drawing.Size(252, 182)
            Me.TabPage1.TabIndex = 0
            Me.TabPage1.Text = "Problem"
            Me.TabPage1.UseVisualStyleBackColor = True
            '
            'cbxProblem_Type
            '
            Me.cbxProblem_Type.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
            Me.cbxProblem_Type.FormattingEnabled = True
            Me.cbxProblem_Type.Location = New System.Drawing.Point(47, 8)
            Me.cbxProblem_Type.Name = "cbxProblem_Type"
            Me.cbxProblem_Type.Size = New System.Drawing.Size(118, 21)
            Me.cbxProblem_Type.TabIndex = 111
            Me.cbxProblem_Type.TabStop = False
            '
            'btnProblem_Submit
            '
            Me.btnProblem_Submit.Location = New System.Drawing.Point(124, 153)
            Me.btnProblem_Submit.Name = "btnProblem_Submit"
            Me.btnProblem_Submit.Size = New System.Drawing.Size(115, 23)
            Me.btnProblem_Submit.TabIndex = 110
            Me.btnProblem_Submit.TabStop = False
            Me.btnProblem_Submit.Text = "Submit Problem"
            Me.btnProblem_Submit.UseVisualStyleBackColor = True
            '
            'Label18
            '
            Me.Label18.AutoSize = True
            Me.Label18.Location = New System.Drawing.Point(10, 40)
            Me.Label18.Name = "Label18"
            Me.Label18.Size = New System.Drawing.Size(56, 13)
            Me.Label18.TabIndex = 109
            Me.Label18.Text = "Comments"
            '
            'txtProblem_Comments
            '
            Me.txtProblem_Comments.Location = New System.Drawing.Point(27, 56)
            Me.txtProblem_Comments.Multiline = True
            Me.txtProblem_Comments.Name = "txtProblem_Comments"
            Me.txtProblem_Comments.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
            Me.txtProblem_Comments.Size = New System.Drawing.Size(178, 64)
            Me.txtProblem_Comments.TabIndex = 108
            Me.txtProblem_Comments.TabStop = False
            '
            'Label10
            '
            Me.Label10.AutoSize = True
            Me.Label10.Location = New System.Drawing.Point(10, 11)
            Me.Label10.Name = "Label10"
            Me.Label10.Size = New System.Drawing.Size(31, 13)
            Me.Label10.TabIndex = 107
            Me.Label10.Text = "Type"
            '
            'TabPage2
            '
            Me.TabPage2.Controls.Add(Me.lblException_Follow_Up)
            Me.TabPage2.Controls.Add(Me.txtExceptionComment_Follow_up)
            Me.TabPage2.Controls.Add(Me.Label24)
            Me.TabPage2.Controls.Add(Me.btnException_Submit)
            Me.TabPage2.Controls.Add(Me.txtExceptionComment)
            Me.TabPage2.Location = New System.Drawing.Point(4, 22)
            Me.TabPage2.Name = "TabPage2"
            Me.TabPage2.Padding = New System.Windows.Forms.Padding(3)
            Me.TabPage2.Size = New System.Drawing.Size(252, 182)
            Me.TabPage2.TabIndex = 1
            Me.TabPage2.Text = "Exception"
            Me.TabPage2.UseVisualStyleBackColor = True
            '
            'lblException_Follow_Up
            '
            Me.lblException_Follow_Up.AutoSize = True
            Me.lblException_Follow_Up.Location = New System.Drawing.Point(1, 88)
            Me.lblException_Follow_Up.Name = "lblException_Follow_Up"
            Me.lblException_Follow_Up.Size = New System.Drawing.Size(56, 26)
            Me.lblException_Follow_Up.TabIndex = 112
            Me.lblException_Follow_Up.Text = "Follow-up" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "Comments"
            Me.lblException_Follow_Up.Visible = False
            '
            'txtExceptionComment_Follow_up
            '
            Me.txtExceptionComment_Follow_up.Location = New System.Drawing.Point(58, 79)
            Me.txtExceptionComment_Follow_up.Multiline = True
            Me.txtExceptionComment_Follow_up.Name = "txtExceptionComment_Follow_up"
            Me.txtExceptionComment_Follow_up.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
            Me.txtExceptionComment_Follow_up.Size = New System.Drawing.Size(173, 62)
            Me.txtExceptionComment_Follow_up.TabIndex = 111
            Me.txtExceptionComment_Follow_up.TabStop = False
            Me.txtExceptionComment_Follow_up.Visible = False
            '
            'Label24
            '
            Me.Label24.AutoSize = True
            Me.Label24.Location = New System.Drawing.Point(1, 14)
            Me.Label24.Name = "Label24"
            Me.Label24.Size = New System.Drawing.Size(56, 13)
            Me.Label24.TabIndex = 110
            Me.Label24.Text = "Comments"
            '
            'btnException_Submit
            '
            Me.btnException_Submit.Location = New System.Drawing.Point(131, 153)
            Me.btnException_Submit.Name = "btnException_Submit"
            Me.btnException_Submit.Size = New System.Drawing.Size(108, 23)
            Me.btnException_Submit.TabIndex = 1
            Me.btnException_Submit.TabStop = False
            Me.btnException_Submit.Text = "Submit Exception"
            Me.btnException_Submit.UseVisualStyleBackColor = True
            '
            'txtExceptionComment
            '
            Me.txtExceptionComment.Location = New System.Drawing.Point(58, 11)
            Me.txtExceptionComment.Multiline = True
            Me.txtExceptionComment.Name = "txtExceptionComment"
            Me.txtExceptionComment.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
            Me.txtExceptionComment.Size = New System.Drawing.Size(173, 62)
            Me.txtExceptionComment.TabIndex = 0
            Me.txtExceptionComment.TabStop = False
            '
            'btnEnter_Scores
            '
            Me.btnEnter_Scores.Enabled = False
            Me.btnEnter_Scores.Location = New System.Drawing.Point(949, 210)
            Me.btnEnter_Scores.Name = "btnEnter_Scores"
            Me.btnEnter_Scores.Size = New System.Drawing.Size(75, 36)
            Me.btnEnter_Scores.TabIndex = 352
            Me.btnEnter_Scores.Text = "Enter Scores"
            Me.btnEnter_Scores.UseVisualStyleBackColor = True
            '
            'EM_WD_GetImage_Test_ScoresBindingSource
            '
            Me.EM_WD_GetImage_Test_ScoresBindingSource.DataMember = "EM_WD_GetImage_Test_Scores"
            Me.EM_WD_GetImage_Test_ScoresBindingSource.DataSource = Me.DsTest_Scores
            '
            'DsTest_Scores
            '
            Me.DsTest_Scores.DataSetName = "dsTest_Scores"
            Me.DsTest_Scores.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
            '
            'ErrorProvider1
            '
            Me.ErrorProvider1.ContainerControl = Me
            '
            'btnGet_Problem
            '
            Me.btnGet_Problem.CausesValidation = False
            Me.btnGet_Problem.Location = New System.Drawing.Point(802, 540)
            Me.btnGet_Problem.Name = "btnGet_Problem"
            Me.btnGet_Problem.Size = New System.Drawing.Size(58, 54)
            Me.btnGet_Problem.TabIndex = 52
            Me.btnGet_Problem.TabStop = False
            Me.btnGet_Problem.Text = "Get Problem"
            Me.btnGet_Problem.UseVisualStyleBackColor = True
            '
            'Label5
            '
            Me.Label5.AutoSize = True
            Me.Label5.Location = New System.Drawing.Point(856, 625)
            Me.Label5.Name = "Label5"
            Me.Label5.Size = New System.Drawing.Size(97, 13)
            Me.Label5.TabIndex = 58
            Me.Label5.Text = "Images Processed:"
            '
            'lblImages_Processed
            '
            Me.lblImages_Processed.AutoSize = True
            Me.lblImages_Processed.Location = New System.Drawing.Point(952, 625)
            Me.lblImages_Processed.Name = "lblImages_Processed"
            Me.lblImages_Processed.Size = New System.Drawing.Size(13, 13)
            Me.lblImages_Processed.TabIndex = 59
            Me.lblImages_Processed.Text = "0"
            '
            'lblVersion
            '
            Me.lblVersion.AutoSize = True
            Me.lblVersion.Font = New System.Drawing.Font("Microsoft Sans Serif", 5.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
            Me.lblVersion.Location = New System.Drawing.Point(493, 638)
            Me.lblVersion.Name = "lblVersion"
            Me.lblVersion.Size = New System.Drawing.Size(0, 7)
            Me.lblVersion.TabIndex = 60
            '
            'dgImages
            '
            Me.dgImages.AllowUserToAddRows = False
            Me.dgImages.AllowUserToDeleteRows = False
            Me.dgImages.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
            Me.dgImages.Location = New System.Drawing.Point(743, 306)
            Me.dgImages.MultiSelect = False
            Me.dgImages.Name = "dgImages"
            Me.dgImages.ReadOnly = True
            Me.dgImages.ShowCellToolTips = False
            Me.dgImages.ShowEditingIcon = False
            Me.dgImages.Size = New System.Drawing.Size(273, 228)
            Me.dgImages.TabIndex = 61
            Me.dgImages.TabStop = False
            '
            'btnGet_Exception
            '
            Me.btnGet_Exception.Location = New System.Drawing.Point(866, 540)
            Me.btnGet_Exception.Name = "btnGet_Exception"
            Me.btnGet_Exception.Size = New System.Drawing.Size(74, 53)
            Me.btnGet_Exception.TabIndex = 63
            Me.btnGet_Exception.TabStop = False
            Me.btnGet_Exception.Text = "Get Exception"
            Me.btnGet_Exception.UseVisualStyleBackColor = True
            '
            'txtHS_Grade
            '
            Me.txtHS_Grade.Location = New System.Drawing.Point(93, 239)
            Me.txtHS_Grade.MaxLength = 4
            Me.txtHS_Grade.Name = "txtHS_Grade"
            Me.txtHS_Grade.Size = New System.Drawing.Size(34, 20)
            Me.txtHS_Grade.TabIndex = 66
            '
            'btnGet_Databank
            '
            Me.btnGet_Databank.Location = New System.Drawing.Point(946, 540)
            Me.btnGet_Databank.Name = "btnGet_Databank"
            Me.btnGet_Databank.Size = New System.Drawing.Size(75, 54)
            Me.btnGet_Databank.TabIndex = 69
            Me.btnGet_Databank.TabStop = False
            Me.btnGet_Databank.Text = "Get Databank"
            Me.btnGet_Databank.UseVisualStyleBackColor = True
            Me.btnGet_Databank.Visible = False
            '
            'TableAdapterManager
            '
            Me.TableAdapterManager.BackupDataSetBeforeUpdate = False
            Me.TableAdapterManager.Connection = Nothing
            Me.TableAdapterManager.UpdateOrder = EM_Work_Dist_V2.dsTest_ScoresTableAdapters.TableAdapterManager.UpdateOrderOption.InsertUpdateDelete
            '
            'EM_WD_GetImage_Test_ScoresTableAdapter
            '
            Me.EM_WD_GetImage_Test_ScoresTableAdapter.ClearBeforeFill = True
            '
            'Image_Viewer
            '
            Me.Image_Viewer.Location = New System.Drawing.Point(4, 2)
            Me.Image_Viewer.MinimumSize = New System.Drawing.Size(20, 20)
            Me.Image_Viewer.Name = "Image_Viewer"
            Me.Image_Viewer.Size = New System.Drawing.Size(460, 642)
            Me.Image_Viewer.TabIndex = 70
            '
            'cbx_GPA
            '
            Me.cbx_GPA.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
            Me.cbx_GPA.FormattingEnabled = True
            Me.cbx_GPA.Items.AddRange(New Object() {"Enter GPA below", "Set 'ADM' for GPA", "Set 'OPS' for GPA"})
            Me.cbx_GPA.Location = New System.Drawing.Point(743, 247)
            Me.cbx_GPA.Name = "cbx_GPA"
            Me.cbx_GPA.Size = New System.Drawing.Size(121, 21)
            Me.cbx_GPA.TabIndex = 72
            '
            'lblTestReceived
            '
            Me.lblTestReceived.AutoSize = True
            Me.lblTestReceived.Location = New System.Drawing.Point(743, 222)
            Me.lblTestReceived.Name = "lblTestReceived"
            Me.lblTestReceived.Size = New System.Drawing.Size(77, 13)
            Me.lblTestReceived.TabIndex = 73
            Me.lblTestReceived.Text = "Test Received"
            '
            'rdlTest_Yes
            '
            Me.rdlTest_Yes.AutoSize = True
            Me.rdlTest_Yes.Location = New System.Drawing.Point(3, 7)
            Me.rdlTest_Yes.Name = "rdlTest_Yes"
            Me.rdlTest_Yes.Size = New System.Drawing.Size(43, 17)
            Me.rdlTest_Yes.TabIndex = 74
            Me.rdlTest_Yes.TabStop = True
            Me.rdlTest_Yes.Text = "Yes"
            Me.rdlTest_Yes.UseVisualStyleBackColor = True
            '
            'rdlTest_No
            '
            Me.rdlTest_No.AutoSize = True
            Me.rdlTest_No.Checked = True
            Me.rdlTest_No.Location = New System.Drawing.Point(45, 7)
            Me.rdlTest_No.Name = "rdlTest_No"
            Me.rdlTest_No.Size = New System.Drawing.Size(39, 17)
            Me.rdlTest_No.TabIndex = 75
            Me.rdlTest_No.TabStop = True
            Me.rdlTest_No.Text = "No"
            Me.rdlTest_No.UseVisualStyleBackColor = True
            '
            'pnlTestScores
            '
            Me.pnlTestScores.Controls.Add(Me.rdlTest_No)
            Me.pnlTestScores.Controls.Add(Me.rdlTest_Yes)
            Me.pnlTestScores.Location = New System.Drawing.Point(846, 213)
            Me.pnlTestScores.Name = "pnlTestScores"
            Me.pnlTestScores.Size = New System.Drawing.Size(94, 28)
            Me.pnlTestScores.TabIndex = 76
            '
            'DP_WDP
            '
            Me.AllowDrop = True
            Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
            Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
            Me.ClientSize = New System.Drawing.Size(1028, 650)
            Me.Controls.Add(Me.btnEnter_Scores)
            Me.Controls.Add(Me.pnlTestScores)
            Me.Controls.Add(Me.lblTestReceived)
            Me.Controls.Add(Me.cbx_GPA)
            Me.Controls.Add(Me.Image_Viewer)
            Me.Controls.Add(Me.btnGet_Databank)
            Me.Controls.Add(Me.lblSearch_Error)
            Me.Controls.Add(Me.btnCollege_Search)
            Me.Controls.Add(Me.btnGet_Exception)
            Me.Controls.Add(Me.lblGPA)
            Me.Controls.Add(Me.btnGet_Problem)
            Me.Controls.Add(Me.txtHS_GPA)
            Me.Controls.Add(Me.dgImages)
            Me.Controls.Add(Me.btnHS_Search)
            Me.Controls.Add(Me.lblVersion)
            Me.Controls.Add(Me.txtCEEBCode)
            Me.Controls.Add(Me.lblImages_Processed)
            Me.Controls.Add(Me.txtCity)
            Me.Controls.Add(Me.Label5)
            Me.Controls.Add(Me.Label32)
            Me.Controls.Add(Me.Label31)
            Me.Controls.Add(Me.TabControl2)
            Me.Controls.Add(Me.Label30)
            Me.Controls.Add(Me.btnGet_DP)
            Me.Controls.Add(Me.txtHS_ClassSize)
            Me.Controls.Add(Me.lblClassSize)
            Me.Controls.Add(Me.lblGradDate)
            Me.Controls.Add(Me.cbxNoSchool)
            Me.Controls.Add(Me.lblRIC)
            Me.Controls.Add(Me.lblNo_School)
            Me.Controls.Add(Me.txtHS_RIC)
            Me.Controls.Add(Me.dtHSGradDate)
            Me.Controls.Add(Me.txtZipCode)
            Me.Controls.Add(Me.cbxCeebName)
            Me.Controls.Add(Me.lblName)
            Me.Name = "DP_WDP"
            Me.Text = "Form1"
            Me.TabControl2.ResumeLayout(False)
            Me.TabPage3.ResumeLayout(False)
            Me.TabPage3.PerformLayout()
            Me.TabControl4.ResumeLayout(False)
            Me.TabPage4.ResumeLayout(False)
            Me.TabPage4.PerformLayout()
            Me.TabPage5.ResumeLayout(False)
            Me.TabPage5.PerformLayout()
            Me.GroupBox1.ResumeLayout(False)
            Me.GroupBox1.PerformLayout()
            Me.TabPage6.ResumeLayout(False)
            Me.gbx_Payments.ResumeLayout(False)
            Me.gbx_Payments.PerformLayout()
            Me.TabControl3.ResumeLayout(False)
            Me.TabPage1.ResumeLayout(False)
            Me.TabPage1.PerformLayout()
            Me.TabPage2.ResumeLayout(False)
            Me.TabPage2.PerformLayout()
            CType(Me.EM_WD_GetImage_Test_ScoresBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.DsTest_Scores, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.OABannerTESCselBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.DsTestCodesBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.ErrorProvider1, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.dgImages, System.ComponentModel.ISupportInitialize).EndInit()
            Me.pnlTestScores.ResumeLayout(False)
            Me.pnlTestScores.PerformLayout()
            Me.ResumeLayout(False)
            Me.PerformLayout()

        End Sub
        Friend WithEvents Label1 As System.Windows.Forms.Label
        Friend WithEvents cbxCeebName As System.Windows.Forms.ComboBox
        Friend WithEvents dtHSGradDate As System.Windows.Forms.DateTimePicker
        Friend WithEvents btnGet_DP As System.Windows.Forms.Button
        Friend WithEvents txtBannerID As System.Windows.Forms.TextBox
        Friend WithEvents txtAdmrComment As System.Windows.Forms.TextBox
        Friend WithEvents txtHS_RIC As System.Windows.Forms.TextBox
        Friend WithEvents rdlCommentType_EM As System.Windows.Forms.RadioButton
        Friend WithEvents rdlCommentType_EMO As System.Windows.Forms.RadioButton
        Friend WithEvents txtGeneralComment As System.Windows.Forms.TextBox
        Friend WithEvents btnSubmit As System.Windows.Forms.Button
        Friend WithEvents Label2 As System.Windows.Forms.Label
        Friend WithEvents Label3 As System.Windows.Forms.Label
        Friend WithEvents Label4 As System.Windows.Forms.Label
        Friend WithEvents lblName As System.Windows.Forms.Label
        Friend WithEvents lblRIC As System.Windows.Forms.Label
        Friend WithEvents lblGradDate As System.Windows.Forms.Label
        Friend WithEvents Label14 As System.Windows.Forms.Label
        Friend WithEvents Label15 As System.Windows.Forms.Label
        Friend WithEvents TabControl2 As System.Windows.Forms.TabControl
        Friend WithEvents TabPage3 As System.Windows.Forms.TabPage
        Friend WithEvents Label23 As System.Windows.Forms.Label
        Friend WithEvents Label20 As System.Windows.Forms.Label
        Friend WithEvents txtSSN1 As System.Windows.Forms.TextBox
        Friend WithEvents Label19 As System.Windows.Forms.Label
        Friend WithEvents Label11 As System.Windows.Forms.Label
        Friend WithEvents txtZipCode As System.Windows.Forms.TextBox
        Friend WithEvents TabPage6 As System.Windows.Forms.TabPage
        Friend WithEvents dtDOB As System.Windows.Forms.DateTimePicker
        Friend WithEvents cbxSeq3 As System.Windows.Forms.ComboBox
        Friend WithEvents cbxSeq2 As System.Windows.Forms.ComboBox
        Friend WithEvents cbxSeq1 As System.Windows.Forms.ComboBox
        Friend WithEvents ErrorProvider1 As System.Windows.Forms.ErrorProvider
        Friend WithEvents cbxAdmrCode As System.Windows.Forms.ComboBox
        Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
        Friend WithEvents cbxSourceCode As System.Windows.Forms.ComboBox
        Friend WithEvents cbxGender As System.Windows.Forms.ComboBox
        Friend WithEvents cbxAttributeCode1 As System.Windows.Forms.ComboBox
        Friend WithEvents cbxInterestCode1 As System.Windows.Forms.ComboBox
        Friend WithEvents cbxInterestCode2 As System.Windows.Forms.ComboBox
        Friend WithEvents txtSSN3 As System.Windows.Forms.TextBox
        Friend WithEvents txtSSN2 As System.Windows.Forms.TextBox
        Friend WithEvents cbxNoSchool As System.Windows.Forms.ComboBox
        Friend WithEvents lblNo_School As System.Windows.Forms.Label
        Friend WithEvents Label22 As System.Windows.Forms.Label
        Friend WithEvents chkbxMandatory As System.Windows.Forms.CheckBox
        Friend WithEvents txtHS_ClassSize As System.Windows.Forms.TextBox
        Friend WithEvents lblClassSize As System.Windows.Forms.Label
        Friend WithEvents cbxContactCode As System.Windows.Forms.ComboBox
        Friend WithEvents Label16 As System.Windows.Forms.Label
        Friend WithEvents gbx_Payments As System.Windows.Forms.GroupBox
        Friend WithEvents cbxPayment_Department As System.Windows.Forms.ComboBox
        Friend WithEvents cbxPayment_Method As System.Windows.Forms.ComboBox
        Friend WithEvents cbxPayment_Type As System.Windows.Forms.ComboBox
        Friend WithEvents Label8 As System.Windows.Forms.Label
        Friend WithEvents Label6 As System.Windows.Forms.Label
        Friend WithEvents Label7 As System.Windows.Forms.Label
        Friend WithEvents lblImages_Processed As System.Windows.Forms.Label
        Friend WithEvents Label5 As System.Windows.Forms.Label
        Friend WithEvents btnGet_Problem As System.Windows.Forms.Button
        Friend WithEvents lblVersion As System.Windows.Forms.Label
        Friend WithEvents dgImages As System.Windows.Forms.DataGridView
        Friend WithEvents TabControl3 As System.Windows.Forms.TabControl
        Friend WithEvents TabPage1 As System.Windows.Forms.TabPage
        Friend WithEvents btnProblem_Submit As System.Windows.Forms.Button
        Friend WithEvents Label18 As System.Windows.Forms.Label
        Friend WithEvents txtProblem_Comments As System.Windows.Forms.TextBox
        Friend WithEvents Label10 As System.Windows.Forms.Label
        Friend WithEvents TabPage2 As System.Windows.Forms.TabPage
        Friend WithEvents Label24 As System.Windows.Forms.Label
        Friend WithEvents btnException_Submit As System.Windows.Forms.Button
        Friend WithEvents txtExceptionComment As System.Windows.Forms.TextBox
        Friend WithEvents btnUnclaimed_Submit As System.Windows.Forms.Button
        Friend WithEvents lblUnclaimed_Last_Name As System.Windows.Forms.Label
        Friend WithEvents txtUnclaimed_First_Name As System.Windows.Forms.TextBox
        Friend WithEvents txtUnclaimed_Last_Name As System.Windows.Forms.TextBox
        Friend WithEvents lblUnclaimed_First_Name As System.Windows.Forms.Label
        Friend WithEvents cbxProblem_Type As System.Windows.Forms.ComboBox
        Friend WithEvents btnGet_Exception As System.Windows.Forms.Button
        Friend WithEvents lblException_Follow_Up As System.Windows.Forms.Label
        Friend WithEvents txtExceptionComment_Follow_up As System.Windows.Forms.TextBox
        Friend WithEvents btnCollege_Search As System.Windows.Forms.Button
        Friend WithEvents btnHS_Search As System.Windows.Forms.Button
        Friend WithEvents txtCEEBCode As System.Windows.Forms.TextBox
        Friend WithEvents txtCity As System.Windows.Forms.TextBox
        Friend WithEvents Label32 As System.Windows.Forms.Label
        Friend WithEvents Label31 As System.Windows.Forms.Label
        Friend WithEvents Label30 As System.Windows.Forms.Label
        Friend WithEvents lblGPA As System.Windows.Forms.Label
        Friend WithEvents txtHS_GPA As System.Windows.Forms.TextBox
        Friend WithEvents txtHS_Grade As System.Windows.Forms.TextBox
        Friend WithEvents lblSearch_Error As System.Windows.Forms.Label
        Friend WithEvents TabControl4 As System.Windows.Forms.TabControl
        Friend WithEvents TabPage4 As System.Windows.Forms.TabPage
        Friend WithEvents TabPage5 As System.Windows.Forms.TabPage
        Friend WithEvents btnGet_Databank As System.Windows.Forms.Button
        Friend WithEvents TableAdapterManager As EM_Work_Dist_V2.dsTest_ScoresTableAdapters.TableAdapterManager
        Friend WithEvents DsTest_Scores As EM_Work_Dist_V2.dsTest_Scores
        Friend WithEvents EM_WD_GetImage_Test_ScoresBindingSource As System.Windows.Forms.BindingSource
        Friend WithEvents EM_WD_GetImage_Test_ScoresTableAdapter As EM_Work_Dist_V2.dsTest_ScoresTableAdapters.EM_WD_GetImage_Test_ScoresTableAdapter
        Friend WithEvents DsTestCodesBindingSource As System.Windows.Forms.BindingSource
        'Friend WithEvents DsTest_Codes As EM_Work_Dist_V2.dsTest_Codes
        Friend WithEvents OABannerTESCselBindingSource As System.Windows.Forms.BindingSource
        'Friend WithEvents OA_Banner_TESC_selTableAdapter As EM_Work_Dist_V2.dsTest_CodesTableAdapters.OA_Banner_TESC_selTableAdapter
        Friend WithEvents Image_Viewer As System.Windows.Forms.WebBrowser
        Friend WithEvents btnSubmit_Matric As System.Windows.Forms.Button
        Friend WithEvents cbx_GPA As System.Windows.Forms.ComboBox
        Friend WithEvents btnEnter_Scores As System.Windows.Forms.Button
        Friend WithEvents rdlTest_No As System.Windows.Forms.RadioButton
        Friend WithEvents rdlTest_Yes As System.Windows.Forms.RadioButton
        Friend WithEvents lblTestReceived As System.Windows.Forms.Label
        Friend WithEvents pnlTestScores As System.Windows.Forms.Panel

    End Class
End Namespace
