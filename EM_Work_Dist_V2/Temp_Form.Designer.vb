﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Temp_Form
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(Temp_Form))
        Me.btnGet_Exception = New System.Windows.Forms.Button()
        Me.btnGet_Problem = New System.Windows.Forms.Button()
        Me.dgImages = New System.Windows.Forms.DataGridView()
        Me.btnGet_HS_Transcript = New System.Windows.Forms.Button()
        Me.TabControl2 = New System.Windows.Forms.TabControl()
        Me.TabPage3 = New System.Windows.Forms.TabPage()
        Me.TabControl3 = New System.Windows.Forms.TabControl()
        Me.TabPage1 = New System.Windows.Forms.TabPage()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.txtBannerID = New System.Windows.Forms.TextBox()
        Me.btnSubmit = New System.Windows.Forms.Button()
        Me.cbxSeq1 = New System.Windows.Forms.ComboBox()
        Me.cbxSeq2 = New System.Windows.Forms.ComboBox()
        Me.TabPage2 = New System.Windows.Forms.TabPage()
        Me.TextBox5 = New System.Windows.Forms.TextBox()
        Me.Label21 = New System.Windows.Forms.Label()
        Me.Label17 = New System.Windows.Forms.Label()
        Me.Button3 = New System.Windows.Forms.Button()
        Me.TextBox6 = New System.Windows.Forms.TextBox()
        Me.DataGridView1 = New System.Windows.Forms.DataGridView()
        Me.cbxAttributeCode1 = New System.Windows.Forms.ComboBox()
        Me.Label15 = New System.Windows.Forms.Label()
        Me.cbxInterestCode1 = New System.Windows.Forms.ComboBox()
        Me.cbxInterestCode2 = New System.Windows.Forms.ComboBox()
        Me.Label14 = New System.Windows.Forms.Label()
        Me.txtSSN3 = New System.Windows.Forms.TextBox()
        Me.txtSSN2 = New System.Windows.Forms.TextBox()
        Me.cbxSourceCode = New System.Windows.Forms.ComboBox()
        Me.cbxGender = New System.Windows.Forms.ComboBox()
        Me.dtDOB = New System.Windows.Forms.DateTimePicker()
        Me.Label23 = New System.Windows.Forms.Label()
        Me.Label20 = New System.Windows.Forms.Label()
        Me.txtSSN1 = New System.Windows.Forms.TextBox()
        Me.Label19 = New System.Windows.Forms.Label()
        Me.Label11 = New System.Windows.Forms.Label()
        Me.GroupBox3 = New System.Windows.Forms.GroupBox()
        Me.RadioButton1 = New System.Windows.Forms.RadioButton()
        Me.RadioButton2 = New System.Windows.Forms.RadioButton()
        Me.TextBox1 = New System.Windows.Forms.TextBox()
        Me.cbxContactCode = New System.Windows.Forms.ComboBox()
        Me.Label16 = New System.Windows.Forms.Label()
        Me.Label22 = New System.Windows.Forms.Label()
        Me.chkbxMandatory = New System.Windows.Forms.CheckBox()
        Me.cbxAdmrCode = New System.Windows.Forms.ComboBox()
        Me.txtAdmrComment = New System.Windows.Forms.TextBox()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.TabPage4 = New System.Windows.Forms.TabPage()
        Me.TabControl4 = New System.Windows.Forms.TabControl()
        Me.TabPage5 = New System.Windows.Forms.TabPage()
        Me.ComboBox1 = New System.Windows.Forms.ComboBox()
        Me.Button1 = New System.Windows.Forms.Button()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.TextBox2 = New System.Windows.Forms.TextBox()
        Me.Label9 = New System.Windows.Forms.Label()
        Me.TabPage6 = New System.Windows.Forms.TabPage()
        Me.Label12 = New System.Windows.Forms.Label()
        Me.TextBox3 = New System.Windows.Forms.TextBox()
        Me.Label13 = New System.Windows.Forms.Label()
        Me.Button2 = New System.Windows.Forms.Button()
        Me.TextBox4 = New System.Windows.Forms.TextBox()
        Me.TabPage8 = New System.Windows.Forms.TabPage()
        Me.gbx_Payments = New System.Windows.Forms.GroupBox()
        Me.cbxPayment_Department = New System.Windows.Forms.ComboBox()
        Me.cbxPayment_Method = New System.Windows.Forms.ComboBox()
        Me.cbxPayment_Type = New System.Windows.Forms.ComboBox()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.btnFeedMe = New System.Windows.Forms.Button()
        Me.Label10 = New System.Windows.Forms.Label()
        Me.ComboBox2 = New System.Windows.Forms.ComboBox()
        Me.Button4 = New System.Windows.Forms.Button()
        Me.Label18 = New System.Windows.Forms.Label()
        Me.lblHS_Search_Error = New System.Windows.Forms.Label()
        Me.Label33 = New System.Windows.Forms.Label()
        Me.txtHS_GPA = New System.Windows.Forms.TextBox()
        Me.btnHS_Search = New System.Windows.Forms.Button()
        Me.txtHSCEEBCode = New System.Windows.Forms.TextBox()
        Me.txtHSCity = New System.Windows.Forms.TextBox()
        Me.Label32 = New System.Windows.Forms.Label()
        Me.Label31 = New System.Windows.Forms.Label()
        Me.Label30 = New System.Windows.Forms.Label()
        Me.txtHS_ClassSize = New System.Windows.Forms.TextBox()
        Me.lblHS_ClassSize = New System.Windows.Forms.Label()
        Me.cbxHSNoSchool = New System.Windows.Forms.ComboBox()
        Me.lblNo_School = New System.Windows.Forms.Label()
        Me.txtHSZipCode = New System.Windows.Forms.TextBox()
        Me.lblHS_Name = New System.Windows.Forms.Label()
        Me.cbxHSCeebName = New System.Windows.Forms.ComboBox()
        Me.dtHSGradDate = New System.Windows.Forms.DateTimePicker()
        Me.txtHS_RIC = New System.Windows.Forms.TextBox()
        Me.lblHS_RIC = New System.Windows.Forms.Label()
        Me.lblHSGradDate = New System.Windows.Forms.Label()
        Me.Label24 = New System.Windows.Forms.Label()
        Me.Label25 = New System.Windows.Forms.Label()
        CType(Me.dgImages, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.TabControl2.SuspendLayout()
        Me.TabPage3.SuspendLayout()
        Me.TabControl3.SuspendLayout()
        Me.TabPage1.SuspendLayout()
        Me.TabPage2.SuspendLayout()
        CType(Me.DataGridView1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox3.SuspendLayout()
        Me.TabPage4.SuspendLayout()
        Me.TabControl4.SuspendLayout()
        Me.TabPage5.SuspendLayout()
        Me.TabPage6.SuspendLayout()
        Me.gbx_Payments.SuspendLayout()
        Me.SuspendLayout()
        '
        'btnGet_Exception
        '
        Me.btnGet_Exception.Location = New System.Drawing.Point(769, 521)
        Me.btnGet_Exception.Name = "btnGet_Exception"
        Me.btnGet_Exception.Size = New System.Drawing.Size(133, 20)
        Me.btnGet_Exception.TabIndex = 73
        Me.btnGet_Exception.Text = "Get Databank"
        Me.btnGet_Exception.UseVisualStyleBackColor = True
        '
        'btnGet_Problem
        '
        Me.btnGet_Problem.CausesValidation = False
        Me.btnGet_Problem.Location = New System.Drawing.Point(769, 598)
        Me.btnGet_Problem.Name = "btnGet_Problem"
        Me.btnGet_Problem.Size = New System.Drawing.Size(133, 19)
        Me.btnGet_Problem.TabIndex = 68
        Me.btnGet_Problem.TabStop = False
        Me.btnGet_Problem.Text = "Get Exception"
        Me.btnGet_Problem.UseVisualStyleBackColor = True
        '
        'dgImages
        '
        Me.dgImages.AllowUserToAddRows = False
        Me.dgImages.AllowUserToDeleteRows = False
        Me.dgImages.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgImages.Location = New System.Drawing.Point(742, 270)
        Me.dgImages.Name = "dgImages"
        Me.dgImages.ShowEditingIcon = False
        Me.dgImages.Size = New System.Drawing.Size(276, 245)
        Me.dgImages.TabIndex = 71
        '
        'btnGet_HS_Transcript
        '
        Me.btnGet_HS_Transcript.Location = New System.Drawing.Point(769, 547)
        Me.btnGet_HS_Transcript.Name = "btnGet_HS_Transcript"
        Me.btnGet_HS_Transcript.Size = New System.Drawing.Size(133, 19)
        Me.btnGet_HS_Transcript.TabIndex = 70
        Me.btnGet_HS_Transcript.TabStop = False
        Me.btnGet_HS_Transcript.Text = "Get DP"
        Me.btnGet_HS_Transcript.UseVisualStyleBackColor = True
        '
        'TabControl2
        '
        Me.TabControl2.Controls.Add(Me.TabPage3)
        Me.TabControl2.Controls.Add(Me.TabPage4)
        Me.TabControl2.Location = New System.Drawing.Point(470, 13)
        Me.TabControl2.Name = "TabControl2"
        Me.TabControl2.SelectedIndex = 0
        Me.TabControl2.Size = New System.Drawing.Size(267, 635)
        Me.TabControl2.TabIndex = 67
        Me.TabControl2.TabStop = False
        '
        'TabPage3
        '
        Me.TabPage3.Controls.Add(Me.TabControl3)
        Me.TabPage3.Controls.Add(Me.DataGridView1)
        Me.TabPage3.Controls.Add(Me.cbxAttributeCode1)
        Me.TabPage3.Controls.Add(Me.Label15)
        Me.TabPage3.Controls.Add(Me.cbxInterestCode1)
        Me.TabPage3.Controls.Add(Me.cbxInterestCode2)
        Me.TabPage3.Controls.Add(Me.Label14)
        Me.TabPage3.Controls.Add(Me.txtSSN3)
        Me.TabPage3.Controls.Add(Me.txtSSN2)
        Me.TabPage3.Controls.Add(Me.cbxSourceCode)
        Me.TabPage3.Controls.Add(Me.cbxGender)
        Me.TabPage3.Controls.Add(Me.dtDOB)
        Me.TabPage3.Controls.Add(Me.Label23)
        Me.TabPage3.Controls.Add(Me.Label20)
        Me.TabPage3.Controls.Add(Me.txtSSN1)
        Me.TabPage3.Controls.Add(Me.Label19)
        Me.TabPage3.Controls.Add(Me.Label11)
        Me.TabPage3.Controls.Add(Me.GroupBox3)
        Me.TabPage3.Controls.Add(Me.TextBox1)
        Me.TabPage3.Controls.Add(Me.cbxContactCode)
        Me.TabPage3.Controls.Add(Me.Label16)
        Me.TabPage3.Controls.Add(Me.Label22)
        Me.TabPage3.Controls.Add(Me.chkbxMandatory)
        Me.TabPage3.Controls.Add(Me.cbxAdmrCode)
        Me.TabPage3.Controls.Add(Me.txtAdmrComment)
        Me.TabPage3.Controls.Add(Me.Label3)
        Me.TabPage3.Controls.Add(Me.Label4)
        Me.TabPage3.Controls.Add(Me.Label10)
        Me.TabPage3.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TabPage3.Location = New System.Drawing.Point(4, 22)
        Me.TabPage3.Name = "TabPage3"
        Me.TabPage3.Padding = New System.Windows.Forms.Padding(3)
        Me.TabPage3.Size = New System.Drawing.Size(259, 609)
        Me.TabPage3.TabIndex = 0
        Me.TabPage3.Text = "General"
        Me.TabPage3.UseVisualStyleBackColor = True
        '
        'TabControl3
        '
        Me.TabControl3.Controls.Add(Me.TabPage1)
        Me.TabControl3.Controls.Add(Me.TabPage2)
        Me.TabControl3.Location = New System.Drawing.Point(5, 380)
        Me.TabControl3.Name = "TabControl3"
        Me.TabControl3.SelectedIndex = 0
        Me.TabControl3.Size = New System.Drawing.Size(247, 142)
        Me.TabControl3.TabIndex = 373
        '
        'TabPage1
        '
        Me.TabPage1.Controls.Add(Me.ComboBox2)
        Me.TabPage1.Controls.Add(Me.Label1)
        Me.TabPage1.Controls.Add(Me.Label2)
        Me.TabPage1.Controls.Add(Me.txtBannerID)
        Me.TabPage1.Controls.Add(Me.btnSubmit)
        Me.TabPage1.Controls.Add(Me.cbxSeq1)
        Me.TabPage1.Controls.Add(Me.cbxSeq2)
        Me.TabPage1.Location = New System.Drawing.Point(4, 22)
        Me.TabPage1.Name = "TabPage1"
        Me.TabPage1.Padding = New System.Windows.Forms.Padding(3)
        Me.TabPage1.Size = New System.Drawing.Size(239, 116)
        Me.TabPage1.TabIndex = 0
        Me.TabPage1.Text = "Entry"
        Me.TabPage1.UseVisualStyleBackColor = True
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(10, 14)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(18, 13)
        Me.Label1.TabIndex = 0
        Me.Label1.Text = "ID"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(10, 44)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(36, 13)
        Me.Label2.TabIndex = 24
        Me.Label2.Text = "Seq #"
        '
        'txtBannerID
        '
        Me.txtBannerID.Location = New System.Drawing.Point(52, 11)
        Me.txtBannerID.MaxLength = 10
        Me.txtBannerID.Name = "txtBannerID"
        Me.txtBannerID.Size = New System.Drawing.Size(100, 20)
        Me.txtBannerID.TabIndex = 1
        '
        'btnSubmit
        '
        Me.btnSubmit.Location = New System.Drawing.Point(98, 81)
        Me.btnSubmit.Name = "btnSubmit"
        Me.btnSubmit.Size = New System.Drawing.Size(133, 29)
        Me.btnSubmit.TabIndex = 65
        Me.btnSubmit.TabStop = False
        Me.btnSubmit.Text = "Submit"
        Me.btnSubmit.UseVisualStyleBackColor = True
        '
        'cbxSeq1
        '
        Me.cbxSeq1.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cbxSeq1.FormattingEnabled = True
        Me.cbxSeq1.Items.AddRange(New Object() {"N/A", "1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12", "13", "14", "15"})
        Me.cbxSeq1.Location = New System.Drawing.Point(52, 41)
        Me.cbxSeq1.Name = "cbxSeq1"
        Me.cbxSeq1.Size = New System.Drawing.Size(40, 21)
        Me.cbxSeq1.TabIndex = 2
        '
        'cbxSeq2
        '
        Me.cbxSeq2.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cbxSeq2.FormattingEnabled = True
        Me.cbxSeq2.Items.AddRange(New Object() {"N/A", "1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12", "13", "14", "15"})
        Me.cbxSeq2.Location = New System.Drawing.Point(104, 41)
        Me.cbxSeq2.Name = "cbxSeq2"
        Me.cbxSeq2.Size = New System.Drawing.Size(40, 21)
        Me.cbxSeq2.TabIndex = 28
        Me.cbxSeq2.TabStop = False
        '
        'TabPage2
        '
        Me.TabPage2.Controls.Add(Me.TextBox5)
        Me.TabPage2.Controls.Add(Me.Label21)
        Me.TabPage2.Controls.Add(Me.Label17)
        Me.TabPage2.Controls.Add(Me.Button3)
        Me.TabPage2.Controls.Add(Me.TextBox6)
        Me.TabPage2.Location = New System.Drawing.Point(4, 22)
        Me.TabPage2.Name = "TabPage2"
        Me.TabPage2.Padding = New System.Windows.Forms.Padding(3)
        Me.TabPage2.Size = New System.Drawing.Size(239, 116)
        Me.TabPage2.TabIndex = 1
        Me.TabPage2.Text = "Unclaimed"
        Me.TabPage2.UseVisualStyleBackColor = True
        '
        'TextBox5
        '
        Me.TextBox5.Location = New System.Drawing.Point(84, 49)
        Me.TextBox5.Name = "TextBox5"
        Me.TextBox5.Size = New System.Drawing.Size(149, 20)
        Me.TextBox5.TabIndex = 373
        '
        'Label21
        '
        Me.Label21.AutoSize = True
        Me.Label21.Location = New System.Drawing.Point(6, 52)
        Me.Label21.Name = "Label21"
        Me.Label21.Size = New System.Drawing.Size(57, 13)
        Me.Label21.TabIndex = 375
        Me.Label21.Text = "First Name"
        '
        'Label17
        '
        Me.Label17.AutoSize = True
        Me.Label17.Location = New System.Drawing.Point(6, 18)
        Me.Label17.Name = "Label17"
        Me.Label17.Size = New System.Drawing.Size(58, 13)
        Me.Label17.TabIndex = 376
        Me.Label17.Text = "Last Name"
        '
        'Button3
        '
        Me.Button3.Location = New System.Drawing.Point(122, 75)
        Me.Button3.Name = "Button3"
        Me.Button3.Size = New System.Drawing.Size(108, 35)
        Me.Button3.TabIndex = 377
        Me.Button3.Text = "Submit Unclaimed"
        Me.Button3.UseVisualStyleBackColor = True
        '
        'TextBox6
        '
        Me.TextBox6.Location = New System.Drawing.Point(84, 15)
        Me.TextBox6.Name = "TextBox6"
        Me.TextBox6.Size = New System.Drawing.Size(149, 20)
        Me.TextBox6.TabIndex = 374
        '
        'DataGridView1
        '
        Me.DataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.DataGridView1.Location = New System.Drawing.Point(3, 195)
        Me.DataGridView1.Name = "DataGridView1"
        Me.DataGridView1.Size = New System.Drawing.Size(252, 81)
        Me.DataGridView1.TabIndex = 367
        '
        'cbxAttributeCode1
        '
        Me.cbxAttributeCode1.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cbxAttributeCode1.FormattingEnabled = True
        Me.cbxAttributeCode1.Location = New System.Drawing.Point(87, 91)
        Me.cbxAttributeCode1.Name = "cbxAttributeCode1"
        Me.cbxAttributeCode1.Size = New System.Drawing.Size(45, 21)
        Me.cbxAttributeCode1.TabIndex = 366
        '
        'Label15
        '
        Me.Label15.AutoSize = True
        Me.Label15.Location = New System.Drawing.Point(6, 94)
        Me.Label15.Name = "Label15"
        Me.Label15.Size = New System.Drawing.Size(74, 13)
        Me.Label15.TabIndex = 365
        Me.Label15.Text = "Attribute Code"
        '
        'cbxInterestCode1
        '
        Me.cbxInterestCode1.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cbxInterestCode1.FormattingEnabled = True
        Me.cbxInterestCode1.Location = New System.Drawing.Point(87, 65)
        Me.cbxInterestCode1.Name = "cbxInterestCode1"
        Me.cbxInterestCode1.Size = New System.Drawing.Size(45, 21)
        Me.cbxInterestCode1.TabIndex = 364
        '
        'cbxInterestCode2
        '
        Me.cbxInterestCode2.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cbxInterestCode2.FormattingEnabled = True
        Me.cbxInterestCode2.Location = New System.Drawing.Point(141, 65)
        Me.cbxInterestCode2.Name = "cbxInterestCode2"
        Me.cbxInterestCode2.Size = New System.Drawing.Size(45, 21)
        Me.cbxInterestCode2.TabIndex = 363
        Me.cbxInterestCode2.TabStop = False
        '
        'Label14
        '
        Me.Label14.AutoSize = True
        Me.Label14.Location = New System.Drawing.Point(6, 68)
        Me.Label14.Name = "Label14"
        Me.Label14.Size = New System.Drawing.Size(75, 13)
        Me.Label14.TabIndex = 362
        Me.Label14.Text = "Interest Codes"
        '
        'txtSSN3
        '
        Me.txtSSN3.Location = New System.Drawing.Point(207, 169)
        Me.txtSSN3.MaxLength = 4
        Me.txtSSN3.Name = "txtSSN3"
        Me.txtSSN3.Size = New System.Drawing.Size(34, 20)
        Me.txtSSN3.TabIndex = 357
        '
        'txtSSN2
        '
        Me.txtSSN2.Location = New System.Drawing.Point(176, 169)
        Me.txtSSN2.MaxLength = 2
        Me.txtSSN2.Name = "txtSSN2"
        Me.txtSSN2.Size = New System.Drawing.Size(23, 20)
        Me.txtSSN2.TabIndex = 355
        '
        'cbxSourceCode
        '
        Me.cbxSourceCode.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cbxSourceCode.FormattingEnabled = True
        Me.cbxSourceCode.Location = New System.Drawing.Point(87, 119)
        Me.cbxSourceCode.Name = "cbxSourceCode"
        Me.cbxSourceCode.Size = New System.Drawing.Size(161, 21)
        Me.cbxSourceCode.TabIndex = 360
        '
        'cbxGender
        '
        Me.cbxGender.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cbxGender.FormattingEnabled = True
        Me.cbxGender.Items.AddRange(New Object() {"N/A", "M", "F"})
        Me.cbxGender.Location = New System.Drawing.Point(54, 145)
        Me.cbxGender.Name = "cbxGender"
        Me.cbxGender.Size = New System.Drawing.Size(55, 21)
        Me.cbxGender.TabIndex = 358
        '
        'dtDOB
        '
        Me.dtDOB.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtDOB.Location = New System.Drawing.Point(155, 145)
        Me.dtDOB.Name = "dtDOB"
        Me.dtDOB.Size = New System.Drawing.Size(100, 20)
        Me.dtDOB.TabIndex = 353
        '
        'Label23
        '
        Me.Label23.AutoSize = True
        Me.Label23.Location = New System.Drawing.Point(7, 122)
        Me.Label23.Name = "Label23"
        Me.Label23.Size = New System.Drawing.Size(69, 13)
        Me.Label23.TabIndex = 361
        Me.Label23.Text = "Source Code"
        '
        'Label20
        '
        Me.Label20.AutoSize = True
        Me.Label20.Location = New System.Drawing.Point(6, 148)
        Me.Label20.Name = "Label20"
        Me.Label20.Size = New System.Drawing.Size(42, 13)
        Me.Label20.TabIndex = 359
        Me.Label20.Text = "Gender"
        '
        'txtSSN1
        '
        Me.txtSSN1.Location = New System.Drawing.Point(141, 169)
        Me.txtSSN1.MaxLength = 3
        Me.txtSSN1.Name = "txtSSN1"
        Me.txtSSN1.Size = New System.Drawing.Size(26, 20)
        Me.txtSSN1.TabIndex = 354
        '
        'Label19
        '
        Me.Label19.AutoSize = True
        Me.Label19.Location = New System.Drawing.Point(6, 172)
        Me.Label19.Name = "Label19"
        Me.Label19.Size = New System.Drawing.Size(117, 13)
        Me.Label19.TabIndex = 356
        Me.Label19.Text = "Social Security Number"
        '
        'Label11
        '
        Me.Label11.AutoSize = True
        Me.Label11.Location = New System.Drawing.Point(119, 148)
        Me.Label11.Name = "Label11"
        Me.Label11.Size = New System.Drawing.Size(30, 13)
        Me.Label11.TabIndex = 352
        Me.Label11.Text = "DOB"
        '
        'GroupBox3
        '
        Me.GroupBox3.Controls.Add(Me.RadioButton1)
        Me.GroupBox3.Controls.Add(Me.RadioButton2)
        Me.GroupBox3.Location = New System.Drawing.Point(12, 283)
        Me.GroupBox3.Name = "GroupBox3"
        Me.GroupBox3.Size = New System.Drawing.Size(221, 30)
        Me.GroupBox3.TabIndex = 351
        Me.GroupBox3.TabStop = False
        Me.GroupBox3.Text = "Comment"
        '
        'RadioButton1
        '
        Me.RadioButton1.AutoSize = True
        Me.RadioButton1.Location = New System.Drawing.Point(82, 7)
        Me.RadioButton1.Name = "RadioButton1"
        Me.RadioButton1.Size = New System.Drawing.Size(41, 17)
        Me.RadioButton1.TabIndex = 17
        Me.RadioButton1.Text = "EM"
        Me.RadioButton1.UseVisualStyleBackColor = True
        '
        'RadioButton2
        '
        Me.RadioButton2.AutoSize = True
        Me.RadioButton2.Location = New System.Drawing.Point(138, 7)
        Me.RadioButton2.Name = "RadioButton2"
        Me.RadioButton2.Size = New System.Drawing.Size(49, 17)
        Me.RadioButton2.TabIndex = 18
        Me.RadioButton2.Text = "EMO"
        Me.RadioButton2.UseVisualStyleBackColor = True
        '
        'TextBox1
        '
        Me.TextBox1.Location = New System.Drawing.Point(5, 319)
        Me.TextBox1.MaxLength = 4000
        Me.TextBox1.Multiline = True
        Me.TextBox1.Name = "TextBox1"
        Me.TextBox1.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.TextBox1.Size = New System.Drawing.Size(246, 55)
        Me.TextBox1.TabIndex = 105
        Me.TextBox1.TabStop = False
        '
        'cbxContactCode
        '
        Me.cbxContactCode.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cbxContactCode.FormattingEnabled = True
        Me.cbxContactCode.Location = New System.Drawing.Point(211, 91)
        Me.cbxContactCode.Name = "cbxContactCode"
        Me.cbxContactCode.Size = New System.Drawing.Size(45, 21)
        Me.cbxContactCode.TabIndex = 5
        '
        'Label16
        '
        Me.Label16.AutoSize = True
        Me.Label16.Location = New System.Drawing.Point(138, 94)
        Me.Label16.Name = "Label16"
        Me.Label16.Size = New System.Drawing.Size(72, 13)
        Me.Label16.TabIndex = 101
        Me.Label16.Text = "Contact Code"
        '
        'Label22
        '
        Me.Label22.AutoSize = True
        Me.Label22.Location = New System.Drawing.Point(173, 7)
        Me.Label22.Name = "Label22"
        Me.Label22.Size = New System.Drawing.Size(63, 13)
        Me.Label22.TabIndex = 32
        Me.Label22.Text = "Mandatory?"
        '
        'chkbxMandatory
        '
        Me.chkbxMandatory.AutoSize = True
        Me.chkbxMandatory.Checked = True
        Me.chkbxMandatory.CheckState = System.Windows.Forms.CheckState.Checked
        Me.chkbxMandatory.Location = New System.Drawing.Point(237, 7)
        Me.chkbxMandatory.Name = "chkbxMandatory"
        Me.chkbxMandatory.Size = New System.Drawing.Size(15, 14)
        Me.chkbxMandatory.TabIndex = 31
        Me.chkbxMandatory.TabStop = False
        Me.chkbxMandatory.UseVisualStyleBackColor = True
        '
        'cbxAdmrCode
        '
        Me.cbxAdmrCode.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cbxAdmrCode.FormattingEnabled = True
        Me.cbxAdmrCode.Location = New System.Drawing.Point(49, 4)
        Me.cbxAdmrCode.Name = "cbxAdmrCode"
        Me.cbxAdmrCode.Size = New System.Drawing.Size(118, 21)
        Me.cbxAdmrCode.TabIndex = 3
        '
        'txtAdmrComment
        '
        Me.txtAdmrComment.Location = New System.Drawing.Point(94, 32)
        Me.txtAdmrComment.MaxLength = 30
        Me.txtAdmrComment.Name = "txtAdmrComment"
        Me.txtAdmrComment.Size = New System.Drawing.Size(152, 20)
        Me.txtAdmrComment.TabIndex = 4
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(7, 7)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(39, 13)
        Me.Label3.TabIndex = 25
        Me.Label3.Text = "ADMR"
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(7, 35)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(86, 13)
        Me.Label4.TabIndex = 26
        Me.Label4.Text = "ADMR Comment"
        '
        'TabPage4
        '
        Me.TabPage4.Controls.Add(Me.TabControl4)
        Me.TabPage4.Controls.Add(Me.gbx_Payments)
        Me.TabPage4.Location = New System.Drawing.Point(4, 22)
        Me.TabPage4.Name = "TabPage4"
        Me.TabPage4.Padding = New System.Windows.Forms.Padding(3)
        Me.TabPage4.Size = New System.Drawing.Size(259, 609)
        Me.TabPage4.TabIndex = 1
        Me.TabPage4.Text = "Comments"
        Me.TabPage4.UseVisualStyleBackColor = True
        '
        'TabControl4
        '
        Me.TabControl4.Controls.Add(Me.TabPage5)
        Me.TabControl4.Controls.Add(Me.TabPage6)
        Me.TabControl4.Controls.Add(Me.TabPage8)
        Me.TabControl4.Location = New System.Drawing.Point(1, 126)
        Me.TabControl4.Name = "TabControl4"
        Me.TabControl4.SelectedIndex = 0
        Me.TabControl4.Size = New System.Drawing.Size(256, 208)
        Me.TabControl4.TabIndex = 352
        '
        'TabPage5
        '
        Me.TabPage5.Controls.Add(Me.ComboBox1)
        Me.TabPage5.Controls.Add(Me.Button1)
        Me.TabPage5.Controls.Add(Me.Label5)
        Me.TabPage5.Controls.Add(Me.TextBox2)
        Me.TabPage5.Controls.Add(Me.Label9)
        Me.TabPage5.Location = New System.Drawing.Point(4, 22)
        Me.TabPage5.Name = "TabPage5"
        Me.TabPage5.Padding = New System.Windows.Forms.Padding(3)
        Me.TabPage5.Size = New System.Drawing.Size(248, 182)
        Me.TabPage5.TabIndex = 0
        Me.TabPage5.Text = "Problem"
        Me.TabPage5.UseVisualStyleBackColor = True
        '
        'ComboBox1
        '
        Me.ComboBox1.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.ComboBox1.FormattingEnabled = True
        Me.ComboBox1.Location = New System.Drawing.Point(47, 8)
        Me.ComboBox1.Name = "ComboBox1"
        Me.ComboBox1.Size = New System.Drawing.Size(118, 21)
        Me.ComboBox1.TabIndex = 111
        '
        'Button1
        '
        Me.Button1.Location = New System.Drawing.Point(148, 153)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(115, 23)
        Me.Button1.TabIndex = 110
        Me.Button1.Text = "Submit Problem"
        Me.Button1.UseVisualStyleBackColor = True
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Location = New System.Drawing.Point(10, 40)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(56, 13)
        Me.Label5.TabIndex = 109
        Me.Label5.Text = "Comments"
        '
        'TextBox2
        '
        Me.TextBox2.Location = New System.Drawing.Point(72, 40)
        Me.TextBox2.Multiline = True
        Me.TextBox2.Name = "TextBox2"
        Me.TextBox2.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.TextBox2.Size = New System.Drawing.Size(178, 64)
        Me.TextBox2.TabIndex = 108
        '
        'Label9
        '
        Me.Label9.AutoSize = True
        Me.Label9.Location = New System.Drawing.Point(10, 11)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(31, 13)
        Me.Label9.TabIndex = 107
        Me.Label9.Text = "Type"
        '
        'TabPage6
        '
        Me.TabPage6.Controls.Add(Me.Label12)
        Me.TabPage6.Controls.Add(Me.TextBox3)
        Me.TabPage6.Controls.Add(Me.Label13)
        Me.TabPage6.Controls.Add(Me.Button2)
        Me.TabPage6.Controls.Add(Me.TextBox4)
        Me.TabPage6.Location = New System.Drawing.Point(4, 22)
        Me.TabPage6.Name = "TabPage6"
        Me.TabPage6.Padding = New System.Windows.Forms.Padding(3)
        Me.TabPage6.Size = New System.Drawing.Size(248, 182)
        Me.TabPage6.TabIndex = 1
        Me.TabPage6.Text = "Exception"
        Me.TabPage6.UseVisualStyleBackColor = True
        '
        'Label12
        '
        Me.Label12.AutoSize = True
        Me.Label12.Location = New System.Drawing.Point(6, 88)
        Me.Label12.Name = "Label12"
        Me.Label12.Size = New System.Drawing.Size(56, 26)
        Me.Label12.TabIndex = 112
        Me.Label12.Text = "Follow-up" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "Comments"
        Me.Label12.Visible = False
        '
        'TextBox3
        '
        Me.TextBox3.Location = New System.Drawing.Point(66, 85)
        Me.TextBox3.Multiline = True
        Me.TextBox3.Name = "TextBox3"
        Me.TextBox3.Size = New System.Drawing.Size(171, 62)
        Me.TextBox3.TabIndex = 111
        Me.TextBox3.Visible = False
        '
        'Label13
        '
        Me.Label13.AutoSize = True
        Me.Label13.Location = New System.Drawing.Point(6, 14)
        Me.Label13.Name = "Label13"
        Me.Label13.Size = New System.Drawing.Size(56, 13)
        Me.Label13.TabIndex = 110
        Me.Label13.Text = "Comments"
        '
        'Button2
        '
        Me.Button2.Location = New System.Drawing.Point(134, 153)
        Me.Button2.Name = "Button2"
        Me.Button2.Size = New System.Drawing.Size(108, 23)
        Me.Button2.TabIndex = 1
        Me.Button2.Text = "Submit Exception"
        Me.Button2.UseVisualStyleBackColor = True
        '
        'TextBox4
        '
        Me.TextBox4.Location = New System.Drawing.Point(66, 11)
        Me.TextBox4.Multiline = True
        Me.TextBox4.Name = "TextBox4"
        Me.TextBox4.Size = New System.Drawing.Size(175, 62)
        Me.TextBox4.TabIndex = 0
        '
        'TabPage8
        '
        Me.TabPage8.Location = New System.Drawing.Point(4, 22)
        Me.TabPage8.Name = "TabPage8"
        Me.TabPage8.Size = New System.Drawing.Size(248, 182)
        Me.TabPage8.TabIndex = 2
        Me.TabPage8.Text = "Unclaimed"
        Me.TabPage8.UseVisualStyleBackColor = True
        '
        'gbx_Payments
        '
        Me.gbx_Payments.Controls.Add(Me.cbxPayment_Department)
        Me.gbx_Payments.Controls.Add(Me.cbxPayment_Method)
        Me.gbx_Payments.Controls.Add(Me.cbxPayment_Type)
        Me.gbx_Payments.Controls.Add(Me.Label8)
        Me.gbx_Payments.Controls.Add(Me.Label6)
        Me.gbx_Payments.Controls.Add(Me.Label7)
        Me.gbx_Payments.Location = New System.Drawing.Point(17, 7)
        Me.gbx_Payments.Name = "gbx_Payments"
        Me.gbx_Payments.Size = New System.Drawing.Size(227, 97)
        Me.gbx_Payments.TabIndex = 351
        Me.gbx_Payments.TabStop = False
        Me.gbx_Payments.Text = "Matric Payments"
        Me.gbx_Payments.Visible = False
        '
        'cbxPayment_Department
        '
        Me.cbxPayment_Department.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cbxPayment_Department.FormattingEnabled = True
        Me.cbxPayment_Department.Items.AddRange(New Object() {"", "Freshman", "Graduate", "Part-time/Transfer"})
        Me.cbxPayment_Department.Location = New System.Drawing.Point(91, 71)
        Me.cbxPayment_Department.Name = "cbxPayment_Department"
        Me.cbxPayment_Department.Size = New System.Drawing.Size(121, 21)
        Me.cbxPayment_Department.TabIndex = 107
        '
        'cbxPayment_Method
        '
        Me.cbxPayment_Method.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cbxPayment_Method.FormattingEnabled = True
        Me.cbxPayment_Method.Items.AddRange(New Object() {"", "Check", "Credit Card", "Cash", "Waived"})
        Me.cbxPayment_Method.Location = New System.Drawing.Point(91, 43)
        Me.cbxPayment_Method.Name = "cbxPayment_Method"
        Me.cbxPayment_Method.Size = New System.Drawing.Size(121, 21)
        Me.cbxPayment_Method.TabIndex = 106
        '
        'cbxPayment_Type
        '
        Me.cbxPayment_Type.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cbxPayment_Type.FormattingEnabled = True
        Me.cbxPayment_Type.Items.AddRange(New Object() {"", "Application Fee", "Housing", "Tuition", "Unknown"})
        Me.cbxPayment_Type.Location = New System.Drawing.Point(91, 16)
        Me.cbxPayment_Type.Name = "cbxPayment_Type"
        Me.cbxPayment_Type.Size = New System.Drawing.Size(121, 21)
        Me.cbxPayment_Type.TabIndex = 105
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.Location = New System.Drawing.Point(17, 74)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(62, 13)
        Me.Label8.TabIndex = 104
        Me.Label8.Text = "Department"
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Location = New System.Drawing.Point(17, 19)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(31, 13)
        Me.Label6.TabIndex = 102
        Me.Label6.Text = "Type"
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Location = New System.Drawing.Point(17, 46)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(43, 13)
        Me.Label7.TabIndex = 103
        Me.Label7.Text = "Method"
        '
        'btnFeedMe
        '
        Me.btnFeedMe.Location = New System.Drawing.Point(769, 572)
        Me.btnFeedMe.Name = "btnFeedMe"
        Me.btnFeedMe.Size = New System.Drawing.Size(133, 20)
        Me.btnFeedMe.TabIndex = 64
        Me.btnFeedMe.TabStop = False
        Me.btnFeedMe.Text = "Get Problem"
        Me.btnFeedMe.UseVisualStyleBackColor = True
        '
        'Label10
        '
        Me.Label10.AutoSize = True
        Me.Label10.Location = New System.Drawing.Point(2, 51)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(256, 13)
        Me.Label10.TabIndex = 374
        Me.Label10.Text = "---------------------------------------------------------------------------------" & _
    "--"
        '
        'ComboBox2
        '
        Me.ComboBox2.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.ComboBox2.FormattingEnabled = True
        Me.ComboBox2.Items.AddRange(New Object() {"N/A", "1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12", "13", "14", "15"})
        Me.ComboBox2.Location = New System.Drawing.Point(157, 41)
        Me.ComboBox2.Name = "ComboBox2"
        Me.ComboBox2.Size = New System.Drawing.Size(40, 21)
        Me.ComboBox2.TabIndex = 66
        Me.ComboBox2.TabStop = False
        '
        'Button4
        '
        Me.Button4.Location = New System.Drawing.Point(926, 57)
        Me.Button4.Name = "Button4"
        Me.Button4.Size = New System.Drawing.Size(75, 46)
        Me.Button4.TabIndex = 398
        Me.Button4.TabStop = False
        Me.Button4.Text = "Search Colleges"
        Me.Button4.UseVisualStyleBackColor = True
        '
        'Label18
        '
        Me.Label18.AutoSize = True
        Me.Label18.Location = New System.Drawing.Point(739, 100)
        Me.Label18.Name = "Label18"
        Me.Label18.Size = New System.Drawing.Size(283, 13)
        Me.Label18.TabIndex = 397
        Me.Label18.Text = "---------------------------------------------------------------------------------" & _
    "-----------"
        '
        'lblHS_Search_Error
        '
        Me.lblHS_Search_Error.AutoSize = True
        Me.lblHS_Search_Error.Location = New System.Drawing.Point(927, 70)
        Me.lblHS_Search_Error.Name = "lblHS_Search_Error"
        Me.lblHS_Search_Error.Size = New System.Drawing.Size(0, 13)
        Me.lblHS_Search_Error.TabIndex = 396
        '
        'Label33
        '
        Me.Label33.AutoSize = True
        Me.Label33.Location = New System.Drawing.Point(741, 238)
        Me.Label33.Name = "Label33"
        Me.Label33.Size = New System.Drawing.Size(29, 13)
        Me.Label33.TabIndex = 395
        Me.Label33.Text = "GPA"
        '
        'txtHS_GPA
        '
        Me.txtHS_GPA.Location = New System.Drawing.Point(775, 235)
        Me.txtHS_GPA.MaxLength = 4
        Me.txtHS_GPA.Name = "txtHS_GPA"
        Me.txtHS_GPA.Size = New System.Drawing.Size(34, 20)
        Me.txtHS_GPA.TabIndex = 394
        '
        'btnHS_Search
        '
        Me.btnHS_Search.Location = New System.Drawing.Point(926, 13)
        Me.btnHS_Search.Name = "btnHS_Search"
        Me.btnHS_Search.Size = New System.Drawing.Size(75, 38)
        Me.btnHS_Search.TabIndex = 393
        Me.btnHS_Search.TabStop = False
        Me.btnHS_Search.Text = "Search High Schools"
        Me.btnHS_Search.UseVisualStyleBackColor = True
        '
        'txtHSCEEBCode
        '
        Me.txtHSCEEBCode.Location = New System.Drawing.Point(797, 25)
        Me.txtHSCEEBCode.MaxLength = 6
        Me.txtHSCEEBCode.Name = "txtHSCEEBCode"
        Me.txtHSCEEBCode.Size = New System.Drawing.Size(88, 20)
        Me.txtHSCEEBCode.TabIndex = 392
        Me.txtHSCEEBCode.TabStop = False
        '
        'txtHSCity
        '
        Me.txtHSCity.Location = New System.Drawing.Point(797, 77)
        Me.txtHSCity.MaxLength = 20
        Me.txtHSCity.Name = "txtHSCity"
        Me.txtHSCity.Size = New System.Drawing.Size(88, 20)
        Me.txtHSCity.TabIndex = 391
        Me.txtHSCity.TabStop = False
        '
        'Label32
        '
        Me.Label32.AutoSize = True
        Me.Label32.Location = New System.Drawing.Point(741, 80)
        Me.Label32.Name = "Label32"
        Me.Label32.Size = New System.Drawing.Size(24, 13)
        Me.Label32.TabIndex = 390
        Me.Label32.Text = "City"
        '
        'Label31
        '
        Me.Label31.AutoSize = True
        Me.Label31.Location = New System.Drawing.Point(741, 28)
        Me.Label31.Name = "Label31"
        Me.Label31.Size = New System.Drawing.Size(35, 13)
        Me.Label31.TabIndex = 389
        Me.Label31.Text = "CEEB"
        '
        'Label30
        '
        Me.Label30.AutoSize = True
        Me.Label30.Location = New System.Drawing.Point(741, 54)
        Me.Label30.Name = "Label30"
        Me.Label30.Size = New System.Drawing.Size(22, 13)
        Me.Label30.TabIndex = 388
        Me.Label30.Text = "Zip"
        '
        'txtHS_ClassSize
        '
        Me.txtHS_ClassSize.Location = New System.Drawing.Point(986, 235)
        Me.txtHS_ClassSize.MaxLength = 4
        Me.txtHS_ClassSize.Name = "txtHS_ClassSize"
        Me.txtHS_ClassSize.Size = New System.Drawing.Size(34, 20)
        Me.txtHS_ClassSize.TabIndex = 387
        '
        'lblHS_ClassSize
        '
        Me.lblHS_ClassSize.AutoSize = True
        Me.lblHS_ClassSize.Location = New System.Drawing.Point(930, 238)
        Me.lblHS_ClassSize.Name = "lblHS_ClassSize"
        Me.lblHS_ClassSize.Size = New System.Drawing.Size(55, 13)
        Me.lblHS_ClassSize.TabIndex = 382
        Me.lblHS_ClassSize.Text = "Class Size"
        '
        'cbxHSNoSchool
        '
        Me.cbxHSNoSchool.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cbxHSNoSchool.FormattingEnabled = True
        Me.cbxHSNoSchool.Items.AddRange(New Object() {"5,3"})
        Me.cbxHSNoSchool.Location = New System.Drawing.Point(755, 169)
        Me.cbxHSNoSchool.Name = "cbxHSNoSchool"
        Me.cbxHSNoSchool.Size = New System.Drawing.Size(202, 21)
        Me.cbxHSNoSchool.TabIndex = 381
        Me.cbxHSNoSchool.TabStop = False
        '
        'lblNo_School
        '
        Me.lblNo_School.AutoSize = True
        Me.lblNo_School.Location = New System.Drawing.Point(741, 152)
        Me.lblNo_School.Name = "lblNo_School"
        Me.lblNo_School.Size = New System.Drawing.Size(93, 13)
        Me.lblNo_School.TabIndex = 380
        Me.lblNo_School.Text = "School Not Found"
        '
        'txtHSZipCode
        '
        Me.txtHSZipCode.Location = New System.Drawing.Point(797, 51)
        Me.txtHSZipCode.MaxLength = 5
        Me.txtHSZipCode.Name = "txtHSZipCode"
        Me.txtHSZipCode.Size = New System.Drawing.Size(88, 20)
        Me.txtHSZipCode.TabIndex = 383
        Me.txtHSZipCode.TabStop = False
        '
        'lblHS_Name
        '
        Me.lblHS_Name.AutoSize = True
        Me.lblHS_Name.Location = New System.Drawing.Point(741, 112)
        Me.lblHS_Name.Name = "lblHS_Name"
        Me.lblHS_Name.Size = New System.Drawing.Size(83, 13)
        Me.lblHS_Name.TabIndex = 377
        Me.lblHS_Name.Text = "Name of School"
        '
        'cbxHSCeebName
        '
        Me.cbxHSCeebName.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cbxHSCeebName.FormattingEnabled = True
        Me.cbxHSCeebName.Location = New System.Drawing.Point(755, 126)
        Me.cbxHSCeebName.Name = "cbxHSCeebName"
        Me.cbxHSCeebName.Size = New System.Drawing.Size(226, 21)
        Me.cbxHSCeebName.TabIndex = 384
        '
        'dtHSGradDate
        '
        Me.dtHSGradDate.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtHSGradDate.Location = New System.Drawing.Point(803, 209)
        Me.dtHSGradDate.Name = "dtHSGradDate"
        Me.dtHSGradDate.Size = New System.Drawing.Size(104, 20)
        Me.dtHSGradDate.TabIndex = 385
        Me.dtHSGradDate.TabStop = False
        '
        'txtHS_RIC
        '
        Me.txtHS_RIC.Location = New System.Drawing.Point(890, 235)
        Me.txtHS_RIC.MaxLength = 4
        Me.txtHS_RIC.Name = "txtHS_RIC"
        Me.txtHS_RIC.Size = New System.Drawing.Size(34, 20)
        Me.txtHS_RIC.TabIndex = 386
        '
        'lblHS_RIC
        '
        Me.lblHS_RIC.AutoSize = True
        Me.lblHS_RIC.Location = New System.Drawing.Point(815, 238)
        Me.lblHS_RIC.Name = "lblHS_RIC"
        Me.lblHS_RIC.Size = New System.Drawing.Size(72, 13)
        Me.lblHS_RIC.TabIndex = 378
        Me.lblHS_RIC.Text = "Rank in Class"
        '
        'lblHSGradDate
        '
        Me.lblHSGradDate.AutoSize = True
        Me.lblHSGradDate.Location = New System.Drawing.Point(741, 215)
        Me.lblHSGradDate.Name = "lblHSGradDate"
        Me.lblHSGradDate.Size = New System.Drawing.Size(56, 13)
        Me.lblHSGradDate.TabIndex = 379
        Me.lblHSGradDate.Text = "Grad Date"
        '
        'Label24
        '
        Me.Label24.AutoSize = True
        Me.Label24.Location = New System.Drawing.Point(739, 193)
        Me.Label24.Name = "Label24"
        Me.Label24.Size = New System.Drawing.Size(283, 13)
        Me.Label24.TabIndex = 399
        Me.Label24.Text = "---------------------------------------------------------------------------------" & _
    "-----------"
        '
        'Label25
        '
        Me.Label25.AutoSize = True
        Me.Label25.Location = New System.Drawing.Point(739, 258)
        Me.Label25.Name = "Label25"
        Me.Label25.Size = New System.Drawing.Size(283, 13)
        Me.Label25.TabIndex = 400
        Me.Label25.Text = "---------------------------------------------------------------------------------" & _
    "-----------"
        '
        'Temp_Form
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(1028, 650)
        Me.Controls.Add(Me.Label25)
        Me.Controls.Add(Me.Label24)
        Me.Controls.Add(Me.Button4)
        Me.Controls.Add(Me.Label18)
        Me.Controls.Add(Me.lblHS_Search_Error)
        Me.Controls.Add(Me.Label33)
        Me.Controls.Add(Me.txtHS_GPA)
        Me.Controls.Add(Me.btnHS_Search)
        Me.Controls.Add(Me.txtHSCEEBCode)
        Me.Controls.Add(Me.txtHSCity)
        Me.Controls.Add(Me.Label32)
        Me.Controls.Add(Me.Label31)
        Me.Controls.Add(Me.Label30)
        Me.Controls.Add(Me.txtHS_ClassSize)
        Me.Controls.Add(Me.lblHS_ClassSize)
        Me.Controls.Add(Me.cbxHSNoSchool)
        Me.Controls.Add(Me.lblNo_School)
        Me.Controls.Add(Me.txtHSZipCode)
        Me.Controls.Add(Me.lblHS_Name)
        Me.Controls.Add(Me.cbxHSCeebName)
        Me.Controls.Add(Me.dtHSGradDate)
        Me.Controls.Add(Me.txtHS_RIC)
        Me.Controls.Add(Me.lblHS_RIC)
        Me.Controls.Add(Me.lblHSGradDate)
        Me.Controls.Add(Me.btnGet_Exception)
        Me.Controls.Add(Me.dgImages)
        Me.Controls.Add(Me.btnGet_Problem)
        Me.Controls.Add(Me.TabControl2)
        Me.Controls.Add(Me.btnGet_HS_Transcript)
        Me.Controls.Add(Me.btnFeedMe)
        Me.Name = "Temp_Form"
        Me.Text = "Temp_Form"
        CType(Me.dgImages, System.ComponentModel.ISupportInitialize).EndInit()
        Me.TabControl2.ResumeLayout(False)
        Me.TabPage3.ResumeLayout(False)
        Me.TabPage3.PerformLayout()
        Me.TabControl3.ResumeLayout(False)
        Me.TabPage1.ResumeLayout(False)
        Me.TabPage1.PerformLayout()
        Me.TabPage2.ResumeLayout(False)
        Me.TabPage2.PerformLayout()
        CType(Me.DataGridView1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox3.ResumeLayout(False)
        Me.GroupBox3.PerformLayout()
        Me.TabPage4.ResumeLayout(False)
        Me.TabControl4.ResumeLayout(False)
        Me.TabPage5.ResumeLayout(False)
        Me.TabPage5.PerformLayout()
        Me.TabPage6.ResumeLayout(False)
        Me.TabPage6.PerformLayout()
        Me.gbx_Payments.ResumeLayout(False)
        Me.gbx_Payments.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents btnGet_Exception As System.Windows.Forms.Button
    Friend WithEvents btnGet_Problem As System.Windows.Forms.Button
    Friend WithEvents dgImages As System.Windows.Forms.DataGridView
    Friend WithEvents btnGet_HS_Transcript As System.Windows.Forms.Button
    Friend WithEvents TabControl2 As System.Windows.Forms.TabControl
    Friend WithEvents TabPage3 As System.Windows.Forms.TabPage
    Friend WithEvents cbxContactCode As System.Windows.Forms.ComboBox
    Friend WithEvents Label16 As System.Windows.Forms.Label
    Friend WithEvents Label22 As System.Windows.Forms.Label
    Friend WithEvents chkbxMandatory As System.Windows.Forms.CheckBox
    Friend WithEvents cbxAdmrCode As System.Windows.Forms.ComboBox
    Friend WithEvents cbxSeq2 As System.Windows.Forms.ComboBox
    Friend WithEvents cbxSeq1 As System.Windows.Forms.ComboBox
    Friend WithEvents txtBannerID As System.Windows.Forms.TextBox
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents txtAdmrComment As System.Windows.Forms.TextBox
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents TabPage4 As System.Windows.Forms.TabPage
    Friend WithEvents btnFeedMe As System.Windows.Forms.Button
    Friend WithEvents btnSubmit As System.Windows.Forms.Button
    Friend WithEvents txtSSN3 As System.Windows.Forms.TextBox
    Friend WithEvents txtSSN2 As System.Windows.Forms.TextBox
    Friend WithEvents cbxSourceCode As System.Windows.Forms.ComboBox
    Friend WithEvents cbxGender As System.Windows.Forms.ComboBox
    Friend WithEvents dtDOB As System.Windows.Forms.DateTimePicker
    Friend WithEvents Label23 As System.Windows.Forms.Label
    Friend WithEvents Label20 As System.Windows.Forms.Label
    Friend WithEvents txtSSN1 As System.Windows.Forms.TextBox
    Friend WithEvents Label19 As System.Windows.Forms.Label
    Friend WithEvents Label11 As System.Windows.Forms.Label
    Friend WithEvents GroupBox3 As System.Windows.Forms.GroupBox
    Friend WithEvents RadioButton1 As System.Windows.Forms.RadioButton
    Friend WithEvents RadioButton2 As System.Windows.Forms.RadioButton
    Friend WithEvents TextBox1 As System.Windows.Forms.TextBox
    Friend WithEvents gbx_Payments As System.Windows.Forms.GroupBox
    Friend WithEvents cbxPayment_Department As System.Windows.Forms.ComboBox
    Friend WithEvents cbxPayment_Method As System.Windows.Forms.ComboBox
    Friend WithEvents cbxPayment_Type As System.Windows.Forms.ComboBox
    Friend WithEvents Label8 As System.Windows.Forms.Label
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents cbxAttributeCode1 As System.Windows.Forms.ComboBox
    Friend WithEvents Label15 As System.Windows.Forms.Label
    Friend WithEvents cbxInterestCode1 As System.Windows.Forms.ComboBox
    Friend WithEvents cbxInterestCode2 As System.Windows.Forms.ComboBox
    Friend WithEvents Label14 As System.Windows.Forms.Label
    Friend WithEvents DataGridView1 As System.Windows.Forms.DataGridView
    Friend WithEvents TabControl3 As System.Windows.Forms.TabControl
    Friend WithEvents TabPage1 As System.Windows.Forms.TabPage
    Friend WithEvents TabPage2 As System.Windows.Forms.TabPage
    Friend WithEvents TextBox5 As System.Windows.Forms.TextBox
    Friend WithEvents Label21 As System.Windows.Forms.Label
    Friend WithEvents Label17 As System.Windows.Forms.Label
    Friend WithEvents Button3 As System.Windows.Forms.Button
    Friend WithEvents TextBox6 As System.Windows.Forms.TextBox
    Friend WithEvents TabControl4 As System.Windows.Forms.TabControl
    Friend WithEvents TabPage5 As System.Windows.Forms.TabPage
    Friend WithEvents ComboBox1 As System.Windows.Forms.ComboBox
    Friend WithEvents Button1 As System.Windows.Forms.Button
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents TextBox2 As System.Windows.Forms.TextBox
    Friend WithEvents Label9 As System.Windows.Forms.Label
    Friend WithEvents TabPage6 As System.Windows.Forms.TabPage
    Friend WithEvents Label12 As System.Windows.Forms.Label
    Friend WithEvents TextBox3 As System.Windows.Forms.TextBox
    Friend WithEvents Label13 As System.Windows.Forms.Label
    Friend WithEvents Button2 As System.Windows.Forms.Button
    Friend WithEvents TextBox4 As System.Windows.Forms.TextBox
    Friend WithEvents TabPage8 As System.Windows.Forms.TabPage
    Friend WithEvents ComboBox2 As System.Windows.Forms.ComboBox
    Friend WithEvents Label10 As System.Windows.Forms.Label
    Friend WithEvents Button4 As System.Windows.Forms.Button
    Friend WithEvents Label18 As System.Windows.Forms.Label
    Friend WithEvents lblHS_Search_Error As System.Windows.Forms.Label
    Friend WithEvents Label33 As System.Windows.Forms.Label
    Friend WithEvents txtHS_GPA As System.Windows.Forms.TextBox
    Friend WithEvents btnHS_Search As System.Windows.Forms.Button
    Friend WithEvents txtHSCEEBCode As System.Windows.Forms.TextBox
    Friend WithEvents txtHSCity As System.Windows.Forms.TextBox
    Friend WithEvents Label32 As System.Windows.Forms.Label
    Friend WithEvents Label31 As System.Windows.Forms.Label
    Friend WithEvents Label30 As System.Windows.Forms.Label
    Friend WithEvents txtHS_ClassSize As System.Windows.Forms.TextBox
    Friend WithEvents lblHS_ClassSize As System.Windows.Forms.Label
    Friend WithEvents cbxHSNoSchool As System.Windows.Forms.ComboBox
    Friend WithEvents lblNo_School As System.Windows.Forms.Label
    Friend WithEvents txtHSZipCode As System.Windows.Forms.TextBox
    Friend WithEvents lblHS_Name As System.Windows.Forms.Label
    Friend WithEvents cbxHSCeebName As System.Windows.Forms.ComboBox
    Friend WithEvents dtHSGradDate As System.Windows.Forms.DateTimePicker
    Friend WithEvents txtHS_RIC As System.Windows.Forms.TextBox
    Friend WithEvents lblHS_RIC As System.Windows.Forms.Label
    Friend WithEvents lblHSGradDate As System.Windows.Forms.Label
    Friend WithEvents Label24 As System.Windows.Forms.Label
    Friend WithEvents Label25 As System.Windows.Forms.Label
End Class
