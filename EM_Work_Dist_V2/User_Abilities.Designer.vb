﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class User_Abilities
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim DataGridViewCellStyle1 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle2 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Me.lnkAdd = New System.Windows.Forms.LinkLabel()
        Me.lnkRemove = New System.Windows.Forms.LinkLabel()
        Me.gvProblem_Type_Not_Assigned = New System.Windows.Forms.DataGridView()
        Me.gvProblem_Type_Assigned = New System.Windows.Forms.DataGridView()
        Me.cbxUsers = New System.Windows.Forms.ComboBox()
        CType(Me.gvProblem_Type_Not_Assigned, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.gvProblem_Type_Assigned, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'lnkAdd
        '
        Me.lnkAdd.AutoSize = True
        Me.lnkAdd.Location = New System.Drawing.Point(176, 103)
        Me.lnkAdd.Name = "lnkAdd"
        Me.lnkAdd.Size = New System.Drawing.Size(38, 13)
        Me.lnkAdd.TabIndex = 0
        Me.lnkAdd.TabStop = True
        Me.lnkAdd.Text = "Add ->"
        '
        'lnkRemove
        '
        Me.lnkRemove.AutoSize = True
        Me.lnkRemove.Location = New System.Drawing.Point(158, 169)
        Me.lnkRemove.Name = "lnkRemove"
        Me.lnkRemove.Size = New System.Drawing.Size(56, 13)
        Me.lnkRemove.TabIndex = 1
        Me.lnkRemove.TabStop = True
        Me.lnkRemove.Text = "<-Remove"
        '
        'gvProblem_Type_Not_Assigned
        '
        Me.gvProblem_Type_Not_Assigned.AllowUserToAddRows = False
        Me.gvProblem_Type_Not_Assigned.AllowUserToResizeColumns = False
        Me.gvProblem_Type_Not_Assigned.AllowUserToResizeRows = False
        DataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle1.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.gvProblem_Type_Not_Assigned.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle1
        Me.gvProblem_Type_Not_Assigned.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.gvProblem_Type_Not_Assigned.ColumnHeadersVisible = False
        Me.gvProblem_Type_Not_Assigned.Location = New System.Drawing.Point(12, 52)
        Me.gvProblem_Type_Not_Assigned.Name = "gvProblem_Type_Not_Assigned"
        Me.gvProblem_Type_Not_Assigned.RowHeadersVisible = False
        Me.gvProblem_Type_Not_Assigned.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.gvProblem_Type_Not_Assigned.ShowEditingIcon = False
        Me.gvProblem_Type_Not_Assigned.Size = New System.Drawing.Size(140, 192)
        Me.gvProblem_Type_Not_Assigned.TabIndex = 2
        '
        'gvProblem_Type_Assigned
        '
        Me.gvProblem_Type_Assigned.AllowUserToAddRows = False
        Me.gvProblem_Type_Assigned.AllowUserToResizeColumns = False
        Me.gvProblem_Type_Assigned.AllowUserToResizeRows = False
        DataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle2.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.gvProblem_Type_Assigned.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle2
        Me.gvProblem_Type_Assigned.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.gvProblem_Type_Assigned.ColumnHeadersVisible = False
        Me.gvProblem_Type_Assigned.Location = New System.Drawing.Point(237, 52)
        Me.gvProblem_Type_Assigned.Name = "gvProblem_Type_Assigned"
        Me.gvProblem_Type_Assigned.RowHeadersVisible = False
        Me.gvProblem_Type_Assigned.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.gvProblem_Type_Assigned.ShowEditingIcon = False
        Me.gvProblem_Type_Assigned.Size = New System.Drawing.Size(142, 192)
        Me.gvProblem_Type_Assigned.TabIndex = 3
        '
        'cbxUsers
        '
        Me.cbxUsers.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cbxUsers.FormattingEnabled = True
        Me.cbxUsers.Location = New System.Drawing.Point(13, 13)
        Me.cbxUsers.Name = "cbxUsers"
        Me.cbxUsers.Size = New System.Drawing.Size(121, 21)
        Me.cbxUsers.TabIndex = 4
        '
        'User_Abilities
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(405, 333)
        Me.Controls.Add(Me.cbxUsers)
        Me.Controls.Add(Me.gvProblem_Type_Assigned)
        Me.Controls.Add(Me.gvProblem_Type_Not_Assigned)
        Me.Controls.Add(Me.lnkRemove)
        Me.Controls.Add(Me.lnkAdd)
        Me.Name = "User_Abilities"
        Me.Text = "User_Abilities"
        CType(Me.gvProblem_Type_Not_Assigned, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.gvProblem_Type_Assigned, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents lnkAdd As System.Windows.Forms.LinkLabel
    Friend WithEvents lnkRemove As System.Windows.Forms.LinkLabel
    Friend WithEvents gvProblem_Type_Not_Assigned As System.Windows.Forms.DataGridView
    Friend WithEvents gvProblem_Type_Assigned As System.Windows.Forms.DataGridView
    Friend WithEvents cbxUsers As System.Windows.Forms.ComboBox
End Class
