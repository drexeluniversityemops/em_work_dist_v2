﻿Friend Class vpair
    Public myText As String
    Public myValue As String

    Public Sub New(ByVal strText As String, ByVal intValue As String)
        Me.myText = strText
        Me.myValue = intValue
    End Sub

    Public ReadOnly Property Text() As String
        Get
            Return myText
        End Get
    End Property

    Public ReadOnly Property Value() As String
        Get
            Return myValue
        End Get
    End Property


    Public Overrides Function toString() As String
        Return Me.Value & " - " & Me.Text
    End Function

End Class