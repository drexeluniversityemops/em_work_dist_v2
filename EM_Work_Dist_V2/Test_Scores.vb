﻿Imports System.IO
Imports System.Drawing.Imaging
Imports System.Threading
Imports System.Threading.Thread
Imports System.Text.RegularExpressions
Imports System.Reflection
Imports System.Security.Principal
Imports System.Collections
Imports System.Data
Imports System.Data.SqlClient
Imports System.Data.SqlTypes
Imports System
Imports System.Collections.Generic
Imports System.ComponentModel
Imports System.Drawing
Imports System.Linq
Imports System.Text
Imports System.Windows.Forms
Imports System.Runtime.Remoting.Messaging
Namespace DataProcessing
    Public Class Test_Scores

        Private Image_ID As Integer
        Private Banner_ID As String = ""
        Private EM_WD_Users_ID As Integer

        Public Sub New(ByVal id As Integer, ByVal para_Banner_ID As String, ByVal User_ID As Integer)
            MyBase.New()
            InitializeComponent()
            Image_ID = id
            Banner_ID = para_Banner_ID
            EM_WD_Users_ID = User_ID
        End Sub

        Public Sub Form_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

            ' dtTest_Date.Value = System.DateTime.Now
            dtTest_Date.Value = DateTime.Now.AddDays(-1) '//ds 9/16/14

            'Hide all group boxes
            grbx_SAT.Visible = False
            grbx_ACT.Visible = False
            grbx_IELTS.Visible = False

            ErrorProvider_Tests.ContainerControl = Me

            'Set the Image ID for the image being passed
            lblImage_ID.Text = Image_ID
            txtBannerID.Text = Banner_ID

            Load_Test_Scores()
        End Sub

        Private Sub Test_Scores_FormClosing(sender As Object, e As FormClosingEventArgs) Handles MyBase.FormClosing
            'Set the variable to nothing so the form can be opened again.
            DP_WDP.Test_Score_Form = Nothing
        End Sub

        Private Sub cbxTest_Type_SelectedIndexChanged(sender As System.Object, e As System.EventArgs) Handles cbxTest_Type.SelectedIndexChanged

            'Hide all group boxes
            grbx_SAT.Visible = False
            grbx_ACT.Visible = False
            grbx_IELTS.Visible = False

            If cbxTest_Type.SelectedIndex <> 0 Then
                Select Case cbxTest_Type.SelectedItem
                    Case "SAT"
                        grbx_SAT.Visible = True
                        grbx_SAT.Location = New Point(32, 124)
                        ErrorProvider_Tests.SetError(cbxTest_Type, "")
                    Case "ACT"
                        grbx_ACT.Visible = True
                        grbx_ACT.Location = New Point(32, 124)
                        ErrorProvider_Tests.SetError(cbxTest_Type, "")
                    Case "IELTS"
                        grbx_IELTS.Visible = True
                        grbx_IELTS.Location = New Point(32, 124)
                        ErrorProvider_Tests.SetError(cbxTest_Type, "")
                End Select
            End If
        End Sub

        Private Sub cbxTest_Source_SelectedIndexChanged(sender As System.Object, e As System.EventArgs) Handles cbxTest_Source.SelectedIndexChanged
            If cbxTest_Source.SelectedIndex > 0 Then
                ErrorProvider_Tests.SetError(cbxTest_Source, "")
            Else
                ErrorProvider_Tests.SetError(cbxTest_Source, "Please select a source.")
            End If
        End Sub

        Private Sub txtBannerID_Validating(ByVal sender As Object, ByVal e As System.ComponentModel.CancelEventArgs) Handles txtBannerID.Validating
            Validate_BannerID()
        End Sub

        Private Sub txtSAT_Validating(ByVal sender As Object, ByVal e As System.ComponentModel.CancelEventArgs) Handles txtSAT_S01.Validating, txtSAT_S07.Validating, txtSAT_S02.Validating, txtSAT_S09.Validating, txtSAT_S08.Validating
            ValidateSAT(sender.Text, sender)
        End Sub

        Private Sub txtACT_Validating(ByVal sender As Object, ByVal e As System.ComponentModel.CancelEventArgs) Handles txtACT_A07.Validating, txtACT_A05.Validating, txtACT_A04.Validating, txtACT_A03.Validating, txtACT_A02.Validating, txtACT_A01.Validating
            ValidateACT(sender.Text, sender)
        End Sub

        Private Sub txtIELTS_Validating(ByVal sender As Object, ByVal e As System.ComponentModel.CancelEventArgs) Handles txtIELTS_IEW.Validating, txtIELTS_IES.Validating, txtIELTS_IER.Validating, txtIELTS_IEOB.Validating, txtIELTS_IEL.Validating
            ValidateIELTS(sender.Text, sender)
        End Sub

        Public Function ValidateSAT(ByVal Score As String, ByVal From_Field As Object) As Boolean
            Dim bStatus As Boolean = True
            If IsNumeric(Trim(Score)) Then
                Dim Score_Value As Integer = Trim(Score)
                If From_Field.Name = "txtSAT_S08" Then
                    Select Case Score_Value
                        Case Is < 0
                            ErrorProvider_Tests.SetError(From_Field, "Please enter a valid score")
                            bStatus = False
                        Case Is > 12
                            ErrorProvider_Tests.SetError(From_Field, "Please enter a valid score")
                            bStatus = False
                        Case Else
                            ErrorProvider_Tests.SetError(From_Field, "")
                    End Select
                ElseIf From_Field.Name = "txtSAT_S09" Then
                    Select Case Score_Value
                        Case Is < 20
                            ErrorProvider_Tests.SetError(From_Field, "Please enter a valid score")
                            bStatus = False
                        Case Is > 80
                            ErrorProvider_Tests.SetError(From_Field, "Please enter a valid score")
                            bStatus = False
                        Case Else
                            ErrorProvider_Tests.SetError(From_Field, "")
                    End Select
                ElseIf From_Field.Name = "txtSAT_S01" Then
                    Select Case Score_Value
                        Case Is < 200 '//ds 8/28/14
                            ErrorProvider_Tests.SetError(From_Field, "Please enter a valid score")
                            bStatus = False
                        Case Is > 800
                            ErrorProvider_Tests.SetError(From_Field, "Please enter a valid score")
                            bStatus = False
                        Case Else
                            'SATs must end in a 0
                            Select Case Microsoft.VisualBasic.Right(Score_Value, 1)
                                Case Is > 0
                                    ErrorProvider_Tests.SetError(From_Field, "Please enter a valid score")
                                    bStatus = False
                                Case Else
                                    ErrorProvider_Tests.SetError(From_Field, "")
                            End Select
                    End Select
                Else
                    Select Case Score_Value
                        Case Is < 200 '1  '//ds 9/16/14 - changed to 200 as instructed by krista
                            ErrorProvider_Tests.SetError(From_Field, "Please enter a valid score")
                            bStatus = False
                        Case Is > 800
                            ErrorProvider_Tests.SetError(From_Field, "Please enter a valid score")
                            bStatus = False
                        Case Else
                            'SATs must end in a 0
                            Select Case Microsoft.VisualBasic.Right(Score_Value, 1)
                                Case Is > 0
                                    ErrorProvider_Tests.SetError(From_Field, "Please enter a valid score")
                                    bStatus = False
                                Case Else
                                    ErrorProvider_Tests.SetError(From_Field, "")
                            End Select
                    End Select
                End If
            ElseIf From_Field.Name = "txtSAT_S07" Then
                'S07 is required for test dates 3/12/05 and forward
                Dim Test_Date As DateTime = dtTest_Date.Text
                If Test_Date > "3/11/05" Then
                    ErrorProvider_Tests.SetError(From_Field, "Please enter a valid score")
                    bStatus = False
                Else
                    If Score <> "" Then
                        ErrorProvider_Tests.SetError(From_Field, "Please enter a valid score")
                        bStatus = False
                    Else
                        ErrorProvider_Tests.SetError(From_Field, "")
                    End If
                End If
            ElseIf From_Field.Name = "txtSAT_S08" Then
                If Score <> "" Then
                    ErrorProvider_Tests.SetError(From_Field, "Please enter a valid score")
                    bStatus = False
                Else
                    ErrorProvider_Tests.SetError(From_Field, "")
                End If
            ElseIf From_Field.Name = "txtSAT_S09" Then
                If Score <> "" Then
                    ErrorProvider_Tests.SetError(From_Field, "Please enter a valid score")
                    bStatus = False
                Else
                    ErrorProvider_Tests.SetError(From_Field, "")
                End If
            Else
                    ErrorProvider_Tests.SetError(From_Field, "Please enter a valid score")
                    bStatus = False
            End If
            Return bStatus
        End Function

        Public Function ValidateACT(ByVal Score As String, ByVal From_Field As Object) As Boolean
            Dim bStatus As Boolean = True

            If Score.Contains(".") Then
                ErrorProvider_Tests.SetError(From_Field, "Please enter a valid score (integer only)")
                bStatus = False
            Else
                If IsNumeric(Trim(Score)) Then
                    Dim Score_Value As Integer = Trim(Score)
                    Select Case Score_Value
                        Case Is < 1
                            ErrorProvider_Tests.SetError(From_Field, "Please enter a valid score")
                            bStatus = False
                        Case Is > 36
                            ErrorProvider_Tests.SetError(From_Field, "Please enter a valid score")
                            bStatus = False
                        Case Else
                            ErrorProvider_Tests.SetError(From_Field, "")
                    End Select
                    'ACT07 is not required, but make sure if something is entered it is valid.
                ElseIf From_Field.Name = "txtACT_A07" Then
                    If Score <> "" Then
                        ErrorProvider_Tests.SetError(From_Field, "Please enter a valid score")
                        bStatus = False
                    Else
                        ErrorProvider_Tests.SetError(From_Field, "")
                    End If
                Else
                    ErrorProvider_Tests.SetError(From_Field, "Please enter a valid score")
                    bStatus = False
                End If
            End If

            Return bStatus
        End Function

        Public Function ValidateIELTS(ByVal Score As String, ByVal From_Field As Object) As Boolean
            Dim bStatus As Boolean = True
            If Score.Length < 3 Then '//ds 8/28/14
                ErrorProvider_Tests.SetError(From_Field, "Please enter a valid score (x.x format)")
                bStatus = False
            Else
                If IsNumeric(Trim(Score)) Then
                    Dim Score_Value As Decimal = FormatNumber(Trim(Score), 1)
                    Select Case Score_Value
                        'Case Is < 1  '//ds 8/28/14
                        Case 0 To 0.4
                            ErrorProvider_Tests.SetError(From_Field, "Please enter a valid score")
                            bStatus = False
                        Case 0.6 To 0.9
                            ErrorProvider_Tests.SetError(From_Field, "Please enter a valid score")
                            bStatus = False
                        Case Is > 9
                            ErrorProvider_Tests.SetError(From_Field, "Please enter a valid score")
                            bStatus = False
                        Case Else
                            'Scores must end in a 0 or 5
                            Select Case Microsoft.VisualBasic.Right(Score_Value, 1)
                                Case Is = 0, 5
                                    ErrorProvider_Tests.SetError(From_Field, "")
                                Case Else
                                    ErrorProvider_Tests.SetError(From_Field, "Please enter a valid score")
                            End Select
                    End Select
                Else
                    ErrorProvider_Tests.SetError(From_Field, "Please enter a valid score")
                    bStatus = False
                End If
            End If

            Return bStatus
        End Function

        Public Function Validate_BannerID() As Boolean
            Dim bStatus As Boolean = True

            ErrorProvider_Tests.SetError(txtBannerID, "Please enter a Banner ID")
            If IsNumeric(Trim(txtBannerID.Text)) Then
                If Microsoft.VisualBasic.Left(Trim(txtBannerID.Text), 1) = 0 Then
                    ErrorProvider_Tests.SetError(txtBannerID, "Please enter a valid Banner ID")
                    bStatus = False
                Else
                    Dim BannerID As Integer = Trim(txtBannerID.Text)
                    Select Case BannerID
                        Case Is < 10000000
                            ErrorProvider_Tests.SetError(txtBannerID, "Please enter a valid Banner ID")
                            bStatus = False
                        Case Is > 69999999
                            ErrorProvider_Tests.SetError(txtBannerID, "Please enter a valid Banner ID")
                            bStatus = False
                        Case Else
                            ErrorProvider_Tests.SetError(txtBannerID, "")
                    End Select
                End If

            Else
                ErrorProvider_Tests.SetError(txtBannerID, "Please enter a Banner ID")
                bStatus = False
            End If
            Return bStatus
        End Function

        Private Sub btnClear_Click(sender As System.Object, e As System.EventArgs) Handles btnClear.Click
            ResetAllControls(Me)
        End Sub

        Public Sub ResetAllControls(ByVal form As Control)
            If form.Controls.Count > 0 Then
                For Each ctrl As Control In form.Controls
                    'Do not reset the Banner ID field until feedme is pressed
                    If Not ctrl.Name = "txtBannerID" Then
                        ResetAllControls(ctrl)
                    End If
                Next
            End If

            If TypeOf form Is TextBox Then
                Dim textBox As TextBox = DirectCast(form, TextBox)
                textBox.Text = Nothing
            End If

            If TypeOf form Is ComboBox Then
                Dim comboBox As ComboBox = DirectCast(form, ComboBox)
                If comboBox.Items.Count > 0 Then
                    comboBox.SelectedIndex = 0
                End If
            End If

            If TypeOf form Is ListBox Then
                Dim listBox As ListBox = DirectCast(form, ListBox)
                listBox.ClearSelected()
            End If

            If TypeOf form Is DateTimePicker Then
                Dim DateTimePicker As DateTimePicker = DirectCast(form, DateTimePicker)
                DateTimePicker.Value = System.DateTime.Now
            End If
        End Sub

        Private Sub btnSubmit_Click(sender As System.Object, e As System.EventArgs) Handles btnSubmit.Click

            '//ds 9/16/14
            Dim result As Integer = DateTime.Compare(Convert.ToDateTime(dtTest_Date.Value.ToShortDateString), Convert.ToDateTime(DateTime.Now.ToShortDateString))
            If (result >= 0) Then
                'dtTest_Date.Value = DateTime.Now.AddDays(-1)
                MessageBox.Show("Please pick a date prior to today's date!")
                Exit Sub
            End If

            If ValidateForm() Then
                Dim TA_Submit_Test_Scores As New Submit_Test_ScoresTableAdapters.QueriesTableAdapter

                '//ds 9/8/14 - added try/catch around code
                Try


                    Select Case cbxTest_Type.SelectedItem
                        Case "SAT"
                            For i As Integer = grbx_SAT.Controls.Count - 1 To 0 Step -1
                                Dim ctrl As Control = grbx_SAT.Controls(i)
                                If ctrl.GetType Is GetType(TextBox) Then
                                    Dim Test_Code As String = Microsoft.VisualBasic.Right(ctrl.Name, Len(ctrl.Name) - InStr(ctrl.Name, "_"))
                                    Dim Test_Value As String = ctrl.Text
                                    Dim txt_Banner_ID As Integer = txtBannerID.Text
                                    Dim Test_Date As DateTime = dtTest_Date.Text
                                    Dim Test_Source As String = cbxTest_Source.SelectedItem

                                    'MessageBox.Show("Image ID: " & Image_ID & Environment.NewLine & _
                                    '    "Banner ID: " & Banner_ID & Environment.NewLine & _
                                    '    "Test Date: " & Test_Date & Environment.NewLine & _
                                    '    "Test Source: " & Test_Source & Environment.NewLine & _
                                    '    "Test Code: " & Test_Code & Environment.NewLine & _
                                    '    "Test Value: " & Test_Value & Environment.NewLine)

                                    'Only load if their is a value to load
                                    If Test_Value <> "" Then
                                        'Load test scores into database, one row at a time
                                        TA_Submit_Test_Scores.EM_WD_Submit_Test_Scores_ins(Image_ID, _
                                                 Test_Code, _
                                                 Banner_ID, _
                                                 Test_Value, _
                                                 Test_Date, _
                                                 Test_Source, _
                                                 EM_WD_Users_ID)
                                    End If
                                End If
                            Next
                        Case "ACT"

                            For i As Integer = grbx_ACT.Controls.Count - 1 To 0 Step -1
                                Dim ctrl As Control = grbx_ACT.Controls(i)
                                If ctrl.GetType Is GetType(TextBox) Then
                                    Dim Test_Code As String = Microsoft.VisualBasic.Right(ctrl.Name, Len(ctrl.Name) - InStr(ctrl.Name, "_"))
                                    Dim Test_Value As String = ctrl.Text
                                    Dim Banner_ID As Integer = txtBannerID.Text
                                    Dim Test_Date As DateTime = dtTest_Date.Text
                                    Dim Test_Source As String = cbxTest_Source.SelectedItem

                                    'MessageBox.Show("Image ID: " & Image_ID & Environment.NewLine & _
                                    '    "Banner ID: " & Banner_ID & Environment.NewLine & _
                                    '    "Test Date: " & Test_Date & Environment.NewLine & _
                                    '    "Test Source: " & Test_Source & Environment.NewLine & _
                                    '    "Test Code: " & Test_Code & Environment.NewLine & _
                                    '    "Test Value: " & Test_Value & Environment.NewLine)

                                    'Only load if their is a value to load
                                    If Test_Value <> "" Then
                                        'Load test scores into database, one row at a time
                                        TA_Submit_Test_Scores.EM_WD_Submit_Test_Scores_ins(Image_ID, _
                                                 Test_Code, _
                                                 Banner_ID, _
                                                 Test_Value, _
                                                 Test_Date, _
                                                 Test_Source, _
                                                 EM_WD_Users_ID)
                                    End If

                                End If
                            Next
                        Case "IELTS"
                            For i As Integer = grbx_IELTS.Controls.Count - 1 To 0 Step -1
                                Dim ctrl As Control = grbx_IELTS.Controls(i)
                                If ctrl.GetType Is GetType(TextBox) Then
                                    Dim Test_Code As String = Microsoft.VisualBasic.Right(ctrl.Name, Len(ctrl.Name) - InStr(ctrl.Name, "_"))
                                    Dim Test_Value As String = ctrl.Text
                                    Dim Banner_ID As Integer = txtBannerID.Text
                                    Dim Test_Date As DateTime = dtTest_Date.Text
                                    Dim Test_Source As String = cbxTest_Source.SelectedItem

                                    'MessageBox.Show("Image ID: " & Image_ID & Environment.NewLine & _
                                    '    "Banner ID: " & Banner_ID & Environment.NewLine & _
                                    '    "Test Date: " & Test_Date & Environment.NewLine & _
                                    '    "Test Source: " & Test_Source & Environment.NewLine & _
                                    '    "Test Code: " & Test_Code & Environment.NewLine & _
                                    '    "Test Value: " & Test_Value & Environment.NewLine)

                                    'Only load if their is a value to load
                                    If Test_Value <> "" Then
                                        'Load test scores into database, one row at a time
                                        TA_Submit_Test_Scores.EM_WD_Submit_Test_Scores_ins(Image_ID, _
                                                 Test_Code, _
                                                 Banner_ID, _
                                                 Test_Value, _
                                                 Test_Date, _
                                                 Test_Source, _
                                                 EM_WD_Users_ID)
                                    End If
                                End If
                            Next
                    End Select

                    MessageBox.Show("The data was successfully written to the database.") '//ds 9/8/14

                Catch ex As Exception
                    MessageBox.Show("Error! The data was not written to the database.") '//ds 9/8/14
                End Try

                'Show the loaded scores
                Load_Test_Scores()

                'After load into database, clear the form
                ResetAllControls(Me)
            End If
        End Sub

        Private Function ValidateForm() As Boolean
            Dim bStatus As Boolean = True

            bStatus = Validate_BannerID()

            Select Case cbxTest_Type.SelectedItem
                Case "SAT"
                    For i As Integer = grbx_SAT.Controls.Count - 1 To 0 Step -1
                        Dim ctrl As Control = grbx_SAT.Controls(i)
                        If ctrl.GetType Is GetType(TextBox) Then
                            If ValidateSAT(ctrl.Text, ctrl) = False Then
                                bStatus = False
                            End If
                        End If
                    Next
                Case "ACT"
                    For i As Integer = grbx_ACT.Controls.Count - 1 To 0 Step -1
                        Dim ctrl As Control = grbx_ACT.Controls(i)
                        If ctrl.GetType Is GetType(TextBox) Then
                            If ValidateACT(ctrl.Text, ctrl) = False Then
                                bStatus = False
                            End If
                        End If
                    Next
                Case "IELTS"
                    For i As Integer = grbx_IELTS.Controls.Count - 1 To 0 Step -1
                        Dim ctrl As Control = grbx_IELTS.Controls(i)
                        If ctrl.GetType Is GetType(TextBox) Then
                            If ValidateIELTS(ctrl.Text, ctrl) = False Then
                                bStatus = False
                            End If
                        End If
                    Next
                Case Else
                    ErrorProvider_Tests.SetError(cbxTest_Type, "Please select a score type.")
                    bStatus = False
            End Select

            If cbxTest_Source.SelectedIndex < 1 Then
                ErrorProvider_Tests.SetError(cbxTest_Source, "Please select a source.")
                bStatus = False
            End If

            Return bStatus
        End Function

        Private Sub txtBannerID_Leave(sender As Object, e As System.EventArgs) Handles txtBannerID.Leave
            Banner_ID = txtBannerID.Text
            Load_Test_Scores()
        End Sub

        Private Sub Load_Test_Scores()
            Try
                dgTest_Scores.Columns.Clear()

                Dim TA_Test_Scores As New dsTest_ScoresTableAdapters.EM_WD_GetImage_Test_ScoresTableAdapter
                Dim dtTest_Scores As dsTest_Scores.EM_WD_GetImage_Test_ScoresDataTable
                dtTest_Scores = TA_Test_Scores.GetData_Test_Scores(Banner_ID)

                If dtTest_Scores.Rows.Count > 0 Then
                    'There are images in the queue to be processed for this user
                    dgTest_Scores.Visible = True
                    dgTest_Scores.DataSource = dtTest_Scores
                    dgTest_Scores.Columns(0).Width = 50 ' Code
                    dgTest_Scores.Columns(1).Width = 50 ' Score
                    dgTest_Scores.Columns(2).Width = 80 ' Test_Date
                    dgTest_Scores.Columns(3).Width = 50 ' Source
                    dgTest_Scores.Columns(4).Width = 110 ' Processed_Date
                    dgTest_Scores.Columns(5).Width = 125 ' Image_Test_Score_ID
                    dgTest_Scores.Columns(6).Width = 60 ' Image_ID
                Else
                    dgTest_Scores.Visible = False
                End If
            Catch ex As Exception
                MessageBox.Show(ex.ToString)
            End Try
        End Sub

        Private Sub btnExit_Click(sender As System.Object, e As System.EventArgs) Handles btnExit.Click
            Me.Close()
        End Sub

        Private Sub dtTest_Date_ValueChanged(sender As System.Object, e As System.EventArgs) Handles dtTest_Date.ValueChanged
            '//ds 9/16/14
            ' Dim result As Integer = DateTime.Compare(dtTest_Date.Value, DateTime.Now)
            'Dim result As Integer = DateTime.Compare(Convert.ToDateTime(dtTest_Date.Value.ToShortDateString), Convert.ToDateTime(DateTime.Now.ToShortDateString))
            'If (result >= 0) Then
            '    'dtTest_Date.Value = DateTime.Now.AddDays(-1)
            '    MessageBox.Show("Please pick a date prior to today's date.")
            '    Exit Sub
            'End If

        End Sub
    End Class
End Namespace
