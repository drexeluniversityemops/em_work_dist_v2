﻿Public Class User_Abilities
    Dim EM_WD_Interface_ID As Integer = 2

    Private Sub Form1_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Load_Not_Assigned()
        Load_Assigned()
        Load_Users()
    End Sub
    Private Sub lnkAdd_LinkClicked(sender As System.Object, e As System.Windows.Forms.LinkLabelLinkClickedEventArgs) Handles lnkAdd.LinkClicked
        Dim dgvRow As DataGridViewRow
        For Each dgvRow In gvProblem_Type_Not_Assigned.SelectedRows
            Dim TA_Submit_Problem_Type_Add As New dsProblem_Types_UsersTableAdapters.QueriesTableAdapter
            TA_Submit_Problem_Type_Add.EM_WD_AssignedProblemType_ins(dgvRow.Cells("Image_Problem_Type_ID").Value, _
                                                           cbxUsers.SelectedValue, _
                                                           EM_WD_Interface_ID)
        Next
        'Reload the DataViews
        Load_Assigned()
        Load_Not_Assigned()
    End Sub

    Private Sub lnkRemove_LinkClicked(sender As System.Object, e As System.Windows.Forms.LinkLabelLinkClickedEventArgs) Handles lnkRemove.LinkClicked

        Dim dgvRow As DataGridViewRow
        For Each dgvRow In gvProblem_Type_Assigned.SelectedRows
            Dim TA_Submit_Problem_Type_Remove As New dsProblem_Types_UsersTableAdapters.QueriesTableAdapter
            TA_Submit_Problem_Type_Remove.EM_WD_AssignedProblemType_del(dgvRow.Cells("Image_Problem_Type_ID").Value, _
                                                           cbxUsers.SelectedValue, _
                                                           EM_WD_Interface_ID)
        Next
        'Reload the DataViews
        Load_Assigned()
        Load_Not_Assigned()
    End Sub

    Sub Load_Assigned()

        gvProblem_Type_Assigned.Columns.Clear()

        Dim TA_Problem_Type_Assigned As New dsProblem_Types_UsersTableAdapters.EM_WD_GetProblemTypeAssignedToUserTableAdapter
        Dim dtProblem_Type_Assigned As dsProblem_Types_Users.EM_WD_GetProblemTypeAssignedToUserDataTable
        dtProblem_Type_Assigned = TA_Problem_Type_Assigned.GetData_Problem_Type_Assigned(cbxUsers.SelectedValue, EM_WD_Interface_ID)

        gvProblem_Type_Assigned.DataSource = dtProblem_Type_Assigned
        gvProblem_Type_Assigned.Columns("Image_Problem_Type_ID").Visible = False

    End Sub

    Sub Load_Not_Assigned()

        gvProblem_Type_Not_Assigned.Columns.Clear()

        Dim TA_Problem_Type_Not_Assigned As New dsProblem_Types_UsersTableAdapters.EM_WD_GetProblemTypeNotAssignedToUserTableAdapter
        Dim dtProblem_Type_Not_Assigned As dsProblem_Types_Users.EM_WD_GetProblemTypeNotAssignedToUserDataTable
        dtProblem_Type_Not_Assigned = TA_Problem_Type_Not_Assigned.GetData_Problem_Type_Not_Assigned(cbxUsers.SelectedValue, EM_WD_Interface_ID)

        gvProblem_Type_Not_Assigned.DataSource = dtProblem_Type_Not_Assigned
        gvProblem_Type_Not_Assigned.Columns("Image_Problem_Type_ID").Visible = False

    End Sub
    Sub Load_Users()

        Dim TA_Users As New dsUser_IDTableAdapters.EM_WD_GetUsersTableAdapter
        Dim dtUsers As dsUser_ID.EM_WD_GetUsersDataTable

        dtUsers = TA_Users.GetData_Users

        'Users
        cbxUsers.DataSource = dtUsers
        cbxUsers.DisplayMember = "EM_WD_Users_Username"
        cbxUsers.ValueMember = "EM_WD_Users_ID"
    End Sub
    Private Sub cbxUsers_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cbxUsers.SelectionChangeCommitted
        'Reload the DataViews
        Load_Assigned()
        Load_Not_Assigned()
    End Sub
End Class