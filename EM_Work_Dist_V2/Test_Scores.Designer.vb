﻿Namespace DataProcessing

    <Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
    Partial Class Test_Scores
        Inherits System.Windows.Forms.Form

        'Form overrides dispose to clean up the component list.
        <System.Diagnostics.DebuggerNonUserCode()> _
        Protected Overrides Sub Dispose(ByVal disposing As Boolean)
            Try
                If disposing AndAlso components IsNot Nothing Then
                    components.Dispose()
                End If
            Finally
                MyBase.Dispose(disposing)
            End Try
        End Sub

        'Required by the Windows Form Designer
        Private components As System.ComponentModel.IContainer

        'NOTE: The following procedure is required by the Windows Form Designer
        'It can be modified using the Windows Form Designer.  
        'Do not modify it using the code editor.
        <System.Diagnostics.DebuggerStepThrough()> _
        Private Sub InitializeComponent()
            Me.components = New System.ComponentModel.Container()
            Me.dtTest_Date = New System.Windows.Forms.DateTimePicker()
            Me.Label1 = New System.Windows.Forms.Label()
            Me.cbxTest_Type = New System.Windows.Forms.ComboBox()
            Me.Label2 = New System.Windows.Forms.Label()
            Me.cbxTest_Source = New System.Windows.Forms.ComboBox()
            Me.Label3 = New System.Windows.Forms.Label()
            Me.Label4 = New System.Windows.Forms.Label()
            Me.Label5 = New System.Windows.Forms.Label()
            Me.txtBannerID = New System.Windows.Forms.TextBox()
            Me.grbx_SAT = New System.Windows.Forms.GroupBox()
            Me.txtSAT_S09 = New System.Windows.Forms.TextBox()
            Me.txtSAT_S08 = New System.Windows.Forms.TextBox()
            Me.Label15 = New System.Windows.Forms.Label()
            Me.Label16 = New System.Windows.Forms.Label()
            Me.txtSAT_S07 = New System.Windows.Forms.TextBox()
            Me.txtSAT_S02 = New System.Windows.Forms.TextBox()
            Me.txtSAT_S01 = New System.Windows.Forms.TextBox()
            Me.Label8 = New System.Windows.Forms.Label()
            Me.Label7 = New System.Windows.Forms.Label()
            Me.Label6 = New System.Windows.Forms.Label()
            Me.grbx_ACT = New System.Windows.Forms.GroupBox()
            Me.txtACT_A07 = New System.Windows.Forms.TextBox()
            Me.txtACT_A05 = New System.Windows.Forms.TextBox()
            Me.txtACT_A04 = New System.Windows.Forms.TextBox()
            Me.Label12 = New System.Windows.Forms.Label()
            Me.Label13 = New System.Windows.Forms.Label()
            Me.Label14 = New System.Windows.Forms.Label()
            Me.txtACT_A03 = New System.Windows.Forms.TextBox()
            Me.txtACT_A02 = New System.Windows.Forms.TextBox()
            Me.txtACT_A01 = New System.Windows.Forms.TextBox()
            Me.Label9 = New System.Windows.Forms.Label()
            Me.Label10 = New System.Windows.Forms.Label()
            Me.Label11 = New System.Windows.Forms.Label()
            Me.grbx_IELTS = New System.Windows.Forms.GroupBox()
            Me.txtIELTS_IEW = New System.Windows.Forms.TextBox()
            Me.txtIELTS_IES = New System.Windows.Forms.TextBox()
            Me.Label31 = New System.Windows.Forms.Label()
            Me.Label32 = New System.Windows.Forms.Label()
            Me.txtIELTS_IER = New System.Windows.Forms.TextBox()
            Me.txtIELTS_IEOB = New System.Windows.Forms.TextBox()
            Me.txtIELTS_IEL = New System.Windows.Forms.TextBox()
            Me.Label33 = New System.Windows.Forms.Label()
            Me.Label34 = New System.Windows.Forms.Label()
            Me.Label35 = New System.Windows.Forms.Label()
            Me.btnSubmit = New System.Windows.Forms.Button()
            Me.btnClear = New System.Windows.Forms.Button()
            Me.lblImage_ID = New System.Windows.Forms.Label()
            Me.dgTest_Scores = New System.Windows.Forms.DataGridView()
            Me.ErrorProvider_Tests = New System.Windows.Forms.ErrorProvider(Me.components)
            Me.btnExit = New System.Windows.Forms.Button()
            Me.grbx_SAT.SuspendLayout()
            Me.grbx_ACT.SuspendLayout()
            Me.grbx_IELTS.SuspendLayout()
            CType(Me.dgTest_Scores, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.ErrorProvider_Tests, System.ComponentModel.ISupportInitialize).BeginInit()
            Me.SuspendLayout()
            '
            'dtTest_Date
            '
            Me.dtTest_Date.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
            Me.dtTest_Date.Location = New System.Drawing.Point(339, 91)
            Me.dtTest_Date.Name = "dtTest_Date"
            Me.dtTest_Date.Size = New System.Drawing.Size(100, 20)
            Me.dtTest_Date.TabIndex = 3
            Me.dtTest_Date.TabStop = False
            '
            'Label1
            '
            Me.Label1.AutoSize = True
            Me.Label1.Location = New System.Drawing.Point(276, 97)
            Me.Label1.Name = "Label1"
            Me.Label1.Size = New System.Drawing.Size(57, 13)
            Me.Label1.TabIndex = 33
            Me.Label1.Text = "Test Date:"
            '
            'cbxTest_Type
            '
            Me.cbxTest_Type.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
            Me.cbxTest_Type.FormattingEnabled = True
            Me.cbxTest_Type.Items.AddRange(New Object() {"", "SAT", "ACT", "IELTS"})
            Me.cbxTest_Type.Location = New System.Drawing.Point(81, 94)
            Me.cbxTest_Type.Name = "cbxTest_Type"
            Me.cbxTest_Type.Size = New System.Drawing.Size(139, 21)
            Me.cbxTest_Type.TabIndex = 2
            '
            'Label2
            '
            Me.Label2.AutoSize = True
            Me.Label2.Location = New System.Drawing.Point(18, 97)
            Me.Label2.Name = "Label2"
            Me.Label2.Size = New System.Drawing.Size(58, 13)
            Me.Label2.TabIndex = 35
            Me.Label2.Text = "Test Type:"
            '
            'cbxTest_Source
            '
            Me.cbxTest_Source.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
            Me.cbxTest_Source.FormattingEnabled = True
            Me.cbxTest_Source.Items.AddRange(New Object() {"", "TRNS", "TEST"})
            Me.cbxTest_Source.Location = New System.Drawing.Point(562, 94)
            Me.cbxTest_Source.Name = "cbxTest_Source"
            Me.cbxTest_Source.Size = New System.Drawing.Size(121, 21)
            Me.cbxTest_Source.TabIndex = 4
            '
            'Label3
            '
            Me.Label3.AutoSize = True
            Me.Label3.Location = New System.Drawing.Point(488, 97)
            Me.Label3.Name = "Label3"
            Me.Label3.Size = New System.Drawing.Size(68, 13)
            Me.Label3.TabIndex = 37
            Me.Label3.Text = "Test Source:"
            '
            'Label4
            '
            Me.Label4.AutoSize = True
            Me.Label4.Location = New System.Drawing.Point(18, 36)
            Me.Label4.Name = "Label4"
            Me.Label4.Size = New System.Drawing.Size(53, 13)
            Me.Label4.TabIndex = 38
            Me.Label4.Text = "Image ID:"
            '
            'Label5
            '
            Me.Label5.AutoSize = True
            Me.Label5.Location = New System.Drawing.Point(18, 66)
            Me.Label5.Name = "Label5"
            Me.Label5.Size = New System.Drawing.Size(58, 13)
            Me.Label5.TabIndex = 39
            Me.Label5.Text = "Banner ID:"
            '
            'txtBannerID
            '
            Me.txtBannerID.Location = New System.Drawing.Point(81, 63)
            Me.txtBannerID.MaxLength = 10
            Me.txtBannerID.Name = "txtBannerID"
            Me.txtBannerID.Size = New System.Drawing.Size(100, 20)
            Me.txtBannerID.TabIndex = 1
            '
            'grbx_SAT
            '
            Me.grbx_SAT.Controls.Add(Me.txtSAT_S09)
            Me.grbx_SAT.Controls.Add(Me.txtSAT_S08)
            Me.grbx_SAT.Controls.Add(Me.Label15)
            Me.grbx_SAT.Controls.Add(Me.Label16)
            Me.grbx_SAT.Controls.Add(Me.txtSAT_S07)
            Me.grbx_SAT.Controls.Add(Me.txtSAT_S02)
            Me.grbx_SAT.Controls.Add(Me.txtSAT_S01)
            Me.grbx_SAT.Controls.Add(Me.Label8)
            Me.grbx_SAT.Controls.Add(Me.Label7)
            Me.grbx_SAT.Controls.Add(Me.Label6)
            Me.grbx_SAT.Location = New System.Drawing.Point(32, 124)
            Me.grbx_SAT.Name = "grbx_SAT"
            Me.grbx_SAT.Size = New System.Drawing.Size(670, 63)
            Me.grbx_SAT.TabIndex = 5
            Me.grbx_SAT.TabStop = False
            Me.grbx_SAT.Text = "SAT"
            '
            'txtSAT_S09
            '
            Me.txtSAT_S09.Location = New System.Drawing.Point(489, 23)
            Me.txtSAT_S09.MaxLength = 3
            Me.txtSAT_S09.Name = "txtSAT_S09"
            Me.txtSAT_S09.Size = New System.Drawing.Size(50, 20)
            Me.txtSAT_S09.TabIndex = 10
            '
            'txtSAT_S08
            '
            Me.txtSAT_S08.Location = New System.Drawing.Point(378, 23)
            Me.txtSAT_S08.MaxLength = 3
            Me.txtSAT_S08.Name = "txtSAT_S08"
            Me.txtSAT_S08.Size = New System.Drawing.Size(50, 20)
            Me.txtSAT_S08.TabIndex = 9
            '
            'Label15
            '
            Me.Label15.AutoSize = True
            Me.Label15.Location = New System.Drawing.Point(457, 26)
            Me.Label15.Name = "Label15"
            Me.Label15.Size = New System.Drawing.Size(30, 13)
            Me.Label15.TabIndex = 7
            Me.Label15.Text = "MCS"
            '
            'Label16
            '
            Me.Label16.AutoSize = True
            Me.Label16.Location = New System.Drawing.Point(346, 26)
            Me.Label16.Name = "Label16"
            Me.Label16.Size = New System.Drawing.Size(28, 13)
            Me.Label16.TabIndex = 6
            Me.Label16.Text = "ESS"
            '
            'txtSAT_S07
            '
            Me.txtSAT_S07.Location = New System.Drawing.Point(263, 23)
            Me.txtSAT_S07.MaxLength = 3
            Me.txtSAT_S07.Name = "txtSAT_S07"
            Me.txtSAT_S07.Size = New System.Drawing.Size(50, 20)
            Me.txtSAT_S07.TabIndex = 8
            '
            'txtSAT_S02
            '
            Me.txtSAT_S02.Location = New System.Drawing.Point(155, 23)
            Me.txtSAT_S02.MaxLength = 3
            Me.txtSAT_S02.Name = "txtSAT_S02"
            Me.txtSAT_S02.Size = New System.Drawing.Size(50, 20)
            Me.txtSAT_S02.TabIndex = 7
            '
            'txtSAT_S01
            '
            Me.txtSAT_S01.Location = New System.Drawing.Point(61, 23)
            Me.txtSAT_S01.MaxLength = 3
            Me.txtSAT_S01.Name = "txtSAT_S01"
            Me.txtSAT_S01.Size = New System.Drawing.Size(50, 20)
            Me.txtSAT_S01.TabIndex = 6
            '
            'Label8
            '
            Me.Label8.AutoSize = True
            Me.Label8.Location = New System.Drawing.Point(231, 26)
            Me.Label8.Name = "Label8"
            Me.Label8.Size = New System.Drawing.Size(26, 13)
            Me.Label8.TabIndex = 2
            Me.Label8.Text = "WR"
            '
            'Label7
            '
            Me.Label7.AutoSize = True
            Me.Label7.Location = New System.Drawing.Point(133, 26)
            Me.Label7.Name = "Label7"
            Me.Label7.Size = New System.Drawing.Size(16, 13)
            Me.Label7.TabIndex = 1
            Me.Label7.Text = "M"
            '
            'Label6
            '
            Me.Label6.AutoSize = True
            Me.Label6.Location = New System.Drawing.Point(21, 26)
            Me.Label6.Name = "Label6"
            Me.Label6.Size = New System.Drawing.Size(34, 13)
            Me.Label6.TabIndex = 0
            Me.Label6.Text = "CR/V"
            '
            'grbx_ACT
            '
            Me.grbx_ACT.Controls.Add(Me.txtACT_A07)
            Me.grbx_ACT.Controls.Add(Me.txtACT_A05)
            Me.grbx_ACT.Controls.Add(Me.txtACT_A04)
            Me.grbx_ACT.Controls.Add(Me.Label12)
            Me.grbx_ACT.Controls.Add(Me.Label13)
            Me.grbx_ACT.Controls.Add(Me.Label14)
            Me.grbx_ACT.Controls.Add(Me.txtACT_A03)
            Me.grbx_ACT.Controls.Add(Me.txtACT_A02)
            Me.grbx_ACT.Controls.Add(Me.txtACT_A01)
            Me.grbx_ACT.Controls.Add(Me.Label9)
            Me.grbx_ACT.Controls.Add(Me.Label10)
            Me.grbx_ACT.Controls.Add(Me.Label11)
            Me.grbx_ACT.Location = New System.Drawing.Point(32, 337)
            Me.grbx_ACT.Name = "grbx_ACT"
            Me.grbx_ACT.Size = New System.Drawing.Size(670, 63)
            Me.grbx_ACT.TabIndex = 5
            Me.grbx_ACT.TabStop = False
            Me.grbx_ACT.Text = "ACT"
            '
            'txtACT_A07
            '
            Me.txtACT_A07.Location = New System.Drawing.Point(601, 23)
            Me.txtACT_A07.MaxLength = 3
            Me.txtACT_A07.Name = "txtACT_A07"
            Me.txtACT_A07.Size = New System.Drawing.Size(50, 20)
            Me.txtACT_A07.TabIndex = 11
            '
            'txtACT_A05
            '
            Me.txtACT_A05.Location = New System.Drawing.Point(467, 23)
            Me.txtACT_A05.MaxLength = 3
            Me.txtACT_A05.Name = "txtACT_A05"
            Me.txtACT_A05.Size = New System.Drawing.Size(50, 20)
            Me.txtACT_A05.TabIndex = 10
            '
            'txtACT_A04
            '
            Me.txtACT_A04.Location = New System.Drawing.Point(345, 23)
            Me.txtACT_A04.MaxLength = 3
            Me.txtACT_A04.Name = "txtACT_A04"
            Me.txtACT_A04.Size = New System.Drawing.Size(50, 20)
            Me.txtACT_A04.TabIndex = 9
            '
            'Label12
            '
            Me.Label12.AutoSize = True
            Me.Label12.Location = New System.Drawing.Point(541, 26)
            Me.Label12.Name = "Label12"
            Me.Label12.Size = New System.Drawing.Size(54, 13)
            Me.Label12.TabIndex = 14
            Me.Label12.Text = "ENG/WR"
            '
            'Label13
            '
            Me.Label13.AutoSize = True
            Me.Label13.Location = New System.Drawing.Point(423, 26)
            Me.Label13.Name = "Label13"
            Me.Label13.Size = New System.Drawing.Size(38, 13)
            Me.Label13.TabIndex = 13
            Me.Label13.Text = "COMP"
            '
            'Label14
            '
            Me.Label14.AutoSize = True
            Me.Label14.Location = New System.Drawing.Point(317, 26)
            Me.Label14.Name = "Label14"
            Me.Label14.Size = New System.Drawing.Size(22, 13)
            Me.Label14.TabIndex = 12
            Me.Label14.Text = "SR"
            '
            'txtACT_A03
            '
            Me.txtACT_A03.Location = New System.Drawing.Point(242, 23)
            Me.txtACT_A03.MaxLength = 3
            Me.txtACT_A03.Name = "txtACT_A03"
            Me.txtACT_A03.Size = New System.Drawing.Size(50, 20)
            Me.txtACT_A03.TabIndex = 8
            '
            'txtACT_A02
            '
            Me.txtACT_A02.Location = New System.Drawing.Point(145, 23)
            Me.txtACT_A02.MaxLength = 3
            Me.txtACT_A02.Name = "txtACT_A02"
            Me.txtACT_A02.Size = New System.Drawing.Size(50, 20)
            Me.txtACT_A02.TabIndex = 7
            '
            'txtACT_A01
            '
            Me.txtACT_A01.Location = New System.Drawing.Point(53, 23)
            Me.txtACT_A01.MaxLength = 3
            Me.txtACT_A01.Name = "txtACT_A01"
            Me.txtACT_A01.Size = New System.Drawing.Size(50, 20)
            Me.txtACT_A01.TabIndex = 6
            '
            'Label9
            '
            Me.Label9.AutoSize = True
            Me.Label9.Location = New System.Drawing.Point(221, 26)
            Me.Label9.Name = "Label9"
            Me.Label9.Size = New System.Drawing.Size(15, 13)
            Me.Label9.TabIndex = 8
            Me.Label9.Text = "R"
            '
            'Label10
            '
            Me.Label10.AutoSize = True
            Me.Label10.Location = New System.Drawing.Point(123, 26)
            Me.Label10.Name = "Label10"
            Me.Label10.Size = New System.Drawing.Size(16, 13)
            Me.Label10.TabIndex = 7
            Me.Label10.Text = "M"
            '
            'Label11
            '
            Me.Label11.AutoSize = True
            Me.Label11.Location = New System.Drawing.Point(21, 26)
            Me.Label11.Name = "Label11"
            Me.Label11.Size = New System.Drawing.Size(30, 13)
            Me.Label11.TabIndex = 6
            Me.Label11.Text = "ENG"
            '
            'grbx_IELTS
            '
            Me.grbx_IELTS.Controls.Add(Me.txtIELTS_IEW)
            Me.grbx_IELTS.Controls.Add(Me.txtIELTS_IES)
            Me.grbx_IELTS.Controls.Add(Me.Label31)
            Me.grbx_IELTS.Controls.Add(Me.Label32)
            Me.grbx_IELTS.Controls.Add(Me.txtIELTS_IER)
            Me.grbx_IELTS.Controls.Add(Me.txtIELTS_IEOB)
            Me.grbx_IELTS.Controls.Add(Me.txtIELTS_IEL)
            Me.grbx_IELTS.Controls.Add(Me.Label33)
            Me.grbx_IELTS.Controls.Add(Me.Label34)
            Me.grbx_IELTS.Controls.Add(Me.Label35)
            Me.grbx_IELTS.Location = New System.Drawing.Point(32, 419)
            Me.grbx_IELTS.Name = "grbx_IELTS"
            Me.grbx_IELTS.Size = New System.Drawing.Size(670, 63)
            Me.grbx_IELTS.TabIndex = 5
            Me.grbx_IELTS.TabStop = False
            Me.grbx_IELTS.Text = "IELTS"
            '
            'txtIELTS_IEW
            '
            Me.txtIELTS_IEW.Location = New System.Drawing.Point(467, 23)
            Me.txtIELTS_IEW.MaxLength = 3
            Me.txtIELTS_IEW.Name = "txtIELTS_IEW"
            Me.txtIELTS_IEW.Size = New System.Drawing.Size(50, 20)
            Me.txtIELTS_IEW.TabIndex = 10
            '
            'txtIELTS_IES
            '
            Me.txtIELTS_IES.Location = New System.Drawing.Point(357, 23)
            Me.txtIELTS_IES.MaxLength = 3
            Me.txtIELTS_IES.Name = "txtIELTS_IES"
            Me.txtIELTS_IES.Size = New System.Drawing.Size(50, 20)
            Me.txtIELTS_IES.TabIndex = 9
            '
            'Label31
            '
            Me.Label31.AutoSize = True
            Me.Label31.Location = New System.Drawing.Point(435, 26)
            Me.Label31.Name = "Label31"
            Me.Label31.Size = New System.Drawing.Size(26, 13)
            Me.Label31.TabIndex = 31
            Me.Label31.Text = "WR"
            '
            'Label32
            '
            Me.Label32.AutoSize = True
            Me.Label32.Location = New System.Drawing.Point(336, 26)
            Me.Label32.Name = "Label32"
            Me.Label32.Size = New System.Drawing.Size(14, 13)
            Me.Label32.TabIndex = 30
            Me.Label32.Text = "S"
            '
            'txtIELTS_IER
            '
            Me.txtIELTS_IER.Location = New System.Drawing.Point(253, 23)
            Me.txtIELTS_IER.MaxLength = 3
            Me.txtIELTS_IER.Name = "txtIELTS_IER"
            Me.txtIELTS_IER.Size = New System.Drawing.Size(50, 20)
            Me.txtIELTS_IER.TabIndex = 8
            '
            'txtIELTS_IEOB
            '
            Me.txtIELTS_IEOB.Location = New System.Drawing.Point(155, 23)
            Me.txtIELTS_IEOB.MaxLength = 3
            Me.txtIELTS_IEOB.Name = "txtIELTS_IEOB"
            Me.txtIELTS_IEOB.Size = New System.Drawing.Size(50, 20)
            Me.txtIELTS_IEOB.TabIndex = 7
            '
            'txtIELTS_IEL
            '
            Me.txtIELTS_IEL.Location = New System.Drawing.Point(53, 23)
            Me.txtIELTS_IEL.MaxLength = 3
            Me.txtIELTS_IEL.Name = "txtIELTS_IEL"
            Me.txtIELTS_IEL.Size = New System.Drawing.Size(50, 20)
            Me.txtIELTS_IEL.TabIndex = 6
            '
            'Label33
            '
            Me.Label33.AutoSize = True
            Me.Label33.Location = New System.Drawing.Point(232, 26)
            Me.Label33.Name = "Label33"
            Me.Label33.Size = New System.Drawing.Size(15, 13)
            Me.Label33.TabIndex = 26
            Me.Label33.Text = "R"
            '
            'Label34
            '
            Me.Label34.AutoSize = True
            Me.Label34.Location = New System.Drawing.Point(123, 26)
            Me.Label34.Name = "Label34"
            Me.Label34.Size = New System.Drawing.Size(29, 13)
            Me.Label34.TabIndex = 25
            Me.Label34.Text = "OBS"
            '
            'Label35
            '
            Me.Label35.AutoSize = True
            Me.Label35.Location = New System.Drawing.Point(21, 26)
            Me.Label35.Name = "Label35"
            Me.Label35.Size = New System.Drawing.Size(23, 13)
            Me.Label35.TabIndex = 24
            Me.Label35.Text = "LIS"
            '
            'btnSubmit
            '
            Me.btnSubmit.Location = New System.Drawing.Point(410, 504)
            Me.btnSubmit.Name = "btnSubmit"
            Me.btnSubmit.Size = New System.Drawing.Size(113, 23)
            Me.btnSubmit.TabIndex = 12
            Me.btnSubmit.Text = "Submit"
            Me.btnSubmit.UseVisualStyleBackColor = True
            '
            'btnClear
            '
            Me.btnClear.Location = New System.Drawing.Point(226, 504)
            Me.btnClear.Name = "btnClear"
            Me.btnClear.Size = New System.Drawing.Size(75, 23)
            Me.btnClear.TabIndex = 46
            Me.btnClear.TabStop = False
            Me.btnClear.Text = "Clear"
            Me.btnClear.UseVisualStyleBackColor = True
            '
            'lblImage_ID
            '
            Me.lblImage_ID.AutoSize = True
            Me.lblImage_ID.Location = New System.Drawing.Point(82, 36)
            Me.lblImage_ID.Name = "lblImage_ID"
            Me.lblImage_ID.Size = New System.Drawing.Size(53, 13)
            Me.lblImage_ID.TabIndex = 47
            Me.lblImage_ID.Text = "Image_ID"
            '
            'dgTest_Scores
            '
            Me.dgTest_Scores.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
            Me.dgTest_Scores.Location = New System.Drawing.Point(32, 193)
            Me.dgTest_Scores.Name = "dgTest_Scores"
            Me.dgTest_Scores.Size = New System.Drawing.Size(670, 138)
            Me.dgTest_Scores.TabIndex = 48
            '
            'ErrorProvider_Tests
            '
            Me.ErrorProvider_Tests.ContainerControl = Me
            '
            'btnExit
            '
            Me.btnExit.Location = New System.Drawing.Point(633, 504)
            Me.btnExit.Name = "btnExit"
            Me.btnExit.Size = New System.Drawing.Size(75, 23)
            Me.btnExit.TabIndex = 49
            Me.btnExit.Text = "&Exit"
            Me.btnExit.UseVisualStyleBackColor = True
            '
            'Test_Scores
            '
            Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
            Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
            Me.ClientSize = New System.Drawing.Size(954, 538)
            Me.Controls.Add(Me.btnExit)
            Me.Controls.Add(Me.dgTest_Scores)
            Me.Controls.Add(Me.lblImage_ID)
            Me.Controls.Add(Me.btnClear)
            Me.Controls.Add(Me.btnSubmit)
            Me.Controls.Add(Me.grbx_IELTS)
            Me.Controls.Add(Me.grbx_ACT)
            Me.Controls.Add(Me.grbx_SAT)
            Me.Controls.Add(Me.txtBannerID)
            Me.Controls.Add(Me.Label5)
            Me.Controls.Add(Me.Label4)
            Me.Controls.Add(Me.Label3)
            Me.Controls.Add(Me.cbxTest_Source)
            Me.Controls.Add(Me.Label2)
            Me.Controls.Add(Me.cbxTest_Type)
            Me.Controls.Add(Me.Label1)
            Me.Controls.Add(Me.dtTest_Date)
            Me.Name = "Test_Scores"
            Me.Text = "Test_Scores"
            Me.grbx_SAT.ResumeLayout(False)
            Me.grbx_SAT.PerformLayout()
            Me.grbx_ACT.ResumeLayout(False)
            Me.grbx_ACT.PerformLayout()
            Me.grbx_IELTS.ResumeLayout(False)
            Me.grbx_IELTS.PerformLayout()
            CType(Me.dgTest_Scores, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.ErrorProvider_Tests, System.ComponentModel.ISupportInitialize).EndInit()
            Me.ResumeLayout(False)
            Me.PerformLayout()

        End Sub
        Friend WithEvents dtTest_Date As System.Windows.Forms.DateTimePicker
        Friend WithEvents Label1 As System.Windows.Forms.Label
        Friend WithEvents cbxTest_Type As System.Windows.Forms.ComboBox
        Friend WithEvents Label2 As System.Windows.Forms.Label
        Friend WithEvents cbxTest_Source As System.Windows.Forms.ComboBox
        Friend WithEvents Label3 As System.Windows.Forms.Label
        Friend WithEvents Label4 As System.Windows.Forms.Label
        Friend WithEvents Label5 As System.Windows.Forms.Label
        Friend WithEvents txtBannerID As System.Windows.Forms.TextBox
        Friend WithEvents grbx_SAT As System.Windows.Forms.GroupBox
        Friend WithEvents Label6 As System.Windows.Forms.Label
        Friend WithEvents grbx_ACT As System.Windows.Forms.GroupBox
        Friend WithEvents grbx_IELTS As System.Windows.Forms.GroupBox
        Friend WithEvents txtSAT_S07 As System.Windows.Forms.TextBox
        Friend WithEvents txtSAT_S02 As System.Windows.Forms.TextBox
        Friend WithEvents txtSAT_S01 As System.Windows.Forms.TextBox
        Friend WithEvents Label8 As System.Windows.Forms.Label
        Friend WithEvents Label7 As System.Windows.Forms.Label
        Friend WithEvents txtACT_A07 As System.Windows.Forms.TextBox
        Friend WithEvents txtACT_A05 As System.Windows.Forms.TextBox
        Friend WithEvents txtACT_A04 As System.Windows.Forms.TextBox
        Friend WithEvents Label12 As System.Windows.Forms.Label
        Friend WithEvents Label13 As System.Windows.Forms.Label
        Friend WithEvents Label14 As System.Windows.Forms.Label
        Friend WithEvents txtACT_A03 As System.Windows.Forms.TextBox
        Friend WithEvents txtACT_A02 As System.Windows.Forms.TextBox
        Friend WithEvents txtACT_A01 As System.Windows.Forms.TextBox
        Friend WithEvents Label9 As System.Windows.Forms.Label
        Friend WithEvents Label10 As System.Windows.Forms.Label
        Friend WithEvents Label11 As System.Windows.Forms.Label
        Friend WithEvents txtIELTS_IEW As System.Windows.Forms.TextBox
        Friend WithEvents txtIELTS_IES As System.Windows.Forms.TextBox
        Friend WithEvents Label31 As System.Windows.Forms.Label
        Friend WithEvents Label32 As System.Windows.Forms.Label
        Friend WithEvents txtIELTS_IER As System.Windows.Forms.TextBox
        Friend WithEvents txtIELTS_IEOB As System.Windows.Forms.TextBox
        Friend WithEvents txtIELTS_IEL As System.Windows.Forms.TextBox
        Friend WithEvents Label33 As System.Windows.Forms.Label
        Friend WithEvents Label34 As System.Windows.Forms.Label
        Friend WithEvents Label35 As System.Windows.Forms.Label
        Friend WithEvents btnSubmit As System.Windows.Forms.Button
        Friend WithEvents btnClear As System.Windows.Forms.Button
        Friend WithEvents lblImage_ID As System.Windows.Forms.Label
        Friend WithEvents dgTest_Scores As System.Windows.Forms.DataGridView
        Friend WithEvents ErrorProvider_Tests As System.Windows.Forms.ErrorProvider
        Friend WithEvents txtSAT_S09 As System.Windows.Forms.TextBox
        Friend WithEvents txtSAT_S08 As System.Windows.Forms.TextBox
        Friend WithEvents Label15 As System.Windows.Forms.Label
        Friend WithEvents Label16 As System.Windows.Forms.Label
        Friend WithEvents btnExit As System.Windows.Forms.Button
    End Class
End Namespace
