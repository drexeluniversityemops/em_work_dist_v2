USE [EM_Image_Processing]
GO

/****** Object:  StoredProcedure [dbo].[EM_WD_Submit_Test_Scores_del]    Script Date: 9/27/2013 12:12:11 PM *****
DROP PROCEDURE [dbo].[EM_WD_Submit_Test_Scores_del]
DROP PROCEDURE [dbo].[EM_WD_Submit_Test_Scores_updt]*/

ALTER proc [dbo].[EM_WD_GetImage_Test_Scores]

	@Banner_ID varchar(8)
as

SELECT
		Test_Score_Code as Code,
		Test_Score_Score as Score,
		Test_Score_Date as Test_Date,
		Test_Score_Source as Source,
		Test_Score_Processed_Datetime as Processed_Date,
		Image_Test_Scores_ID,
		Image_ID
FROM
		Image_Test_Scores
		
WHERE 
		Banner_ID = @Banner_ID	
		
ORDER BY
		Image_Test_Scores_ID desc

GO


